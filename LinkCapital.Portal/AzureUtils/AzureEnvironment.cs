﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUtils
{
    public static class AzureEnvironment
    {
        private static bool? myIzAzure;

        /// <summary>
        /// Returns true if running in Azure
        /// </summary>
        public static bool IsAzure
        {
            get
            {
                if (myIzAzure == null)
                {
                    myIzAzure = (Environment.GetEnvironmentVariable("APP_POOL_ID") != null);
                }

                return (bool)myIzAzure;
            }
        }
        /// <summary>
        /// Returns true if running Locally
        /// </summary>
        public static bool IsLocal
        {
            get
            {
                return !IsAzure;
            }
        }



    }
}
