﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUtils
{
    public class SuspendOperationException : Exception
    {
        public string Message { get; set; }

        public SuspendOperationException(string message)
        {
            this.Message = message;
        }
    }
}
