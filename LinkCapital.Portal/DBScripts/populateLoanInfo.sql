USE [LinkCapital.Portal]
GO

DELETE FROM [dbo].[States];
Go

DBCC CHECKIDENT (States, RESEED, 0)

INSERT INTO [dbo].[States]
           ([Name]
           ,[Abbreviation])
     VALUES
('Alabama','AL'),
('Alaska','AK'),
('Arizona','AZ'),
('Arkansas','AR'),
('California','CA'),
('Colorado','CO'),
('Connecticut','CT'),
('Delaware','DE'),
('Florida','FL'),
('Georgia','GA'),
('Hawaii','HI'),
('Idaho','ID'),
('Illinois','IL'),
('Indiana','IN'),
('Iowa','IA'),
('Kansas','KS'),
('Kentucky','KY'),
('Louisiana','LA'),
('Maine','ME'),
('Maryland','MD'),
('Massachusetts','MA'),
('Michigan','MI'),
('Minnesota','MN'),
('Mississippi','MS'),
('Missouri','MO'),
('Montana','MT'),
('Nebraska','NE'),
('Nevada','NV'),
('New Hampshire','NH'),
('New Jersey','NJ'),
('New Mexico','NM'),
('New York','NY'),
('North Carolina','NC'),
('North Dakota','ND'),
('Ohio','OH'),
('Oklahoma','OK'),
('Oregon','OR'),
('Pennsylvania','PA'),
('Rhode Island','RI'),
('South Carolina','SC'),
('South Dakota','SD'),
('Tennessee','TN'),
('Texas','TX'),
('Utah','UT'),
('Vermont','VT'),
('Virginia','VA'),
('Washington','WA'),
('West Virginia','WV'),
('Wisconsin','WI'),
('Wyoming','WY'),
('District of Columbia','DC')
GO

DELETE FROM [dbo].[LoanPrograms]
Go

DBCC CHECKIDENT (LoanPrograms, RESEED, 0)

INSERT INTO [dbo].[LoanPrograms]
           ([Name]
           ,[Url])
     VALUES
		   ('Link Capital','http://myaccount.qa.campusdoor.com/ExternalAuth/SSOConnector.aspx?function=NewLoanApp&ReferralID=3301'),
		   ('Bank of Lake Mills','http://myaccount.qa.campusdoor.com/ExternalAuth/SSOConnector.aspx?function=NewLoanApp&ReferralID=3307'),
		   ('Link Capital (Residency)','http://myaccount.qa.campusdoor.com/ExternalAuth/SSOConnector.aspx?function=NewLoanApp&ReferralID=3310'),
		   ('Bank of Lake Mills (Residency)','http://myaccount.qa.campusdoor.com/ExternalAuth/SSOConnector.aspx?function=NewLoanApp&ReferralID=3308')
GO

DELETE FROM [dbo].[LoanRules]
Go

ALTER TABLE [dbo].[LoanRules] DROP CONSTRAINT [FK_dbo.LoanRules_dbo.States_StateId]
GO

ALTER TABLE [dbo].[LoanRules] DROP CONSTRAINT [FK_dbo.LoanRules_dbo.LoanPrograms_LoanProgramId]
GO

/****** Object:  Table [dbo].[LoanRules]    Script Date: 6/5/2015 3:06:36 PM ******/
DROP TABLE [dbo].[LoanRules]
GO

/****** Object:  Table [dbo].[LoanRules]    Script Date: 6/5/2015 3:06:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LoanRules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoanProgramId] [int] NOT NULL,
	[StateId] [int] NOT NULL,
	[LoanAmountThreshold] [decimal](18, 2) NOT NULL,
	[LoanQualificationRestriction] [int] NOT NULL DEFAULT ((0)),
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.LoanRules] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LoanRules]  WITH CHECK ADD  CONSTRAINT [FK_dbo.LoanRules_dbo.LoanPrograms_LoanProgramId] FOREIGN KEY([LoanProgramId])
REFERENCES [dbo].[LoanPrograms] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[LoanRules] CHECK CONSTRAINT [FK_dbo.LoanRules_dbo.LoanPrograms_LoanProgramId]
GO

ALTER TABLE [dbo].[LoanRules]  WITH CHECK ADD  CONSTRAINT [FK_dbo.LoanRules_dbo.States_StateId] FOREIGN KEY([StateId])
REFERENCES [dbo].[States] ([Id])
ON DELETE CASCADE
GO

GO
ALTER TABLE [dbo].[LoanRules] CHECK CONSTRAINT [FK_dbo.LoanRules_dbo.States_StateId]

SET IDENTITY_INSERT [dbo].[LoanRules] ON

INSERT INTO [dbo].[LoanRules]
           ([Id] 
		   ,[LoanProgramId]
           ,[StateId]
           ,[LoanAmountThreshold]
           ,[LoanQualificationRestriction]
		   ,[Active])
     VALUES
		   (15622, 1, 2, 25000.00, 0, 1),
			(15623,1,3,10000.00,0, 1),
			(15624,1,4,0.00,0, 1),
			(15625,1,6,75000.00,0, 1),
			(15626,1,7,15000.00,0, 1),
			(15627,1,9,25000.00,0, 1),
			(15628,1,10,3000.00,0, 1),
			(15629,1,11,0.00,0, 1),
			(15630,1,12,0.00,0, 1),
			(15631,1,13,40000.00,0, 1),
			(15632,1,14,53000.00,0, 1),
			(15633,1,15,25000.00,0, 1),
			(15634,1,16,25000.00,0, 1),
			(15635,1,17,15000.00,0, 1),
			(15636,1,19,53000.00,0, 1),
			(15637,1,21,6000.00,0, 1),
			(15638,1,23,100000.00,0, 1),
			(15639,1,30,50000.00,0, 1),
			(15640,1,31,2500.00,0, 1),
			(15641,1,32,25000.00,0, 1),
			(15642,1,33,15000.00,0, 1),
			(15643,1,35,5000.00,0, 1),
			(15644,1,37,50000.00,0, 1),
			(15645,1,38,0.00,0, 1),
			(15646,1,40,7500.00,0, 1),
			(15647,1,42,0.00,0, 1),
			(15648,1,43,0.00,0, 1),
			(15649,1,44,25000.00,0, 1),
			(15650,1,46,0.00,0, 1),
			(15651,1,48,45000.00,0, 1),
			(15652,1,49,25000.00,0, 1),
			(15653,1,50,75000.00,0, 1),
			(15654,1,51,25000.00,0, 1),
			(15655,2,1,0.00,0, 1),
			(15656,2,2,0.00,0, 1),
			(15657,2,3,0.00,0, 1),
			(15658,2,5,0.00,0, 1),
			(15659,2,6,0.00,0, 1),
			(15660,2,7,0.00,0, 1),
			(15661,2,8,0.00,0, 1),
			(15662,2,9,0.00,0, 1),
			(15663,2,10,0.00,0, 1),
			(15664,2,13,0.00,0, 1),
			(15665,2,14,0.00,0, 1),
			(15666,2,15,0.00,0, 1),
			(15667,2,16,0.00,0, 1),
			(15668,2,17,0.00,0, 1),
			(15669,2,18,0.00,0, 1),
			(15670,2,19,0.00,0, 1),
			(15671,2,20,0.00,0, 1),
			(15672,2,21,0.00,0, 1),
			(15673,2,22,0.00,0, 1),
			(15674,2,23,0.00,0, 1),
			(15675,2,24,0.00,0, 1),
			(15676,2,25,0.00,0, 1),
			(15677,2,26,0.00,0, 1),
			(15678,2,27,0.00,0, 1),
			(15679,2,28,0.00,0, 1),
			(15680,2,29,0.00,0, 1),
			(15681,2,30,0.00,0, 1),
			(15682,2,31,0.00,0, 1),
			(15683,2,32,0.00,0, 1),
			(15684,2,33,0.00,0, 1),
			(15685,2,34,0.00,0, 1),
			(15686,2,35,0.00,0, 1),
			(15687,2,36,0.00,0, 1),
			(15688,2,37,0.00,0, 1),
			(15689,2,39,0.00,0, 1),
			(15690,2,40,0.00,0, 1),
			(15691,2,41,0.00,0, 1),
			(15692,2,44,0.00,0, 1),
			(15693,2,45,0.00,0, 1),
			(15694,2,47,0.00,0, 1),
			(15695,2,48,0.00,0, 1),
			(15696,2,49,0.00,0, 1),
			(15697,2,50,0.00,0, 1),
			(15698,2,51,0.00,0, 1),
			(15699,3,2,25000.00,1, 1),
			(15700,3,3,10000.00,1, 1),
			(15701,3,4,0.00,1, 1),
			(15702,3,6,75000.00,1, 1),
			(15703,3,7,15000.00,1, 1),
			(15704,3,9,25000.00,1, 1),
			(15705,3,10,3000.00,1, 1),
			(15706,3,11,0.00,1, 1),
			(15707,3,12,0.00,1, 1),
			(15708,3,13,40000.00,1, 1),
			(15709,3,14,53000.00,1, 1),
			(15710,3,15,25000.00,1, 1),
			(15711,3,16,25000.00,1, 1),
			(15712,3,17,15000.00,1, 1),
			(15713,3,19,53000.00,1, 1),
			(15714,3,21,6000.00,1, 1),
			(15715,3,23,100000.00,1, 1),
			(15716,3,30,50000.00,1, 1),
			(15717,3,31,2500.00,1, 1),
			(15718,3,32,25000.00,1, 1),
			(15719,3,33,15000.00,1, 1),
			(15720,3,35,5000.00,1, 1),
			(15721,3,37,50000.00,1, 1),
			(15722,3,38,0.00,1, 1),
			(15723,3,40,7500.00,1, 1),
			(15724,3,42,0.00,1, 1),
			(15725,3,43,0.00,1, 1),
			(15726,3,44,25000.00,1, 1),
			(15727,3,46,0.00,1, 1),
			(15728,3,48,45000.00,1, 1),
			(15729,3,49,25000.00,1, 1),
			(15730,3,50,75000.00,1, 1),
			(15731,3,51,25000.00,1, 1),
			(15732,4,1,0.00,1, 1),
			(15733,4,2,0.00,1, 1),
			(15734,4,3,0.00,1, 1),
			(15735,4,5,0.00,1, 1),
			(15736,4,6,0.00,1, 1),
			(15737,4,7,0.00,1, 1),
			(15738,4,8,0.00,1, 1),
			(15739,4,9,0.00,1, 1),
			(15740,4,10,0.00,1, 1),
			(15741,4,13,0.00,1, 1),
			(15742,4,14,0.00,1, 1),
			(15743,4,15,0.00,1, 1),
			(15744,4,16,0.00,1, 1),
			(15745,4,17,0.00,1, 1),
			(15746,4,18,0.00,1, 1),
			(15747,4,19,0.00,1, 1),
			(15748,4,20,0.00,1, 1),
			(15749,4,21,0.00,1, 1),
			(15750,4,22,0.00,1, 1),
			(15751,4,23,0.00,1, 1),
			(15752,4,24,0.00,1, 1),
			(15753,4,25,0.00,1, 1),
			(15754,4,26,0.00,1, 1),
			(15755,4,27,0.00,1, 1),
			(15756,4,28,0.00,1, 1),
			(15757,4,29,0.00,1, 1),
			(15758,4,30,0.00,1, 1),
			(15759,4,31,0.00,1, 1),
			(15760,4,32,0.00,1, 1),
			(15761,4,33,0.00,1, 1),
			(15762,4,34,0.00,1, 1),
			(15763,4,35,0.00,1, 1),
			(15764,4,36,0.00,1, 1),
			(15765,4,37,0.00,1, 1),
			(15766,4,39,0.00,1, 1),
			(15767,4,40,0.00,1, 1),
			(15768,4,41,0.00,1, 1),
			(15769,4,44,0.00,1, 1),
			(15770,4,45,0.00,1, 1),
			(15771,4,47,0.00,1, 1),
			(15772,4,48,0.00,1, 1),
			(15773,4,49,0.00,1, 1),
			(15774,4,50,0.00,1, 1),
			(15775,4,51,0.00,1, 1)
GO

SET IDENTITY_INSERT [dbo].[LoanRules] OFF
GO

INSERT INTO [dbo].[DynamicConfigValues]
           ([ID]
           ,[Name]
           ,[Description]
           ,[Value]
           ,[DefaultValue])
     VALUES
		   ('E60FC66C-ABAD-426F-90F0-443F9074AF08', 'EftReductionRate',	'Rate reduction if borrower is using EFT/ACH', 0.0025, 0.0025),
		   ('41512EF9-0CC9-4BFD-90CB-539C6229F0FE', '3MonthLIBOR',	'3-Month LIBOR used in the loan Calc', 0.0028, 0.0028)
GO

INSERT INTO [dbo].[Rates]
           ([RateType]
           ,[Resident]
           ,[Term]
           ,[InterestRate])
     VALUES
		   (0,0,3,0.0425),
		   (0,0,4,0.0475),
		   (0,0,5,0.0525),
		   (0,0,7,0.055),
		   (0,0,10,0.0575),
		   (0,0,15,0.065),
		   (0,0,20,0.07),
		   (1,0,3,0.025),
		   (1,0,4,0.03),
		   (1,0,5,0.0325),
		   (1,0,7,0.035),
		   (1,0,10,0.0375),
		   (1,0,15,0.0425),
		   (1,0,20,0.045),
		   (0,1,7,0.0599),
		   (0,1,10,0.06125),
		   (0,1,15,0.06675),
		   (0,1,20,0.071)
GO
