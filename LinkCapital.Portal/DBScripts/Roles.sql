USE [LinkCapital.Portal]
GO

INSERT INTO [dbo].[AspNetRoles]
           ([Id]
           ,[Name])
     VALUES
           (1,'Admin'),
		   (2, 'Borrower')
GO

INSERT INTO [dbo].[AspNetUserRoles]
        SELECT [Id], 2 FROM [dbo].[AspNetUsers]
GO
