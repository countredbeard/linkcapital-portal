USE [LinkCapital.Portal]
GO

INSERT INTO [dbo].[DynamicConfigValues]
           ([ID],[Name]
           ,[Description]
           ,[Value]
           ,[DefaultValue])
     VALUES
           (NEWID(),'TermsAndConditions'
           ,'Rate Details, Terms & Conditions'
           ,'<p><strong>Thank you for your interest in LinkCapital.</strong><br>We want to make you aware that by visiting LinkCapital.com, you are agreeing to be bound by the Terms of Use described below. If you have any questions or concerns, please contact us at info@linkcapital.com.</p>
<p><strong>Fixed Rate Loans</strong><br>Fixed rates from 4.00% APR to 6.75% APR (with ACH Discount).</p>
<p><strong>Variable Rate Loans</strong><br>On variable rate loans, The Interest Rate will be equal to the �Current Index� plus a �Margin� identified on my Final Disclosure Statement. The Interest Rate will change if the Current Index changes. The �Current Index� is the daily average of the 3-month London Interbank Offered Rate (LIBOR) (currency in U.S. dollars) that was published on the Wall Street Journal�s website (or any generally recognized successor method or means of publication) on each business day during the 91-day period ending on the 10th day of the calendar month immediately preceding each �Change Date�, as defined below. The Interest Rate will change quarterly on the first day of each calendar quarter if the Current Index changes.</p>
<p><strong>EFT Discount</strong><br>A LinkCapital Borrower may have the option to obtain a .25% lower Interest Rate for making monthly payments by an automatic monthly deduction from a savings or checking account. The benefit will discontinue for a period of 12 months following three consecutive payment-denials.</p>
<p><strong>Fee Information</strong><br>LinkCapital has no origination fees and no prepayment penalties. However, if my payment is not received on or before its due date, my loan will be in default. In addition, I will pay the Lender or Servicer, as applicable, a late charge if Lender or Servicer does not receive any part of a monthly payment within fifteen (15) days after it becomes due. The late charge will be 5% of the unpaid amount of the payment due or Ten Dollars ($10), whichever is less, except where prohibited by law.</p>
<p><strong>Eligibility</strong><br>Must be a U.S. Citizen and meet LinkCapital�s underwriting criteria (including, for example, employment, disposable income, total student loan debt relative to annual salary level, and credit history requirements).</p>
<p><strong>Eligible Loans</strong><br>Graduates may refinance and/or consolidate any unsubsidized or subsidized Federal or private student loan (not guaranteed or insured by the federal government) that was used exclusively for qualified higher education expenses (as defined in 26 USC Section 221) at an accredited U.S. undergraduate, graduate or medical school.</p>
<p><strong>Loan Example</strong><br>Loan examples on the website display representative examples of typical transactions provided by LinkCapital.</p>
<p><strong>Savings Example</strong><br>Savings&nbsp;of $330 per month assumes loan size of $81,321 with an average rate of 7.1% before refinancing and with ACH discount after refinancing, on a 20-year fixed rate loan.</p>
<p><span>Savings of $52,381 over the life of the loan assumes loan size of $219,215 with an average rate of <span>6.9%</span> before refinancing and with <span>ACH discount after refinancing</span>, on a 7-year fixed rate loan. </span></p>
<p><span>Savings of <span>$92,107</span> over the life of the loan assumes loan size of $260,090 with an average rate of <span>7.0%</span> before refinancing and with <span>ACH discount after refinancing</span>, on a 3-year variable rate loan.</span></p>
<p><strong>Legal</strong><br>This website and all content is the exclusive property of LinkCapital and may not be reproduced without permission. All information contained on this website is subject to change without notice. LinkCapital is not responsible for typographical errors.</p>
<p><strong>Links</strong><br>LinkCapital.com may contain links to websites maintained by third parties. Such websites may have Terms of Use, Privacy Policies, or security practices that are different from those of LinkCapital. LinkCapital disclaims any liability for any information or products offered at such websites.</p>
<p><strong>Website Changes and Modifications</strong><br>The materials appearing on LinkCapital.com could include technical, typographical, or photographic errors. LinkCapital does not warrant that any of the materials on its web site are accurate, complete, or current. LinkCapital may make changes to the materials contained on its web site at any time without notice. LinkCapital reserves the right to modify or discontinue products and benefits at any time without notice.</p>
<p><strong>Disclaimer</strong><br>The materials on LinkCapital�s web site are provided �as is�. LinkCapital makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, LinkCapital does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>
<p><strong>Limitations</strong><br>In no event shall LinkCapital or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on LinkCapital�s web site, even if LinkCapital or a LinkCapital authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>
<p><strong>Site Terms of Use Modifications</strong><br>LinkCapital may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.</p>
<p><strong>Applicable Law</strong><br>You agree to abide by all applicable local, state, national and international laws and regulations in your use of LinkCapital.com. Our website is available only to individuals who are permitted to use it under applicable law. You agree to be solely responsible for your actions and the contents of your transmissions through the website. You represent and warrant that you possess the legal right and ability to enter into these Terms of Use and to use the website in accordance with these terms of use.</p>'
           ,'Please contact LinkCapital for our latest Terms And Conditions')

INSERT INTO [dbo].[DynamicConfigValues]
           ([ID],[Name]
           ,[Description]
           ,[Value]
           ,[DefaultValue])
     VALUES
           (NEWID(),'PrivacyPolicy'
           ,'Privacy Policy'
           ,'<p><strong>Updated January 24, 2014</strong><br> LinkCapital collects and stores information about individuals who apply for a LinkCapital loan and about visitors to the LinkCapital website. This information helps LinkCapital provide better products and services, whether it�s a loan with lower interest rate or a pro-active email letting you know that your loan payment is almost past due.</p>
<p><strong>The information we collect</strong><br><br> LinkCapital collects information in three ways:</p>
<ol>
<li>Information you provide. If you have already applied for a loan you�ve seen that a number of demographic and financial information is required. This information is collected by CampusDoor, LinkCapital�s service provider, but ultimately transferred to LinkCapital.</li>
<li>Information we get when you use our services. You interact with LinkCapital�s loan servicing team every time you make a monthly payment or call our help desk for information. This information is collected by Aspire Resources Inc., LinkCapital�s service provider, but ultimately transferred to LinkCapital.</li>
<li>Information we get when you interact with our website. LinkCapital uses cookies and anonymous identifiers to collect and store basic website usage information like the pages on our website that you visited.</li>
</ol>
<p><strong>How we use it</strong><br> We use the information we collect to provide, maintain, and improve our existing products and services. For example, collecting demographic and financial information helps us project a borrower�s risk of default. A lower default risk unlocks a lower interest rate on a LinkCapital loan.</p>
<p><strong>Information we share</strong><br> LinkCapital shares information with its two core business partners:</p>
<ol>
<li>CampusDoor, LinkCapital�s loan application engine</li>
<li>Aspire Resources Inc., Link�s Capitals loan service provider</li>
</ol>
<p><strong>Information Security</strong><br> LinkCapital is committed to protecting the security of your personal information. We use a variety of industry-standard security technologies and procedures to help protect against unauthorized access, use, or disclosure.</p>
<p><strong>Changes</strong><br> Our privacy policy may change from time to time. We will not reduce your rights under this privacy policy without your explicit consent. We will post any privacy policy changes on this page and, if the changes are significant, we will provide a more prominent notice.</p>'
           ,'Please contact LinkCapital for our latest Privacy Policy')

GO