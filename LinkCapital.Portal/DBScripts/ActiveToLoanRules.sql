USE [LinkCapital.Portal]
GO

ALTER TABLE [dbo].[LoanRules]
ADD Active Bit NULL
GO

UPDATE [dbo].[LoanRules]
SET [Active] = 1
GO

ALTER TABLE [dbo].[LoanRules]
ADD Active Bit NOT NULL
GO
