USE [LinkCapital.Portal]
GO

ALTER TABLE [dbo].[AspNetUsers]
	ADD
	Cosigner BIT NOT NULL DEFAULT 0
GO