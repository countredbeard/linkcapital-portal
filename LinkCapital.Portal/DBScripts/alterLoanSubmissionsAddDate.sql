USE [LinkCapital.Portal]
GO

ALTER TABLE [dbo].[LoanSubmissions]
	ADD
	DateSubmitted DATETIME NULL
GO