USE [LinkCapital.Portal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LoanSubmissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[State] [varchar](50) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Resident] [bit] NOT NULL,
	[LoanProgram] [nvarchar](50) NOT NULL,
	[LoanUrl] [nvarchar](200) NOT NULL
 CONSTRAINT [PK_dbo.LoanSubmissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[LoanSubmissions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.LoanSubmissions.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[LoanSubmissions] CHECK CONSTRAINT [FK_dbo.LoanSubmissions.AspNetUsers_UserId]
GO
