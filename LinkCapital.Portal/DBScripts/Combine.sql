USE [LinkCapital.Portal]
GO

ALTER TABLE [dbo].[LoanRules]
ADD Active Bit NULL
GO

UPDATE [dbo].[LoanRules]
SET [Active] = 1
GO

ALTER TABLE [dbo].[LoanRules]
ADD Active Bit NOT NULL
GO

UPDATE [dbo].[LoanPrograms]
   SET [Url] = '&ReferralID=3301'
 WHERE [Id] = 1
GO

UPDATE [dbo].[LoanPrograms]
   SET [Url] = '&ReferralID=3307'
 WHERE [Id] = 2
GO

UPDATE [dbo].[LoanPrograms]
   SET [Url] = '&ReferralID=3310'
 WHERE [Id] = 3
GO

UPDATE [dbo].[LoanPrograms]
   SET [Url] = '&ReferralID=3308'
 WHERE [Id] = 4
GO

INSERT INTO [dbo].[AspNetRoles]
           ([Id]
           ,[Name])
     VALUES
           (1,'Admin'),
		   (2, 'Borrower')
GO

INSERT INTO [dbo].[AspNetUserRoles]
        SELECT [Id], 2 FROM [dbo].[AspNetUsers]
GO
