﻿using AzureUtils;
using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWebJob
{
    class Program
    {
        static void Main(string[] args)
        {
            var environmentVariables = Environment.GetEnvironmentVariables();
            string appPoolId = Environment.GetEnvironmentVariable("APP_POOL_ID");

            if (AzureEnvironment.IsAzure)
            {
                Console.WriteLine("Hi, this is Azure!");
            }
            else
            {
                Console.WriteLine("Hi, this is local!");
            }

            foreach (var key in environmentVariables.Keys)
            {
                Console.WriteLine("'{0}','{1}'", key, environmentVariables[key]);
            }
        }
    }
}
