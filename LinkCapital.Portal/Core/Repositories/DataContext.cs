﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Core.EntityModels;

namespace Core.Repositories
{
    public class DataContext : IdentityDbContext<ApplicationUser>
	{
		#region DBSets

		public DbSet<State> States { get; set; }
		public DbSet<LoanProgram> LoanPrograms { get; set; }
        public DbSet<LoanRule> LoanRules { get; set; }
        public DbSet<RegistrationContext> RegistrationContexts { get; set; }
        public DbSet<PartnerDomain> PartnerDomains { get; set; }
        public DbSet<Partner> Partners { get; set; }

        #endregion

        #region Constructors

        // Any consuming instation must pass the connection string - Aaron V.
        public DataContext(string nameOrConnectionString) : base(nameOrConnectionString, throwIfV1Schema: false) { }

#if DEBUG
        public DataContext()
            : base(
                "data source=.\\SQLEXPRESS;initial catalog=LinkCapital.Portal;User Id=LinkCapitalLocal;Password=zfrhr7bd2l;integrated security=False;multipleactiveresultsets=True;App=EntityFramework"
                ) {}
#endif
        #endregion
    }
}
