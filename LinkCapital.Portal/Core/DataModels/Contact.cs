﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Core.EntityModels;
using Newtonsoft.Json;

namespace Core.DataModels
{
    /// <summary>
    /// Maps the contact from salesforce
    /// </summary>
    public class SimpleContact
    {
        // this class is currently stripped down to fit in to the current project time span
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Email { get; set; }
        public string X2nd_Phone__c { get; set; }
        //public string WorkPhone { get; set; }
        public string Phone { get; set; }
        //public Address MailingAddress { get; set; }
        public string Lead_Status__c { get; set; }
        //public string RecordTypeID = "012G0000001QPdf";
        public string RecordTypeID { get; set; }
        public string AccountID { get; set; }

        public SimpleContact() { }

        public SimpleContact(ApplicationUser user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.UserName;
            Phone = user.PhoneNumber;
            Lead_Status__c = user.LeadStatus;
        }

        public SimpleContact(Contact macroContent)
        {
            //this.Name = macroContent.Name;
            this.FirstName = macroContent.FirstName;
            this.LastName = macroContent.LastName;
            this.Birthdate = macroContent.Birthdate;
            this.Email = macroContent.Email;
            this.X2nd_Phone__c = macroContent.X2nd_Phone__c;
            this.Phone = macroContent.Phone;
            //5this.MailingAddress = macroContent.MailingAddress;
            this.Lead_Status__c = macroContent.Lead_Status__c;
            //RecordTypeID = "012G0000001QPdf";
            AccountID = macroContent.AccountID;
        }
    }

    public class Contact: SimpleContact
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public Address MailingAddress { get; set; }

        public Contact() { }

        public Contact(ApplicationUser user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.UserName;
            Phone = user.PhoneNumber;
            Lead_Status__c = user.LeadStatus;
        }
    }

    // Lead Status types
    /* Soft Denied, Hard Denied, Descision Pending, Fully dispersied, Documantation, DocumantationPending, Submitted, */
}
