﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class Opportunity : SfdcBaseModel
    {
        public string AccountId { get; set; }
        public string OwnerId { get; set; }
        public string Name { get; set; }
        public string Lender__c { get; set; }
        public string Program_Name__c { get; set; }
        public string Application_ID__c { get; set; }
        public string School_Name__c { get; set; }
        public string Requested_Loan_Amount__c { get; set; }
        public string Certified_Loan_Amount__c { get; set; }
        public DateTime? Submitted_Date__c { get; set; }
        public DateTime? Actual_Disbursement_Date__c { get; set; }
        public string Current_Status__c { get; set; }
        public string Current_State__c { get; set; }
        //public DateTime Current_Status_Date__c { get; set; }
        //public string BorrowerAAReason1__c { get; set; }
        //public string BorrowerAAReason2__c { get; set; }
        //public string BorrowerAAReason3__c { get; set; }
        //public string BorrowerAAReason4__c { get; set; }
        public string Credit_Tier__c { get; set; }
        public string Rate_Type__c { get; set; }
        public string Interest_Rate__c { get; set; }
        public string Repayment_Term__c { get; set; }
        public DateTime? Loan_Start_Date__c { get; set; }
        public string Borrower_Employment__c { get; set; }
        public string Borrower_Annual_Salary__c { get; set; }
        public string Borrower_Employer_Name__c { get; set; }
        public string Borrower_Occupation__c { get; set; }
        public string Borrower_Occupation_Other__c { get; set; }
        public string Borrower_Other_Income__c { get; set; }
        public string Borrower_Other_Income_Source__c { get; set; }
        public string Borrower_Housing_Status__c { get; set; }
        public string Borrower_Rent__c { get; set; }
        public string Borrower_CD_Monthly_Debt__c { get; set; }
        public string Borrower_DTI_Percentage__c { get; set; }
        public string Cosigner_Employment__c { get; set; }
        public string Cosigner_Annual_Salary__c { get; set; }
        public string Cosigner_Employer_Name__c { get; set; }
        public string Cosigner_Occupation__c { get; set; }
        public string Cosigner_Occupation_Other__c { get; set; }
        public string Cosigner_Other_Income__c { get; set; }
        public string Cosigner_Other_Income_Source__c { get; set; }
        public string Cosigner_Housing_Status__c { get; set; }
        public string Cosigner_Rent__c { get; set; }
        public string Cosigner_CD_Monthly_Debt__c { get; set; }
        public string Cosigner_DTI_Percentage__c { get; set; }

        public static string RecordType_Id = "012G0000001QW9G";

        public Opportunity()
        {
            RecordTypeId = RecordType_Id;
        }

        public Opportunity(CampusDoorRecord record)
        {
            if (!string.IsNullOrWhiteSpace(record.AccountId))
                AccountId = record.AccountId;

            Name = string.Format("{0} {1}-", record.BorrowerFirstName, record.BorrowerLastName);
            Lender__c = record.LenderBranchName;
            Program_Name__c = record.ProgramName;
            Application_ID__c = record.ApplicationID;
            School_Name__c = record.SchoolName;
            Requested_Loan_Amount__c = record.RequestedLoanAmount; //Decimal.Parse(record.RequestedLoanAmount);
            Certified_Loan_Amount__c = record.CertifiedLoanAmount; //Decimal.Parse(record.CertifiedLoanAmount);
            Current_Status__c = record.CurrentStatus;
            Current_State__c = record.CurrentState;
            //Current_Status_Date__c = string.IsNullOrWhiteSpace(record.CurrentStatusDate) ? null : DateTime.Parse(record.CurrentStatusDate);
            //BorrowerAAReason1__c = record.BorrowerAAReason1;
            //BorrowerAAReason2__c = record.BorrowerAAReason2;
            //BorrowerAAReason3__c = record.BorrowerAAReason3;
            //BorrowerAAReason4__c = record.BorrowerAAReason4;
            Credit_Tier__c = record.CreditTier;
            Rate_Type__c = record.RateType;
            Interest_Rate__c = record.InterestRate;
            Repayment_Term__c = record.RepaymentTerm; //Int32.Parse(record.RepaymentTerm);
            Borrower_Employment__c = record.Borrower_Employment;
            Borrower_Annual_Salary__c = record.Borrower_Annual_Salary; //Decimal.Parse(record.Borrower_Annual_Salary);
            Borrower_Employer_Name__c = record.Borrower_Employer_Name;
            Borrower_Occupation__c = record.Borrower_Occupation;
            Borrower_Occupation_Other__c = record.Borrower_Occupation_Other;
            Borrower_Other_Income__c = record.Borrower_Other_Income; //Decimal.Parse(record.Borrower_Other_Income);
            Borrower_Other_Income_Source__c = record.Borrower_Other_Income_Source;
            Borrower_Housing_Status__c = record.BorrowerHousingStatus;
            Borrower_Rent__c = record.BorrowerRent; //Decimal.Parse(record.BorrowerRent);
            Borrower_CD_Monthly_Debt__c = record.BorrowerTotalCDCalculatedMonthlyDebt; //Decimal.Parse(record.BorrowerTotalCDCalculatedMonthlyDebt);
            Borrower_DTI_Percentage__c = record.BorrowerDTIPercentage;
            Cosigner_Employment__c = record.Cosigner_Employment;
            Cosigner_Annual_Salary__c = record.Cosigner_Annual_Salary; //Decimal.Parse(record.Cosigner_Annual_Salary);
            Cosigner_Employer_Name__c = record.Cosigner_Employer_Name;
            Cosigner_Occupation__c = record.Cosigner_Occupation;
            Cosigner_Occupation_Other__c = record.Cosigner_Occupation_Other;
            Cosigner_Other_Income__c = record.Cosigner_Other_Income; //Decimal.Parse(record.Cosigner_Other_Income);
            Cosigner_Other_Income_Source__c = record.Cosigner_Other_Income_Source;
            Cosigner_Housing_Status__c = record.CosignerHousingStatus;
            Cosigner_Rent__c = record.CosignerRent; //Decimal.Parse(record.CosignerRent);
            Cosigner_CD_Monthly_Debt__c = record.CosignerTotalCDCalculatedMonthlyDebt; //Decimal.Parse(record.CosignerTotalCDCalculatedMonthlyDebt);
            Cosigner_DTI_Percentage__c = record.CosignerDTIPercentage;

            if (!string.IsNullOrWhiteSpace(record.SubmittedDate))
                Submitted_Date__c = DateTime.Parse(record.SubmittedDate);

            if (!string.IsNullOrWhiteSpace(record.ActualDisbursementDate))
                Actual_Disbursement_Date__c = DateTime.Parse(record.ActualDisbursementDate);

            if (!string.IsNullOrWhiteSpace(record.LoanStartDate))
                Loan_Start_Date__c = DateTime.Parse(record.LoanStartDate);
        }

        public string StageName
        {
            get
            {
                switch (Current_Status__c)
                {
                    case "Created Portal Account":
                        return "Created Portal Account";
                    case "Started Application":
                        return "Started Application";
                    case "Left Portal":
                        return "Left Portal";
                    case "Incomplete":
                        return ParseIncompleteStatus();
                    case "Qualified":
                        return ParseQualifiedStatus();
                    case "NotQualified":
                        return ParseNotQualifiedStatus();
                    case "Registered":
                        return ParseRegisteredStatus();
                    case "Submitted":
                        return ParseSubmittedStatus();
                    case "Approved":
                        return ParseApprovedStatus();
                    case "Cancelled":
                        return ParseCancelledStatus();
                    case "Complete":
                        return ParseCompleteStatus();
                    case "DecisionPending":
                        return ParseDecisionPendingStatus();
                    case "Denied":
                        return ParseDeniedStatus();
                    case "Certified":
                        return ParseCertifiedStatus();
                    case "Initialized":
                        return ParseInitializedStatus();
                    case "LoanDocumentationComplete":
                        return ParseLoanDocumentationCompleteStatus();
                    case "LoanDocumentationPending":
                        return ParseLoanDocumentationPendingStatus();
                    case "PostInitializationComplete":
                        return ParsePostInitializationCompleteStatus();
                    case "PostInitializationPending":
                        return ParsePostInitializationPendingStatus();
                    case "PreDocumentationComplete":
                        return ParsePreDocumentationCompleteStatus();
                    case "PreDocumentationPending":
                        return ParsePreDocumentationPendingStatus();
                    case "SchoolCertificationComplete":
                        return ParseSchoolCertificationCompleteStatus();
                    case "SchoolCertificationPending":
                        return ParseSchoolCertificationPendingStatus();
                    case "LoanApproved":
                        return ParseLoanApprovedStatus();
                    default:
                        return "Created Portal Account";

                }
            }
        }

        public static string CommaDelimitedProperties()
        {
            var props = typeof(Opportunity).GetProperties();
            var list = props.Select(prop => prop.Name).ToList();
            return string.Join(",", list);
        }

        private string ParseIncompleteStatus()
        {
            if (Current_State__c == "Incomplete")
            {
                return "CD App. - Mid-Application";
            }
            return "CD App. - Mid-Application";
        }

        private string ParseQualifiedStatus()
        {
            if (Current_State__c == "Qualified")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseNotQualifiedStatus()
        {
            if (Current_State__c == "NotQualified")
            {
                return "CD App. - Declined Soft";
            }

            return "CD App. - Declined Soft";
        }

        private string ParseRegisteredStatus()
        {
            if (Current_State__c == "ExistingAccount")
            {
                return "CD App. - Mid-Application";
            }

            if (Current_State__c == "NewAccount")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseSubmittedStatus()
        {
            if (Current_State__c == "Submitted")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseApprovedStatus()
        {
            if (Current_State__c == "MovedToFulfillment")
            {
                return "CD App. - Approved Accepted";
            }

            if (Current_State__c == "OfferAccepted")
            {
                return "CD App. - Approved Accepted";
            }

            if (Current_State__c == "OfferOnHold")
            {
                return "CD App. - Approved";
            }

            if (Current_State__c == "OfferPending")
            {
                return "CD App. - Approved";
            }

            return "CD App. - Approved";
        }

        private string ParseCancelledStatus()
        {
            if (Current_State__c == "BorrowerDTI")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "BorrowerDTIReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CombinedDTI")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CombinedDTIReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CosignerDTIReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CreditExpired")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CustomerWithdrawn")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpiredApplicationIncomplete")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpireDecisionPendingDocs")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpireDecisionPendingIncome")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpiredError")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpiredGray")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpiredOfferPending")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "OfferDeclined")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ProgramRequirementsNoReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ProgramRequirementsReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "SchoolRequestNoReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "SchoolRequestReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CertExpired")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CertifiedIneligible")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CreditExpired")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CustomerRequest")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "CustomerWithdrawn")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "DuplicateApplication")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExcessiveLoanDebt")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ExpiredOffer")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "LenderRequested")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "LoanDocsExpired")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "OfferDeclined")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "OtherFraud")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ProgramRequirementsNoReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "ProgramRequirementsReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "SchoolRequestNoReapply")
            {
                return "CD App. - Declined Soft";
            }

            if (Current_State__c == "SchoolRequestReapply")
            {
                return "CD App. - Declined Soft";
            }

            return "CD App. - Declined Soft";
        }

        private string ParseCompleteStatus()
        {
            if (Current_State__c == "Complete")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseDecisionPendingStatus()
        {
            if (Current_State__c == "ApplicationError")
            {
                return "CD App. - Decision Pending";
            }

            if (Current_State__c == "CreditBureauError")
            {
                return "CD App. - Decision Pending";
            }

            if (Current_State__c == "CreditBureauUnavailable")
            {
                return "CD App. - Decision Pending";
            }

            if (Current_State__c == "Documentation")
            {
                return "CD App. - Decision Pending";
            }

            if (Current_State__c == "Gray")
            {
                return "CD App. - Decision Pending";
            }

            if (Current_State__c == "Income")
            {
                return "CD App. - Decision Pending";
            }

            if (Current_State__c == "LoanLimit")
            {
                return "CD App. - Decision Pending";
            }

            return "CD App. - Decision Pending";
        }

        private string ParseDeniedStatus()
        {
            if (Current_State__c == "HardDenied")
            {
                return "CD App. - Declined Hard";
            }

            if (Current_State__c == "SoftDenied")
            {
                return "CD App. - Declined Soft";
            }

            return "CD App. - Declined Soft";
        }

        private string ParseCertifiedStatus()
        {
            if (Current_State__c == "OfferAccepted")
            {
                return "CD App. - Approved Accepted";
            }

            if (Current_State__c == "OfferOnHold")
            {
                return "CD App. - Approved";
            }

            if (Current_State__c == "OfferPending")
            {
                return "CD App. - Approved";
            }

            if (Current_State__c == "ChecklistCreated")
            {
                return "CD App. - Approved";
            }

            return "CD App. - Approved";
        }

        private string ParseInitializedStatus()
        {
            if (Current_State__c == "ChecklistCreated")
            {
                return "CD App. - Mid-Application";
            }

            if (Current_State__c == "Initialized")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseLoanDocumentationCompleteStatus()
        {
            if (Current_State__c == "LoanDocumentationComplete")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseLoanDocumentationPendingStatus()
        {
            if (Current_State__c == "LoanDocumentationPending")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParsePostInitializationCompleteStatus()
        {
            if (Current_State__c == "PostInitializationComplete")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParsePostInitializationPendingStatus()
        {
            if (Current_State__c == "PostInitializationPending")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParsePreDocumentationCompleteStatus()
        {
            if (Current_State__c == "PreDocumentationComplete")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParsePreDocumentationPendingStatus()
        {
            if (Current_State__c == "PreDocumentationPending")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseSchoolCertificationCompleteStatus()
        {
            if (Current_State__c == "SchoolCertificationComplete")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseSchoolCertificationPendingStatus()
        {
            if (Current_State__c == "SchoolCertificationPending")
            {
                return "CD App. - Mid-Application";
            }

            return "CD App. - Mid-Application";
        }

        private string ParseLoanApprovedStatus()
        {
            if (Current_State__c == "FullyDisbursed")
            {
                return "CD App. - Funded, In Servicing";
            }

            if (Current_State__c == "PartiallyDisbursed")
            {
                return "CD App. - Disbursed";
            }

            if (Current_State__c == "PendingDisbursement")
            {
                return "CD App. - Disbursed";
            }

            if (Current_State__c == "ReceiptPeriodPending")
            {
                return "CD App. - Disbursed";
            }

            if (Current_State__c == "RightToCancelPeriodPending")
            {
                return "CD App. - Disbursed";
            }

            return "CD App. - Funded, In Servicing";
        }
    }
}
