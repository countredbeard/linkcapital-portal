﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class PortalBorrowerPersonAccount : SimplePersonAccount
    {
        public PortalBorrowerPersonAccount()
        {
            RecordTypeId = "012G0000001QEF5";
        }
    }
}
