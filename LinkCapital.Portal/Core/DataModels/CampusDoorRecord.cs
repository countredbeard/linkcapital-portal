﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class CampusDoorRecord 
    {
        // the sfdc account id for this user
        public string AccountId { get; set; }

        public string LenderBranchName { get; set; }

        public string Lender { get; set; }
        public string ProgramName { get; set; }
       public string ApplicationID { get; set; }
       public string LoanSequence { get; set; }
       public string ReferralID { get; set; }
       public string CommonLineUniqueIdentifier { get; set; }
       public string DOESchoolCode { get; set; }
       public string SchoolName { get; set; }
       public string CostOfAttendence { get; set; }
       public string EstimatedFinancialAid { get; set; }
       public string RecommendedMaximumLoanAmount { get; set; }
       public string RequestedLoanAmount { get; set; }
       public string CertifiedLoanAmount { get; set; }
       public string IncompleteDate { get; set; }
       public string SubmittedDate { get; set; }
       public string ApprovalDisclosureDate { get; set; }
       public string FinalCertificationDat { get; set; }
       public string FinalCreditScoreDate { get; set; }
       public string ActualDisbursementDate { get; set; }
       public string CurrentStatus { get; set; }
       public string CurrentState { get; set; }
       public string CurrentStatusDate { get; set; }
       public string BorrowerAAReason1 { get; set; }
       public string BorrowerAAReason2 { get; set; }
       public string BorrowerAAReason3 { get; set; }
       public string BorrowerAAReason4 { get; set; }
       public string PromNoteVersion { get; set; }
       public string CreditTier { get; set; }
       public string RateType { get; set; }
       public string InterestRate { get; set; }
       public string APR_FinalDisclosure { get; set; }
       public string Servicer { get; set; }
       public string RepaymentTerm { get; set; }
       public string OriginationFeePercentage { get; set; }
       public string OriginationFeeAmountTotal { get; set; }
       public string LoanStartDate { get; set; }
       public string CertifiedEnrollmentPeriodStartDate { get; set; }
       public string CertfiedExpectedGradDate { get; set; }
       public string BorrowerLoanPersonInfoSSN { get; set; }
       public string BorrowerDateofBirth { get; set; }
        public string BorrowerFirstName { get; set; }
        public string BorrowerMiddleInitial { get; set; }
        public string BorrowerLastName { get; set; }
        public string BorrowerPhoneNumber { get; set; }
        public string BorrowerEmailAddress { get; set; }
        public string BorrowerPrimaryAddress { get; set; }
        public string Borrower_City { get; set; }
        public string Borrower_State { get; set; }
        public string Borrower_Zip_Code { get; set; }
        public string eSign_Election { get; set; }
        public string Academic_Year { get; set; }
        public string EnrollmentPeriodStart1 { get; set; }
        public string EnrollmentPeriodEnd { get; set; }
        public string MajorEnumText { get; set; }
        public string Borrower_Employment { get; set; }
        public string Borrower_Annual_Salary { get; set; }
        public string Borrower_Employer_Name { get; set; }
        public string Borrower_Occupation { get; set; }
        public string Borrower_Occupation_Other { get; set; }
        public string Borrower_Employer_Phone_Numbe { get; set; }
        public string Borrower_Employment_Length { get; set; }
        public string Borrower_Other_Income { get; set; }
        public string Borrower_Other_Income_Source { get; set; }
        public string BorrowerHousingStatus { get; set; }
        public string BorrowerRent { get; set; }
        public string BorrowerTotalCDCalculatedMonthlyDebt { get; set; }
        public string BorrowerDTIPercentage { get; set; }
        public string Cosigner_SSN { get; set; }
        public string Cosigner_First_Name { get; set; }
        public string Cosigner_Middle_Name { get; set; }
        public string Cosigner_Last_Name { get; set; }
        public string Cosigner_Phone_Number { get; set; }
        public string Cosigner_Email_Address { get; set; }
        public string Cosigner_Street_Address { get; set; }
        public string Cosigner_City { get; set; }
        public string Cosigner_State { get; set; }
        public string Cosigner_Zip_Code { get; set; }
        public string Cosigner_Employment { get; set; }
        public string Cosigner_Annual_Salary { get; set; }
        public string Cosigner_Employer_Name { get; set; }
        public string Cosigner_Occupation { get; set; }
        public string Cosigner_Occupation_Other { get; set; }
        public string Cosigner_Employer_Phone_Number { get; set; }
        public string Cosigner_Employment_Length { get; set; }
        public string Cosigner_Other_Income { get; set; }
        public string Cosigner_Other_Income_Source { get; set; }
        public string CosignerHousingStatus { get; set; }
        public string CosignerRent { get; set; }
        public string CosignerTotalCDCalculatedMonthlyDebt { get; set; }
        public string CosignerDTIPercentage { get; set; }
        public string PendingDisbursementAmount1 { get; set; }
        public string PendingDisbursementAmount2 { get; set; }
        public string PendingDisbursementAmount3 { get; set; }
        public string PendingDisbursementAmount4 { get; set; }
        public string PendingDisbursementAmount5 { get; set; }
        public string PendingDisbursementAmount6 { get; set; }
        public string PendingDisbursementAmount7 { get; set; }
        public string PendingDisbursementAmount8 { get; set; }
        public string PendingDisbursementAmount9 { get; set; }
        public string PendingDisbursementDate1 { get; set; }
        public string PendingDisbursementDate2 { get; set; }
        public string PendingDisbursementDate3 { get; set; }
        public string PendingDisbursementDate4 { get; set; }
        public string PendingDisbursementDate5 { get; set; }
        public string PendingDisbursementDate6 { get; set; }
        public string PendingDisbursementDate7 { get; set; }
        public string PendingDisbursementDate8 { get; set; }
        public string PendingDisbursementDate9 { get; set; }
        public string Disbursement_Info_1 { get; set; }
        public string Disbursement_Info_2 { get; set; }
        public string Disbursement_Info_3 { get; set; }
        public string Disbursement_Info_4 { get; set; }
        public string Disbursement_Info_5 { get; set; }
        public string Disbursement_Info_6 { get; set; }
        public string Disbursement_Info_7 { get; set; }
        public string Disbursement_Info_8 { get; set; }
        public string Disbursement_Info_9 { get; set; }
        public string disbursementdate1 { get; set; }
        public string disbursementdate2 { get; set; }
        public string disbursementdate3 { get; set; }
        public string disbursementdate4 { get; set; }
        public string disbursementdate5 { get; set; }
        public string disbursementdate6 { get; set; }
        public string disbursementdate7 { get; set; }
        public string disbursementdate8 { get; set; }
        public string disbursementdate9 { get; set; }
        public string Borrower_Reference_1_First_Name { get; set; }
        public string Borrower_Reference_1_Middle_Initial { get; set; }
        public string Borrower_Reference_1_Last_Name { get; set; }
        public string Borrower_Reference_1_Suffix { get; set; }
        public string Borrower_Reference_1_Permanent_Address { get; set; }
        public string Borrower_Reference_1_AptSuiteFloor_Number  { get; set; }
        public string Borrower_Reference_1_City { get; set; }
        public string Borrower_Reference_1_StateTerritory { get; set; }
        public string Borrower_Reference_1_Zip { get; set; }
        public string Borrower_Reference_1_Phone_Number { get; set; }
        public string Borrower_Reference_1_Relationship { get; set; }
        public string Cosigner_Reference_1_First_Name { get; set; }
        public string Cosigner_Reference_1_Middle_Initial { get; set; }
        public string Cosigner_Reference_1_Last_Name { get; set; }
        public string Cosigner_Reference_1_Suffix { get; set; }
        public string Cosigner_Reference_1_Permanent_Address { get; set; }
        public string Cosigner_Reference_1_AptSuiteFloor_Number { get; set; }
        public string Cosigner_Reference_1_City { get; set; }
        public string Cosigner_Reference_1_StateTerritory { get; set; }
        public string Cosigner_Reference_1_Zip { get; set; }
        public string Cosigner_Reference_1_Phone_Number { get; set; }
        public string Cosigner_Reference_1_Relationship { get; set; }
        public string R1 { get; set; }
        public string R2 { get; set; }
        public string R3 { get; set; }
        public string R4 { get; set; }
        public string R5 { get; set; }
        public string R6 { get; set; }
        public string R7 { get; set; }
        public string R8 { get; set; }
        public string R9 { get; set; }
        public string R10 { get; set; }
        public string R11 { get; set; }
        public string R12 { get; set; }
        public string R13 { get; set; }
        public string R14 { get; set; }
        public string R15 { get; set; }
        public string R16 { get; set; }
        public string R17 { get; set; }
        public string R18 { get; set; }
        public string R19 { get; set; }
        public string R20 { get; set; }
        public string R21 { get; set; }
        public string R22 { get; set; }
        public string R23 { get; set; }

        public CampusDoorRecord() { }

        public CampusDoorRecord(string csv)
        {
            LenderBranchName = "Link Capital";

            var parser = new TextFieldParser(new StringReader(csv))
            {
                HasFieldsEnclosedInQuotes = true
            };

            parser.SetDelimiters(",");

            var fields = parser.ReadFields();

            Lender = fields[0];
            ProgramName = fields[1];
            ApplicationID = fields[2];
            LoanSequence = fields[3];
            ReferralID = fields[4];
            CommonLineUniqueIdentifier = fields[5];
            DOESchoolCode = fields[6];
            SchoolName = fields[7];
            CostOfAttendence = fields[8];
            EstimatedFinancialAid = fields[9];
            RecommendedMaximumLoanAmount = fields[10];
            RequestedLoanAmount = fields[11];
            CertifiedLoanAmount = fields[12];
            IncompleteDate = fields[13];
            SubmittedDate = fields[14];
            ApprovalDisclosureDate = fields[15];
            FinalCertificationDat = fields[16];
            FinalCreditScoreDate = fields[17];
            ActualDisbursementDate = fields[18];
            CurrentStatus = fields[19];
            CurrentState = fields[20];
            CurrentStatusDate = fields[21];
            BorrowerAAReason1 = fields[22];
            BorrowerAAReason2 = fields[23];
            BorrowerAAReason3 = fields[24];
            BorrowerAAReason4 = fields[25];
            PromNoteVersion = fields[26];
            CreditTier = fields[27];
            RateType = fields[28];
            InterestRate = fields[29];
            APR_FinalDisclosure = fields[30];
            Servicer = fields[31];
            RepaymentTerm = fields[32];
            OriginationFeePercentage = fields[33];
            OriginationFeeAmountTotal = fields[34];
            LoanStartDate = fields[35];
            CertifiedEnrollmentPeriodStartDate = fields[36];
            CertfiedExpectedGradDate = fields[37];
            BorrowerLoanPersonInfoSSN = fields[38];
            BorrowerDateofBirth = fields[39];
            BorrowerFirstName = fields[40];
            BorrowerMiddleInitial = fields[41];
            BorrowerLastName = fields[42];
            BorrowerPhoneNumber = fields[43];
            BorrowerEmailAddress = fields[44];
            BorrowerPrimaryAddress = fields[45];
            Borrower_City = fields[46];
            Borrower_State = fields[47];
            Borrower_Zip_Code = fields[48];
            eSign_Election = fields[49];
            Academic_Year = fields[50];
            EnrollmentPeriodStart1 = fields[51];
            EnrollmentPeriodEnd = fields[52];
            MajorEnumText = fields[53];
            Borrower_Employment = fields[54];
            Borrower_Annual_Salary = fields[55];
            //Borrower_Employment = splitLine[56];
            //Borrower_Annual_Salary = splitLine[57];
            Borrower_Employer_Name = fields[56];
            Borrower_Occupation = fields[57];
            Borrower_Occupation_Other = fields[58];
            Borrower_Employer_Phone_Numbe = fields[59];
            Borrower_Employment_Length = fields[60];
            Borrower_Other_Income = fields[61];
            Borrower_Other_Income_Source = fields[62];
            BorrowerHousingStatus = fields[63];
            BorrowerRent = fields[64];
            BorrowerTotalCDCalculatedMonthlyDebt = fields[65];
            BorrowerDTIPercentage = fields[66];
            Cosigner_SSN = fields[67];
            Cosigner_First_Name = fields[68];
            Cosigner_Middle_Name = fields[69];
            Cosigner_Last_Name = fields[70];
            Cosigner_Phone_Number = fields[71];
            Cosigner_Email_Address = fields[72];
            Cosigner_Street_Address = fields[73];
            Cosigner_City = fields[74];
            Cosigner_State = fields[75];
            Cosigner_Zip_Code = fields[76];
            Cosigner_Employment = fields[77];
            Cosigner_Annual_Salary = fields[78];
            Cosigner_Employer_Name = fields[79];
            Cosigner_Occupation = fields[80];
            Cosigner_Occupation_Other = fields[81];
            Cosigner_Employer_Phone_Number = fields[82];
            Cosigner_Employment_Length = fields[83];
            Cosigner_Other_Income = fields[84];
            Cosigner_Other_Income_Source = fields[85];
            CosignerHousingStatus = fields[86];
            CosignerRent = fields[87];
            CosignerTotalCDCalculatedMonthlyDebt = fields[88];
            CosignerDTIPercentage = fields[89];
            PendingDisbursementAmount1 = fields[90];
            PendingDisbursementAmount2 = fields[91];
            PendingDisbursementAmount3 = fields[92];
            PendingDisbursementAmount4 = fields[93];
            PendingDisbursementAmount5 = fields[94];
            PendingDisbursementAmount6 = fields[95];
            PendingDisbursementAmount7 = fields[96];
            PendingDisbursementAmount8 = fields[97];
            PendingDisbursementAmount9 = fields[98];
            PendingDisbursementDate1 = fields[99];
            PendingDisbursementDate2 = fields[100];
            PendingDisbursementDate3 = fields[101];
            PendingDisbursementDate4 = fields[102];
            PendingDisbursementDate5 = fields[103];
            PendingDisbursementDate6 = fields[104];
            PendingDisbursementDate7 = fields[105];
            PendingDisbursementDate8 = fields[106];
            PendingDisbursementDate9 = fields[107];
            Disbursement_Info_1 = fields[108];
            Disbursement_Info_2 = fields[109];
            Disbursement_Info_3 = fields[110];
            Disbursement_Info_4 = fields[111];
            Disbursement_Info_5 = fields[112];
            Disbursement_Info_6 = fields[113];
            Disbursement_Info_7 = fields[114];
            Disbursement_Info_8 = fields[115];
            Disbursement_Info_9 = fields[116];
            disbursementdate1 = fields[117];
            disbursementdate2 = fields[118];
            disbursementdate3 = fields[119];
            disbursementdate4 = fields[120];
            disbursementdate5 = fields[121];
            disbursementdate6 = fields[122];
            disbursementdate7 = fields[123];
            disbursementdate8 = fields[124];
            disbursementdate9 = fields[125];
            Borrower_Reference_1_First_Name  = fields[126];
            Borrower_Reference_1_Middle_Initial  = fields[127];
            Borrower_Reference_1_Last_Name  = fields[128];
            Borrower_Reference_1_Suffix  = fields[129];
            Borrower_Reference_1_Permanent_Address  = fields[130];
            Borrower_Reference_1_AptSuiteFloor_Number   = fields[131];
            Borrower_Reference_1_City  = fields[132];
            Borrower_Reference_1_StateTerritory  = fields[133];
            Borrower_Reference_1_Zip  = fields[134];
            Borrower_Reference_1_Phone_Number  = fields[135];
            Borrower_Reference_1_Relationship  = fields[136];
            Cosigner_Reference_1_First_Name  = fields[137];
            Cosigner_Reference_1_Middle_Initial  = fields[138];
            Cosigner_Reference_1_Last_Name  = fields[139];
            Cosigner_Reference_1_Suffix  = fields[140];
            Cosigner_Reference_1_Permanent_Address  = fields[141];
            Cosigner_Reference_1_AptSuiteFloor_Number  = fields[142];
            Cosigner_Reference_1_City  = fields[143];
            Cosigner_Reference_1_StateTerritory  = fields[144];
            Cosigner_Reference_1_Zip  = fields[145];
            Cosigner_Reference_1_Phone_Number  = fields[146];
            Cosigner_Reference_1_Relationship  = fields[147];
            R1  = fields[148];
            R2  = fields[149];
            R3  = fields[150];
            R4  = fields[151];
            R5  = fields[152];
            R6  = fields[153];
            R7  = fields[154];
            R8  = fields[155];
            R9  = fields[156];
            R10  = fields[157];
            R11  = fields[158];
            R12  = fields[159];
            R13  = fields[160];
            R14  = fields[161];
            R15  = fields[162];
            R16  = fields[163];
            R17  = fields[164];
            R18  = fields[165];
            R19  = fields[166];
            R20  = fields[167];
            //R21  = splitLine[171];
            //R22  = splitLine[172];
            //R23  = splitLine[173];
            
            parser.Close();
        }
    }
}
