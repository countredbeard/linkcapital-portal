﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class BorrowerPersonAccount : PersonAccount
    {
        public BorrowerPersonAccount() : base()
        {
            RecordTypeId = "012G0000001QEF5";
        }

        public BorrowerPersonAccount(CampusDoorRecord record) : base()
        {
            FirstName = record.BorrowerFirstName;
            LastName = record.BorrowerLastName;
            PersonEmail = record.BorrowerEmailAddress;
            Phone = record.BorrowerPhoneNumber;
            PersonMailingStreet = record.BorrowerPrimaryAddress;
            PersonMailingCity = record.Borrower_City;
            PersonMailingState = record.Borrower_State;
            PersonMailingPostalCode = record.Borrower_Zip_Code;
            PersonMailingCountry = "USA";
            PersonBirthdate = string.IsNullOrWhiteSpace(record.BorrowerDateofBirth) ? DateTime.MinValue : DateTime.Parse(record.BorrowerDateofBirth);
            Reference_First_Name__pc = record.Borrower_Reference_1_First_Name;
            Reference_Last_Name__pc = record.Borrower_Reference_1_Last_Name;
            Reference_Permanent_Address__pc = record.Borrower_Reference_1_Permanent_Address;
            Reference_Address_Line_2__pc = record.Borrower_Reference_1_AptSuiteFloor_Number;
            Reference_City__pc = record.Borrower_Reference_1_City;
            Reference_State_Territory__pc = record.Borrower_Reference_1_StateTerritory;
            Reference_Zip__pc = record.Borrower_Reference_1_Zip;
            Reference_Phone__pc = record.Borrower_Reference_1_Phone_Number;
            Reference_Relationship__pc = record.Borrower_Reference_1_Relationship;
        }
    }
}
