﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public abstract class PersonAccount : SimplePersonAccount
    {
        //public const bool IsPersonType = true;

        // advanced account fields
        public string Phone { get; set; }
        public string PersonMailingStreet { get; set; }
        public string PersonMailingCity { get; set; }
        public string PersonMailingState { get; set; }
        public string PersonMailingCountry { get; set; }
        public string PersonMailingPostalCode { get; set; }
        public DateTime PersonBirthdate { get; set; }

        // contact mapped fields
        public string Reference_First_Name__pc { get; set; }
        public string Reference_Last_Name__pc { get; set; }
        //public string Reference_Middle_Initial__pc { get; set; }
        public string Reference_Permanent_Address__pc { get; set; }
        public string Reference_Address_Line_2__pc { get; set; }
        public string Reference_City__pc { get; set; }
        public string Reference_State_Territory__pc { get; set; }
        public string Reference_Zip__pc { get; set; }
        public string Reference_Phone__pc { get; set; }
        public string Reference_Relationship__pc { get; set; }

        protected PersonAccount()
        {
            //SObjectTypeName = "Account";
        }

        public static string CommaDelimitedProperties()
        {
            var props = typeof (PersonAccount).GetProperties();
            var list = props.Select(prop => prop.Name).ToList();
            //var list = (from prop in props where prop.Name != "Id" select prop.Name).ToList();

            return string.Join(",", list);
        }
    }
}
