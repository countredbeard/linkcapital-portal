﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.DataModels
{
    public class AccountName
    {
        [JsonProperty(PropertyName = "salutation")]
        public string Salutation { get; set; }
        [JsonProperty(PropertyName = "first name")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "last name")]
        public string LastName { get; set; }
    }
}
