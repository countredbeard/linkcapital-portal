﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class FullOpportunity : Opportunity
    {
        public DateTime CloseDate { get; set; }

        public FullOpportunity(Opportunity opp)
        {
            AccountId = opp.AccountId;
            Name = opp.Name;
            Lender__c = opp.Lender__c;
            Program_Name__c = opp.Program_Name__c;
            Application_ID__c = opp.Application_ID__c;
            School_Name__c = opp.School_Name__c;
            Requested_Loan_Amount__c = opp.Requested_Loan_Amount__c; //Decimal.Parse(opp.RequestedLoanAmount);
            Certified_Loan_Amount__c = opp.Certified_Loan_Amount__c; //Decimal.Parse(opp.CertifiedLoanAmount);
            Current_Status__c = opp.Current_Status__c;
            Current_State__c = opp.Current_State__c;
            Credit_Tier__c = opp.Credit_Tier__c;
            Rate_Type__c = opp.Rate_Type__c;
            Interest_Rate__c = opp.Interest_Rate__c;
            Repayment_Term__c = opp.Repayment_Term__c; //Int32.Parse(opp.RepaymentTerm);
            Borrower_Employment__c = opp.Borrower_Employment__c;
            Borrower_Employer_Name__c = opp.Borrower_Employer_Name__c;
            Borrower_Occupation__c = opp.Borrower_Occupation__c;
            Borrower_Occupation_Other__c = opp.Borrower_Occupation_Other__c;
            Borrower_Other_Income__c = opp.Borrower_Other_Income__c; //Decimal.Parse(opp.Borrower_Other_Income);
            Borrower_Other_Income_Source__c = opp.Borrower_Other_Income_Source__c;
            Borrower_Housing_Status__c = opp.Borrower_Housing_Status__c;
            Borrower_Rent__c = opp.Borrower_Rent__c; //Decimal.Parse(opp.BorrowerRent);
            Borrower_CD_Monthly_Debt__c = opp.Borrower_CD_Monthly_Debt__c; //Decimal.Parse(opp.BorrowerTotalCDCalculatedMonthlyDebt);
            Borrower_DTI_Percentage__c = opp.Borrower_DTI_Percentage__c;
            Cosigner_Employment__c = opp.Cosigner_Employment__c;
            Cosigner_Annual_Salary__c = opp.Cosigner_Annual_Salary__c; //Decimal.Parse(opp.Cosigner_Annual_Salary);
            Cosigner_Employer_Name__c = opp.Cosigner_Employer_Name__c;
            Cosigner_Occupation__c = opp.Cosigner_Occupation__c;
            Cosigner_Occupation_Other__c = opp.Cosigner_Occupation_Other__c;
            Cosigner_Other_Income__c = opp.Cosigner_Other_Income__c; //Decimal.Parse(opp.Cosigner_Other_Income);
            Cosigner_Other_Income_Source__c = opp.Cosigner_Other_Income_Source__c;
            Cosigner_Housing_Status__c = opp.Cosigner_Housing_Status__c;
            Cosigner_Rent__c = opp.Cosigner_Rent__c; //Decimal.Parse(opp.CosignerRent);
            Cosigner_CD_Monthly_Debt__c = opp.Cosigner_CD_Monthly_Debt__c; //Decimal.Parse(opp.CosignerTotalCDCalculatedMonthlyDebt);
            Cosigner_DTI_Percentage__c = opp.Cosigner_DTI_Percentage__c;
            Submitted_Date__c = opp.Submitted_Date__c;
            Actual_Disbursement_Date__c = opp.Actual_Disbursement_Date__c;
            Loan_Start_Date__c = opp.Loan_Start_Date__c;

            CloseDate = DateTime.Parse(DateTime.Now.AddMonths(1).ToString("M/d/yy"));
        }
    }
}
