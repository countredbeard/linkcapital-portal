﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class PortalStorage
    {
        public string LeadStatus { get; set; }
        public PortalBorrowerPersonAccount Account { get; set; }
        public string Id { get; set; }
        public bool IsNew { get { return string.IsNullOrWhiteSpace(Id); } }
    }
}
