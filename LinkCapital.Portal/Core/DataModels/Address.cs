﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.DataModels
{
    public class Address
    {
        [JsonProperty(PropertyName = "City")]
        public string MailingCity { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string MailingCountry { get; set; }
        //public string MailingLatitude { get; set; }
        //public string MailingLongitude { get; set; }
        [JsonProperty(PropertyName = "PostalCode")]
        public string MailingPostalCode { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string MailingState { get; set; }

        //public string MailingStateCode { get; set; }
        [JsonProperty(PropertyName = "Street")]
        public string MailingStreet { get; set; }

        public Address() { }
    }
}
