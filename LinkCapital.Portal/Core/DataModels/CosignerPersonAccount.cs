﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class CosignerPersonAccount : PersonAccount
    {
        public CosignerPersonAccount() : base()
        {
            RecordTypeId = "012G0000001QWFR";
        }
    }
}
