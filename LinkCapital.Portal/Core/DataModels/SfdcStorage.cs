﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class SfdcStorage
    {
        //public bool BorrowerPersonAccountIsNew { get; set; }
        //public bool CosignerPersonAccountIsNew { get; set; }
        //public bool OpportunityIsNew { get; set; }
        //public bool LinkLoanIsNew { get; set; }

        public string BorrowerPersonAccountId { get; set; }
        public string CosignerPersonAccountId { get; set; }
        public string OpportunityId { get; set; }
        public string LinkLoanId { get; set; }

        public BorrowerPersonAccount BorrowerPersonAccount { get; set; }
        public CosignerPersonAccount CosignerPersonAccount { get; set; }
        public Opportunity Opportunity { get; set; }
        public LinkLoan LinkLoan { get; set; }

        // todo: remove this when unneeded
        public bool NewRecord { get; set; }
        public string SFID { get; set; }
        public Contact AssocitedContact { get; set; }
    }
}
