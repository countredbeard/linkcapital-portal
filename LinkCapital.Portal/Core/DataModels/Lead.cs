﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataModels
{
    public class Lead : SfdcBaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public static string RecordType_Id = "012G0000001QEUP";

        public Lead()
        {
            RecordTypeId = RecordType_Id;
        }
    }
}
