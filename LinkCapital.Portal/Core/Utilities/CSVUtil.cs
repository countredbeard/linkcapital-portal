﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Utilities
{
    public static class CSVUtil
    {
        public static List<Dictionary<string, string>>  ParseFile(string filename, bool hasFieldsEnclosedInQuotes = false)
        {
            var parsedData = new List<Dictionary<string, string>>();

            using (var fileStream = new StreamReader(filename))
            {
                var lines = new List<string>();

                while (fileStream.EndOfStream == false)
                {
                    lines.Add(fileStream.ReadLine());
                }

                parsedData = Parse(lines, hasFieldsEnclosedInQuotes);
            }

            return parsedData;
        }

        public static List<Dictionary<string, string>> Parse(IEnumerable<string> lines, bool hasFieldsEnclosedInQuotes = false)
        {
            var parsedData = new List<Dictionary<string, string>>();
            var lineIsHeaders = true;
            var headers = new Dictionary<int, string>();

            foreach (var csv in lines)
            {
                if (string.IsNullOrWhiteSpace(csv)) continue;

                var parser = new TextFieldParser(new StringReader(csv))
                {
                    HasFieldsEnclosedInQuotes = hasFieldsEnclosedInQuotes
                };

                parser.SetDelimiters(",");

                var fields = parser.ReadFields();

                if (fields == null || fields.Length <= 0) continue;

                if (lineIsHeaders)
                {
                    for (var i = 0; i < fields.Length; i++)
                    {
                        headers.Add(i, fields[i]);
                    }

                    lineIsHeaders = false;

                    continue;
                }
                var csvData = headers.ToDictionary(header => header.Value, header => fields[header.Key]);

                parsedData.Add(csvData);
            }
            
            return parsedData;
        }
    }
}
