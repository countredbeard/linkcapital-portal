﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Core.Utilities.Extensions
{
	public static class StringExtension
	{
		/// <summary>
		/// Converts the given string to an object of the given generic type.
		/// Returns the default target type value if the string is null or empty.
		/// Throws an exception if the conversion fails.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceString"></param>
		/// <returns></returns>
		public static T ConvertTo<T>(this string sourceString)
		{
			T targetObject = default(T);

			if (!string.IsNullOrEmpty(sourceString))
			{
				Type targetType = typeof(T);

				if (targetType.IsEnum)
				{
					targetObject = (T)Enum.Parse(targetType, sourceString);
				}
				else
				{
					TypeConverter converter = TypeDescriptor.GetConverter(targetType);
					targetObject = (T)converter.ConvertFromString(sourceString);
				}
			}

			return targetObject;
		}
	}
}
