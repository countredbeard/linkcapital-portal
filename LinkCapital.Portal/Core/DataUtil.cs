﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Common;
using Salesforce.Common.Models;
using Salesforce.Force;
using Core.DataModels;
using Core.EntityModels;

namespace Core
{
    public class DataUtil
    {
        public static string SFClientID { get; set; }
        public static string SFClientSecret { get; set; }
        public static string SFUserName { get; set; }
        public static string SFPassword { get; set; }
        public static string EncyptionKey { get; set; }

        /*public static List<CampusDoorRecord> LoadDataFromCapusDoorLocalFS(string file)
        {
            // HACK: a temp implantation of a file load using local test file
            List<string> RawLines = new List<string>();
            List<CampusDoorRecord> ProcessedLines = new List<CampusDoorRecord>();
            using(StreamReader fileStrem = new StreamReader(file))
            {
                while(fileStrem.EndOfStream == false)
                {
                    RawLines.Add(fileStrem.ReadLine());
                }
            }

            RawLines.RemoveAt(0);
            RawLines.RemoveAt(0);
            RawLines.RemoveAt(0);

            foreach(string currentLine in RawLines)
            {
                ProcessedLines.Add(new CampusDoorRecord(currentLine));
            }

            return ProcessedLines;
        }*/

        public static void StoreInSalesForce(List<CampusDoorRecord> inbound)
        {
            List<SfdcStorage> ItemsToAddOrUpdate = new List<SfdcStorage>();

            foreach(CampusDoorRecord currentRec in inbound)
            {
                SfdcStorage itemToPopulate = new SfdcStorage();
                itemToPopulate.AssocitedContact = new Contact();
                itemToPopulate.AssocitedContact.MailingAddress = new Address();

                itemToPopulate.AssocitedContact.Email = currentRec.BorrowerEmailAddress;
                itemToPopulate.AssocitedContact.FirstName = currentRec.BorrowerFirstName;
                itemToPopulate.AssocitedContact.LastName = currentRec.BorrowerLastName;

                //StringBuilder AddressAssemble = new StringBuilder();
                //AddressAssemble.Append(currentRec.BorrowerPrimaryAddress).Append(",");
                //AddressAssemble.Append(currentRec.Borrower_City).Append(",");
                //AddressAssemble.Append(currentRec.Borrower_State).Append(",");
                //AddressAssemble.Append(currentRec.Borrower_Zip_Code);
                //itemToPopulate.AssocitedContact.MailingAddress = AddressAssemble.ToString();
                itemToPopulate.AssocitedContact.MailingAddress.MailingStreet = currentRec.BorrowerPrimaryAddress;
                itemToPopulate.AssocitedContact.MailingAddress.MailingCity = currentRec.Borrower_City;
                itemToPopulate.AssocitedContact.MailingAddress.MailingState = currentRec.Borrower_State;
                itemToPopulate.AssocitedContact.MailingAddress.MailingPostalCode = currentRec.Borrower_Zip_Code;

                string[] tempDOB = currentRec.BorrowerDateofBirth.Split('/');
                itemToPopulate.AssocitedContact.Birthdate = new DateTime(Convert.ToInt32(tempDOB[2]), Convert.ToInt32(tempDOB[0]), Convert.ToInt32(tempDOB[0]));
                
                itemToPopulate.AssocitedContact.Phone = currentRec.BorrowerPhoneNumber;
                //itemToPopulate.AssocitedContact.WorkPhone = currentRec.Borrower_Employer_Phone_Numbe;
                itemToPopulate.AssocitedContact.Lead_Status__c = DeriveLeadStatus(currentRec.CurrentStatus, currentRec.CurrentState);

                // check if we have a record for the given users
                itemToPopulate = FindContact(itemToPopulate).Result;

                ItemsToAddOrUpdate.Add(itemToPopulate);
            }

            foreach(SfdcStorage itemToStore in ItemsToAddOrUpdate)
            {
                if(itemToStore.NewRecord)
                {
                    Task<string> temp = StoreNewContact(itemToStore);
                    Task.WaitAll(temp);
                }
                else
                {
                    Task<SuccessResponse> reponse = UpdateContact(itemToStore);
                    Task.WaitAll(reponse);
                }

            }
        }

        protected static string DeriveLeadStatus(string currentStatus, string currentState)
        {
            //string leadStatus = string.Empty;

            // New, Mid-Application, Declined, Approved/Pre-Funding, Funded

            // HACK: some of this code for the moment is a best guess and needs to be approved by Link - Aaron V. 5/13/2015

            if (currentStatus == "Incomplete" && currentState == "Incomplete")
            {
                return "Mid-Application";  
            }

            if (currentStatus == "NotQualified" && currentState == "NotQualified")
            {
                return "Declined";
            }

            if (currentStatus == "Qualified" && currentState == "Qualified")
            {
                return "Approved/Pre-Funding";
            }

            if (currentStatus == "Registered" && currentState == "ExistingAccount")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Registered" && currentState == "NewAccount")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Submitted" && currentState == "Submitted")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Approved" && currentState == "MovedToFulfillment")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Approved" && currentState == "OfferAccepted")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Approved" && currentState == "OfferOnHold")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Approved" && currentState == "OfferPending")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Cancelled" && currentState == "BorrowerDTI")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "BorrowerDTIReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CombinedDTI")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CombinedDTIReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CosignerDTIReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CreditExpired")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CustomerWithdrawn")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpiredApplicationIncomplete")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpireDecisionPendingDocs")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpireDecisionPendingIncome")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpiredError")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpiredGray")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpiredOfferPending")
            {
                return "New";
            }

            if (currentStatus == "Cancelled" && currentState == "OfferDeclined")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "ProgramRequirementsNoReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "ProgramRequirementsReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "SchoolRequestNoReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "SchoolRequestReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Complete" && currentState == "Complete")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "ApplicationError")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "CreditBureauError")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "CreditBureauUnavailable")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "Documentation")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "Gray")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "Income")
            {
                return "Mid-Application";
            }

            if (currentStatus == "DecisionPending" && currentState == "LoanLimit")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Denied" && currentState == "HardDenied")
            {
                return "Declined";
            }

            if (currentStatus == "Denied" && currentState == "SoftDenied")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CertExpired")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CertifiedIneligible")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CreditExpired")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CustomerRequest")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "CustomerWithdrawn")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "DuplicateApplication")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "ExcessiveLoanDebt")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "ExpiredOffer")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "LenderRequested")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "LoanDocsExpired")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "OfferDeclined")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "OtherFraud")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "ProgramRequirementsNoReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "ProgramRequirementsReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "SchoolRequestNoReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Cancelled" && currentState == "SchoolRequestReapply")
            {
                return "Declined";
            }

            if (currentStatus == "Certified" && currentState == "OfferAccepted")
            {
                return "Approved/Pre-Funding";
            }

            if (currentStatus == "Certified" && currentState == "OfferOnHold")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Certified" && currentState == "OfferPending")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Certified" && currentState == "ChecklistCreated")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Initialized" && currentState == "ChecklistCreated")
            {
                return "Mid-Application";
            }

            if (currentStatus == "Initialized" && currentState == "Initialized")
            {
                return "Mid-Application";
            }

            if (currentStatus == "LoanDocumentationComplete" && currentState == "LoanDocumentationComplete")
            {
                return "Mid-Application";
            }

            if (currentStatus == "LoanDocumentationPending" && currentState == "LoanDocumentationPending")
            {
                return "Mid-Application";
            }

            if (currentStatus == "PostInitializationComplete" && currentState == "PostInitializationComplete")
            {
                return "Mid-Application";
            }

            if (currentStatus == "PostInitializationPending" && currentState == "PostInitializationPending")
            {
                return "Mid-Application";
            }

            if (currentStatus == "PreDocumentationComplete" && currentState == "PreDocumentationComplete")
            {
                return "Mid-Application";
            }

            if (currentStatus == "PreDocumentationPending" && currentState == "PreDocumentationPending")
            {
                return "Mid-Application";
            }

            if (currentStatus == "SchoolCertificationComplete" && currentState == "SchoolCertificationComplete")
            {
                return "Mid-Application";
            }

            if (currentStatus == "SchoolCertificationPending" && currentState == "SchoolCertificationPending")
            {
                return "Mid-Application";
            }

            if (currentStatus == "LoanApproved" && currentState == "FullyDisbursed")
            {
                return "Funded";
            }

            if (currentStatus == "LoanApproved" && currentState == "PartiallyDisbursed")
            {
                return "Funded";
            }

            if (currentStatus == "LoanApproved" && currentState == "PendingDisbursement")
            {
                return "Funded";
            }

            if (currentStatus == "LoanApproved" && currentState == "ReceiptPeriodPending")
            {
                return "Funded";
            }

            if (currentStatus == "LoanApproved" && currentState == "RightToCancelPeriodPending")
            {
                return "Funded";
            }

            throw new Exception("Not yet implemented");
        }

        public static async Task<SfdcStorage> FindContact(SfdcStorage recToFind)
        {
            QueryResult<Contact> recs = null;

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

                recs = await client.QueryAsync<Contact>("SELECT id, name, description FROM Contact WHERE email = '" + recToFind.AssocitedContact.Email + "'");
            }

            if(recs.Records.Count > 0)
            {
                recToFind.NewRecord = false;
                recToFind.SFID = recs.Records[0].ID;
            }
            else
            {
                recToFind.NewRecord = true;
            }

            return recToFind;
        }

        public static async Task<Contact> FindContact(string email)
        {
            QueryResult<Contact> recs = null;

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

                recs = await client.QueryAsync<Contact>("SELECT id, name, description, RecordTypeID, Email, Firstname, Lastname, Lead_Status__c, Phone, AccountId FROM Contact WHERE email = '" + email + "'");
                //recs = await client.QueryAsync<Contact>("SELECT * FROM Contact WHERE email = '" + email + "'");
            }
            if (recs.TotalSize > 0)
            {
                return recs.Records[0];
            }
            else
            {
                return null;
            }
        }

        public static async Task<List<Contact>> FindContacts()
        {
            QueryResult<Contact> recs = null;

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

                recs = await client.QueryAsync<Contact>("SELECT id, name, description, RecordTypeID, Email, Firstname, Lastname, Lead_Status__c, Phone, AccountId FROM Contact");
            }
            if (recs.TotalSize > 0)
            {
                return recs.Records.ToList<Contact>();
            }
            else
            {
                return null;
            }
        }

        public static async Task<string> StoreNewContact(SfdcStorage itemToStore)
        {
            string id = string.Empty;

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                
                 id = await client.CreateAsync("Contact", itemToStore);
            }

            return id;
        }

        public static async Task<string> StoreNewContact(ApplicationUser itemToStore)
        {
            // transer info from portal account to new sf contact
            Contact sfContact = new Contact();
            sfContact.FirstName = itemToStore.FirstName;
            sfContact.LastName = itemToStore.LastName;
            sfContact.Email = itemToStore.UserName;
            sfContact.Birthdate = DateTime.Parse("01/01/1901");
            sfContact.Lead_Status__c = "Has Portal Account";
            sfContact.RecordTypeID = "012G0000001QPdf";

            string id = string.Empty;

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

                id = await client.CreateAsync("Contact", sfContact);
            }

            return id;
        }

        public static async Task<SuccessResponse> UpdateContact(SfdcStorage itemToStore)
        {
            using (AuthenticationClient auth = new AuthenticationClient())
            {

                await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);


                return await client.UpdateAsync("Contact", itemToStore.SFID, itemToStore.AssocitedContact);
                
            }

            
        }

        public static async Task<SuccessResponse> UpdateContact(ApplicationUser itemToStore, bool autoMigrateAccount = true)
        {
            // automigrate will switch the account to a borrower account on sync

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                Contact toStore = FindContact(itemToStore.Email).Result;

                if (toStore != null)
                {
                    toStore.Lead_Status__c = itemToStore.LeadStatus;
                    toStore.FirstName = itemToStore.FirstName;
                    toStore.LastName = itemToStore.LastName;
                    toStore.Name = itemToStore.LastName + ", " + itemToStore.FirstName;
                    if (autoMigrateAccount) toStore.RecordTypeID = "012G0000001QPdf";
                    if (toStore.AccountID == null) toStore.AccountID = "001G000001pZaeb";

                    //if(toStore.acc)

                    if (toStore.Birthdate.Year < 1800) toStore.Birthdate = DateTime.Parse("1/1/1900");

                    await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                    ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                    //SimpleContact scStore = toStore;
                    return await client.UpdateAsync("Contact", itemToStore.UserSFDCID, new SimpleContact(toStore));
                }

                return null;

            }


        }

        public static async Task<SuccessResponse> UpdateContact(Contact itemToSotre, bool autoMigrateAccount = true)
        {
            // automigrate will switch the account to a borrower account on sync

            using (AuthenticationClient auth = new AuthenticationClient())
            {

                Contact toStore = itemToSotre;

                if (toStore != null)
                {
                    await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

                    ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                    //SimpleContact scStore = toStore;
                    return await client.UpdateAsync("Contact", toStore.ID, new SimpleContact(toStore));
                }

                return null;

            }


        }

        //public static async Task<List<Contact>> GetContactRecIDs(string recTypeToGet)
        //{
        //    QueryResult<Contact> recs = null;

        //    using (AuthenticationClient auth = new AuthenticationClient())
        //    {

        //        await auth.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword);

        //        ForceClient client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);

        //        recs = await client.QueryAsync<Contact>("SELECT id, name, description, RecordType.id FROM Contact");
        //    }
        //    if (recs.TotalSize > 0)
        //    {
        //        return recs.Records.ToList<Contact>();
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        //public static async Task<SuccessResponse> FixBadContacts()
        //{

        //}
    }
}
