﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.EntityModels;
using Core.Repositories;

namespace Core.Salesforce
{
    public class DataUtility
    {
        private string DbConnectionString { get; set; }

        private DataContext Context { get; set; }

        public IList<ApplicationUser> AllUsers { get; set; }

        public DataUtility(string connectionString)
        {
            DbConnectionString = connectionString;

            Context = new DataContext(DbConnectionString);

            AllUsers = Context.Users.Where(x => !x.IsCosigner && x.EmailConfirmed).ToList();
        }

        public ApplicationUser FindByEmail(string email)
        {
            return Context.Users.FirstOrDefault(x => x.Email == email);
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }
    }
}
