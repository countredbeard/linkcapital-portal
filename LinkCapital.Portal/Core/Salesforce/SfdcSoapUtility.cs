﻿using Core.Exceptions;
using Core.SFDC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;

namespace Core.Salesforce
{
    /// <summary>
    /// Some helpful documentation:
    /// https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_quickstart_steps_walk_through_code.htm
    /// https://developer.salesforce.com/page/Integrating_Force.com_with_Microsoft_.NET
    /// https://developer.salesforce.com/page/Integrating_Force.com_with_Microsoft_.NET
    /// https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_calls_convertlead.htm
    /// </summary>
    public class SfdcSoapUtility
    {
        private SforceService binding { get; set; }

        private string SfdcUsername { get; set; }
        private string SfdcPassword { get; set; }
        private string SfdcToken { get; set; }

        public SfdcSoapUtility(string username, string password, string token)
        {
            SfdcUsername = username;
            SfdcPassword = password;
            SfdcToken = token;

            binding = new SforceService {Timeout = 60000};

            if (!Login())
            {
                throw new SfdcSoapLoginException();
            }
        }

        public string ConvertLead(string id)
        {
            var leadConverts = new LeadConvert[1];
            leadConverts[0] = new LeadConvert { convertedStatus = "Converted", leadId = id };

            var convertResults = binding.convertLead(leadConverts);

            if (convertResults[0].success)
                return convertResults[0].accountId;

            throw new LeadConversionFailedException();
        }

        private bool Login()
        {
            LoginResult loginResult = null;

            try
            {
                loginResult = binding.login(SfdcUsername, SfdcPassword + SfdcToken);
            }
            catch (SoapException e)
            {
                loginResult = null;
                return false;
            }
            
            if (loginResult.passwordExpired)
            {
                return false;
            }

            binding.Url = loginResult.serverUrl;

            binding.SessionHeaderValue = new SessionHeader {sessionId = loginResult.sessionId};

            return true;
        }
    }
}
