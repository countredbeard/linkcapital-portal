﻿using Core.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Salesforce
{
    public class AccountDeletedException : Exception
    {
        public SimplePersonAccount Account { get; set; }

        public AccountDeletedException(SimplePersonAccount account)
        {
            this.Account = account;
        }
    }
}
