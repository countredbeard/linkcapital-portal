﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Salesforce.Common;
using Salesforce.Common.Models;
using Salesforce.Force;
using Core.DataModels;
using Core.Exceptions;
using Core.Repositories;

namespace Core.Salesforce
{
    public class SfdcUtility
    {
        public string SFClientID { get; set; }
        public string SFClientSecret { get; set; }
        public string SFSoapClientSecret { get; set; }
        public string SFUserName { get; set; }
        public string SFPassword { get; set; }

        private AuthenticationClient myAuth;

        public AuthenticationClient auth
        {
            get
            {
                if (myAuth == null)
                {
                    AuthenticationClient temp = new AuthenticationClient();
                    temp.UsernamePasswordAsync(SFClientID, SFClientSecret, SFUserName, SFPassword).Wait();
                    myAuth = temp;
                }

                return myAuth;
            }
        }

        private ForceClient myClient;

        public ForceClient client
        {
            get
            {
                if (myClient == null)
                {
                    myClient = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                }

                return myClient;
            }
        }
        
        public async Task<string> StoreCDInSfdc(CampusDoorRecord record)
        {
            // not a valid record
            if (string.IsNullOrWhiteSpace(record.Lender)) throw new Exception("This shouldn't have been blank...");

            var storage = new SfdcStorage
            {
                BorrowerPersonAccount = new BorrowerPersonAccount(record),
                BorrowerPersonAccountId = record.AccountId,
                Opportunity = new Opportunity(record)
            };
            
            if (string.IsNullOrWhiteSpace(storage.BorrowerPersonAccountId))
            {
                throw new PersonAccountNotFoundException();
            }
            
            Console.WriteLine("Attempting to update {0} {1} {2} {3}", storage.BorrowerPersonAccount.FirstName, storage.BorrowerPersonAccount.LastName, storage.BorrowerPersonAccount.PersonEmail, storage.BorrowerPersonAccountId);
            await UpdatePersonAccountAndOpportunity(storage);
            return storage.BorrowerPersonAccountId;
        }

        public async Task<string> StoreBLMInSfdc(BlmRecord record)
        {
            // not a valid record
            if (string.IsNullOrWhiteSpace(record.Lender)) throw new Exception("This shouldn't have been blank...");

            var storage = new SfdcStorage
            {
                BorrowerPersonAccount = new BorrowerPersonAccount(record),
                BorrowerPersonAccountId = record.AccountId,
                Opportunity = new Opportunity(record)
            };

            // only ping sfdc for an account if the db doesn't have one
            if (string.IsNullOrWhiteSpace(storage.BorrowerPersonAccountId))
            {
                var result = await FindBorrwerPersonAccountByProperties(storage.BorrowerPersonAccount.FirstName,
                storage.BorrowerPersonAccount.LastName, storage.BorrowerPersonAccount.PersonEmail);
                storage.BorrowerPersonAccountId = result.Id;
                storage.Opportunity.AccountId = result.Id;
            }

            Console.WriteLine("Attempting to update {0} {1} {2} {3}", storage.BorrowerPersonAccount.FirstName, storage.BorrowerPersonAccount.LastName, storage.BorrowerPersonAccount.PersonEmail, storage.BorrowerPersonAccountId);
            await UpdatePersonAccountAndOpportunity(storage);
            return storage.BorrowerPersonAccountId;
        }

        public async Task<string> StorePortalInSfdc(PortalStorage storage)
        {
            if (storage.IsNew)
            {
                // you can't use await in catch statements, so here's a workaround
                var personAccountNotFound = false;
                var leadNotFound = false;

                try
                {
                    var result =
                        await FindBorrwerPersonAccountByProperties(storage.Account.FirstName, storage.Account.LastName,
                            storage.Account.PersonEmail);
                    storage.Id = result.Id;
                }
                catch (PersonAccountNotFoundException)
                {
                    personAccountNotFound = true;

                    Console.WriteLine(
                        "The borrower person account was not located (First Name: {0}, Last Name: {1}, PersonEmail: {2}), trying Lead next",
                        storage.Account.FirstName,
                        storage.Account.LastName,
                        storage.Account.PersonEmail);
                }

                if (personAccountNotFound)
                {
                    try
                    {
                        var result = await FindLeadByProperties(storage.Account.FirstName, storage.Account.LastName,
                            storage.Account.PersonEmail);

                        storage.Id = ConvertLeadToAccount(result.Id);
                    }
                    catch (LeadNotFoundException)
                    {
                        leadNotFound = true;

                        Console.WriteLine(
                            "A Lead was not located (First Name: {0}, Last Name: {1}, PersonEmail: {2})!",
                            storage.Account.FirstName,
                            storage.Account.LastName,
                            storage.Account.PersonEmail);
                    }
                    catch (LeadConversionFailedException)
                    {
                        Console.WriteLine(
                            "The lead conversion failed (First Name: {0}, Last Name: {1}, PersonEmail: {2})!",
                            storage.Account.FirstName,
                            storage.Account.LastName,
                            storage.Account.PersonEmail);
                    }

                    if (leadNotFound)
                    {
                        try
                        {
                            var lead = new Lead()
                            {
                                FirstName = storage.Account.FirstName,
                                LastName = storage.Account.LastName,
                                Email = storage.Account.PersonEmail
                            };

                            var leadId = await CreateLead(lead);
                            storage.Id = ConvertLeadToAccount(leadId);
                        }
                        catch (LeadConversionFailedException)
                        {
                            Console.WriteLine(
                            "The lead conversion failed (First Name: {0}, Last Name: {1}, PersonEmail: {2})!",
                            storage.Account.FirstName,
                            storage.Account.LastName,
                            storage.Account.PersonEmail);
                        }
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(storage.Id)) throw new PortalSyncFailedToLocateBorrowerException();

            await UpdatePersonAccountAndOpportunity(storage);
            return storage.Id;
        }

        public async Task<string> UpdatePersonAccountAndOpportunity(PortalStorage storage)
        {
            var accountResult = await UpdatePersonAccount(storage.Account, storage.Id);

            var oppQueryResult = await FindBorrowerOpportunityId(storage.Id);

            // hack: for now, bail out if no opp is found, more needs to be done here
            if (string.IsNullOrWhiteSpace(oppQueryResult.Id)) return storage.Id;

            var opportunity = await FindBorrowerOpportunity(oppQueryResult.Id);

            opportunity.Current_Status__c = storage.LeadStatus;

            await UpdateBorrowerOpportunity(opportunity, oppQueryResult.Id);
            return storage.Id;
        }

        public async Task<string> UpdatePersonAccountAndOpportunity(SfdcStorage storage)
        {
            var result = await UpdatePersonAccount(storage.BorrowerPersonAccount, storage.BorrowerPersonAccountId);

            var oppQueryResult = await FindBorrowerOpportunityId(storage.BorrowerPersonAccountId, storage.Opportunity.Application_ID__c);

            if (string.IsNullOrWhiteSpace(oppQueryResult.Id))
            {
                // don't need to create then update, just create and bail
                oppQueryResult.Id = await CreateBorrowerOpportunity(storage.Opportunity);
                return storage.BorrowerPersonAccountId;
            }

            storage.OpportunityId = oppQueryResult.Id;
            storage.Opportunity.OwnerId = oppQueryResult.OwnerId;

            await UpdateBorrowerOpportunity(storage.Opportunity, oppQueryResult.Id);
            return storage.BorrowerPersonAccountId;
        }

        public string ConvertLeadToAccount(string leadId)
        {
            var soapUtility = new SfdcSoapUtility(SFUserName, SFPassword, SFSoapClientSecret);
            return soapUtility.ConvertLead(leadId);
        }

        #region Query Methods

        public async Task<SfdcQueryResult> FindBorrwerPersonAccountByProperties(string firstName, string lastName, string email)
        {
            QueryResult<SfdcQueryResult> recs = null;

            recs = await client.QueryAsync<SfdcQueryResult>(
                string.Format("SELECT Id, Name FROM Account WHERE FirstName='{0}' AND LastName='{1}' AND PersonEmail='{2}' ORDER BY LastModifiedDate DESC",
                firstName.Replace("'", @"\'"),
                lastName.Replace("'", @"\'"),
                email)
            );

            if (recs.Records.Count <= 0)
            {
                throw new PersonAccountNotFoundException();
            } else if (recs.Records.Count > 1)
            {
                Console.WriteLine("Found multiple records for {0} {1}:", firstName, lastName);
                foreach (var rec in recs.Records)
                {
                    Console.WriteLine("  {0} {1}", rec.Name, rec.Id);
                }
            }

            return recs.Records.First();
        }

        public async Task<SfdcQueryResult> FindLeadByProperties(string firstName, string lastName, string email)
        {
            QueryResult<SfdcQueryResult> recs = null;

            recs = await client.QueryAsync<SfdcQueryResult>(
                string.Format("SELECT Id, Name FROM Lead WHERE FirstName='{0}' AND LastName='{1}' AND Email='{2}' AND RecordTypeId='{3}' ORDER BY LastModifiedDate DESC",
                firstName.Replace("'", @"\'"),
                lastName.Replace("'", @"\'"),
                email,
                Lead.RecordType_Id)
            );

            if (recs.Records.Count <= 0) throw new LeadNotFoundException();

            return recs.Records.First();
        }

        public async Task<SfdcQueryResult> FindBorrowerOpportunityId(string accountId, string applicationId = null)
        {
            QueryResult<SfdcQueryResult> recs = null;

            // try to match on application id
            recs = await client.QueryAsync<SfdcQueryResult>(
                string.Format(
                    "SELECT Id, Name, OwnerId FROM Opportunity WHERE AccountId='{0}' AND RecordTypeId='{1}' AND Application_ID__c='{2}' ORDER BY CreatedDate DESC",
                    accountId,
                    Opportunity.RecordType_Id,
                    applicationId
                )
            );

            if (recs.Records.Count > 0)
            {
                return recs.Records[0];
            }

            // try to match opportunities with no application id
            recs = await client.QueryAsync<SfdcQueryResult>(
                string.Format(
                    "SELECT Id, Name, OwnerId FROM Opportunity WHERE AccountId='{0}' AND RecordTypeId='{1}' AND Application_ID__c='' ORDER BY CreatedDate DESC",
                    accountId,
                    Opportunity.RecordType_Id
                )
            );

            if (recs.Records.Count > 0)
            {
                return recs.Records[0];
            }

            // no matches, let's return null so the program can create a new one
            return new SfdcQueryResult()
            {
                Id = null,
                Name = "Not Found"
            };
        }

        public async Task<Opportunity> FindBorrowerOpportunity(string oppId)
        {
            QueryResult<Opportunity> recs = null;

            recs = await client.QueryAsync<Opportunity>(
                string.Format(
                    "SELECT {0} FROM Opportunity WHERE Id='{1}'  ORDER BY CreatedDate DESC",
                    Opportunity.CommaDelimitedProperties(),
                    oppId
                )
            );

            return recs.Records[0];
        }

        #endregion

        #region Create Methods

        public async Task<string> CreateLead(Lead lead)
        {
            string id = string.Empty;

            id = await client.CreateAsync("Lead", lead);

            return id;
        }

        public async Task<string> CreateBorrowerPersonAccount(BorrowerPersonAccount account)
        {
            string id = string.Empty;
             
            id = await client.CreateAsync("Account", account);

            return id;
        }

        public async Task<string> CreateBorrowerOpportunity(Opportunity opportunity)
        {
            string id = string.Empty;

            var full = new FullOpportunity(opportunity);

            id = await client.CreateAsync("Opportunity", full);

            return id;
        }

        #endregion

        #region Update Methods

        public async Task<string> UpdatePersonAccount<T>(T account, string id) where T : SimplePersonAccount
        {
            try
            {
                var response = await client.UpdateAsync("Account", id, account);

                TestCallResponse("UpdatePersonAccount", response);

                return response.Id;
            }
            catch (ForceException e)
            {
                if (e.Message.ToLower().Contains("entity is deleted"))
                {
                    throw new AccountDeletedException(account);
                }
                else
                {
                    throw e;
                }
            }
            
        }

        public async Task<SuccessResponse> UpdateBorrowerPersonAccount(SfdcStorage itemToStore)
        {
            var response = await client.UpdateAsync("Account", itemToStore.BorrowerPersonAccountId, itemToStore.BorrowerPersonAccount);

            TestCallResponse("UpdateBorrowerPersonAccount", response);

            return response;
        }

        public async Task<SuccessResponse> UpdateBorrowerOpportunity(Opportunity opportunity, string id)
        {
            var response = await client.UpdateAsync("Opportunity", id, opportunity);

            TestCallResponse("UpdateBorrowerOpportunity", response);

            return response;
        }

        #endregion

        private static void TestCallResponse(string activity, SuccessResponse response)
        {
            if (response.Success != "true")
            {
                Console.Error.WriteLine("SF Call failed: {0}\nError: {1}", activity, response.Errors);
            }
        }
    }
}
