﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.EntityModels
{
    public class LinkCapitalLoan
    {
        /// <summary>
        /// Local DB ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Associated user ID
        /// </summary>
        public int UserSFDCID { get; set; }

        /// <summary>
        /// Remote sales force ID for the loan object
        /// </summary>
        public int LoanSFDCID { get; set; }

        DateTime SFDCSyncDate { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}