﻿using System.ComponentModel.DataAnnotations;

namespace Core.EntityModels
{
	public class LoanProgram
	{
		public int Id { get; set; }
		[Required]
		[MaxLength(50)]
		public string Name { get; set; }
		[Required]
		[MaxLength(200)]
		public string Url { get; set; }
	}
}