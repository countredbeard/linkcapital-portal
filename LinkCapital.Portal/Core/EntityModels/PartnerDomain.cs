﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.EntityModels
{
    /// <summary>
    /// Data class used to check if a users Organizational email is vaild
    /// </summary>
    public class PartnerDomain
    {
        public int ID { get; set; }
        
        public int AffliateID { get; set; }

        /// <summary>
        /// The Organizations domain ex(linkcapital.com)
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// denotes if the OrgDoamin should be accepted as a usable domain
        /// </summary>
        public bool Active { get; set; }
    }
}