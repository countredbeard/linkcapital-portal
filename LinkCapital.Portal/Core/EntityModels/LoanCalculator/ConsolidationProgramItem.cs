﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.EntityModels;

namespace Web.EntityModels.LoanCalculator
{
    public class ConsolidationProgramItem
    {
        public RateTypes RateType { get; set; } 
        public int Term { get; set; }
        public double MonthlyPayment { get; set; }
        public double InterestRate { get; set; }
        public double LifetimeCost { get; set; }
        public double LifetimeSavings { get; set; }
        public double MonthlySavings { get; set; }
    }
}