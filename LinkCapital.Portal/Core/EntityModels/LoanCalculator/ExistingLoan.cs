﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.EntityModels.LoanCalculator
{ 
    public class ExistingLoan
    {
        public double Balance { get; set; }
        public double InterestRate { get; set; }
        public int RemainingTerm { get; set; }
        public double MonthlyPayment { get; set; }
        public double LifetimeCost { get; set; }
    }
}