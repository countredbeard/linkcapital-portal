﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.EntityModels;

namespace Web.EntityModels.LoanCalculator
{
    public class ResidentProgramItem : ConsolidationProgramItem
    {
        public double MonthlyPaymentPre { get; set; }
        public double MonthlyPaymentPost { get; set; }
        public double LifetimeSavingsPre { get; set; }
        public double LifetimeSavingsPost { get; set; }
        public double MonthlySavingsPre { get; set; }
        public double MonthlySavingsPost { get; set; }
    }
}
