﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Core.EntityModels
{
	public class LoanRule
	{
		public int Id { get; set; }
		public int LoanProgramId { get; set; }
		public int StateId { get; set; }
		public decimal LoanAmountThreshold { get; set; }
        public LoanQualificationRestriction LoanQualificationRestriction { get; set; }

		public LoanProgram LoanProgram { get; set; }
		public State State { get; set; }
	}
}