﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EntityModels
{
    public class Partner
    {
        public int ID { get; set; }
        public string AffiliateName { get; set; }
    }
}
