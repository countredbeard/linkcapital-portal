﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.EntityModels
{
    public enum RateTypes
    {
        Fixed, Variable
    }

    public class Rate
    {
        public int ID { get; set; }
        public RateTypes RateType { get; set; }
        public bool Resident { get; set; }
        public int Term { get; set; }
        public double InterestRate { get; set; }
    }
}