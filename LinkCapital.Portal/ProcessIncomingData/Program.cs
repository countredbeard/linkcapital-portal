﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.DataModels;
using Core.EntityModels;
using Core.Exceptions;
using Core.Salesforce;
using Microsoft.WindowsAzure;
using Salesforce.Common;
using Salesforce.Common.Models;
using Salesforce.Force;

namespace CampusDoorProcessing
{
    // small webjob to process incoming data from aspire and campus door
    // project uses https://github.com/developerforce/Force.com-Toolkit-for-NET
    class Program
    {
        static void Main(string[] args)
        {
            var home = Environment.GetEnvironmentVariable("HOME");
            var downloadsDirectory = string.Format(@"{0}\data\downloads\", home);

            if (!Directory.Exists(downloadsDirectory))
            {
                Console.Error.WriteLine("The directory {0} doesn't exist!", downloadsDirectory);
            }
            //var downloadsDirectory = Directory.GetCurrentDirectory();

            DirectoryInfo di = new DirectoryInfo(downloadsDirectory);
            FileSystemInfo[] files = di.GetFileSystemInfos();
            var sortedFiles =
                files.Where(
                    x => x.Extension.Contains("csv") && x.Name.StartsWith("CampusDoor") && !x.Name.Contains("Archive"))
                    .OrderBy(f => f.Name);

            foreach (var file in sortedFiles)
            {
                Console.WriteLine("Processing file {0}", file.Name);
                Task.WaitAll(ProcessFile(file.FullName));
            }
        }

        private static async Task<string> ProcessFile(string file)
        {
            var utility = new SfdcUtility
            {
                SFClientID = CloudConfigurationManager.GetSetting("SFClientID"),
                SFClientSecret = CloudConfigurationManager.GetSetting("SFClientSecret"),
                SFUserName = CloudConfigurationManager.GetSetting("SFUserName"),
                SFPassword = CloudConfigurationManager.GetSetting("SFPassword")
            };

            var dataUtility = new DataUtility(Environment.GetEnvironmentVariable("SQLAZURECONNSTR_LinkCapital.Portal") ??
                                              CloudConfigurationManager.GetSetting("DBConnection"));

            ApplicationUser.DbEncryptionKey = CloudConfigurationManager.GetSetting("DbEncryptionKey") ?? "BSrhVfHM-PCtSJS06lTxhBP-T$N90MAGA97VLmbB0ILjDXM6JflcdkGaaCoJ-uUW_0p1OnLWdnkw9xbn71anMwTzTOG$_uvNWnzR";

            var records = LoadData(file);

            foreach (var record in records)
            {
                // invalid record
                if (string.IsNullOrWhiteSpace(record.Lender)) continue;

                var foundUser = dataUtility.FindByEmail(record.BorrowerEmailAddress);
                if (foundUser == null || string.IsNullOrWhiteSpace(foundUser.UserSFDCID))
                {
                    Console.Error.WriteLine("A user with the email address {0} was not found in the database", record.BorrowerEmailAddress);
                }
                else
                {
                    record.AccountId = foundUser.UserSFDCID;
                }

                try
                {
                    await utility.StoreCDInSfdc(record);
                }
                catch (PersonAccountNotFoundException)
                {
                    Console.Error.WriteLine(
                        "The borrower was not located (First Name: {0}, Last Name: {1}, Email: {2})!",
                        record.BorrowerFirstName,
                        record.BorrowerLastName,
                        record.BorrowerEmailAddress);
                }
            }

            // rename to Archive
            File.Move(file, file.Replace(".csv", "-Archive.csv"));

            return "OK";
        }

        private static List<CampusDoorRecord> LoadData(string file)
        {
            List<string> RawLines = new List<string>();
            List<CampusDoorRecord> ProcessedLines = new List<CampusDoorRecord>();
            using (StreamReader fileStrem = new StreamReader(file))
            {
                while (fileStrem.EndOfStream == false)
                {
                    RawLines.Add(fileStrem.ReadLine());
                }
            }

            var foundHeaders = false;
            while (!foundHeaders)
            {
                if (RawLines[0].Trim().ToLower().StartsWith("lender"))
                    foundHeaders = true;

                RawLines.RemoveAt(0);
            }

            foreach (string currentLine in RawLines)
            {
                // 168 is current col count
                if (string.IsNullOrWhiteSpace(currentLine) || (currentLine.Split(',')).Count() < 168)
                    continue;

                ProcessedLines.Add(new CampusDoorRecord(currentLine));
            }

            return ProcessedLines;
        }
    }
}
