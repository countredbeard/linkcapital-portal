﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ExternalDataObj
{
    /// <summary>
    /// Maps the contact from salesforce
    /// </summary>
    public class Contact
    {
        // this class is currently stripped down to fit in to the current project time span
        public string ID { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Email { get; set; }
        public string X2nd_Phone__c { get; set; }
        //public string WorkPhone { get; set; }
        public string Phone { get; set; }
        //public Address MailingAddress { get; set; }
        public Address MailingAddress { get; set; }
        public string LeadStatus { get; set; }
    }

    public class Address
    {
        [JsonProperty(PropertyName = "city")]
        public string MailingCity { get; set; }

        [JsonProperty(PropertyName = "country")]
        public string MailingCountry { get; set; }
        //public string MailingLatitude { get; set; }
        //public string MailingLongitude { get; set; }
         [JsonProperty(PropertyName = "zip")]
        public string MailingPostalCode { get; set; }

        [JsonProperty(PropertyName = "state")]
        public string MailingState { get; set; }

        //public string MailingStateCode { get; set; }
         [JsonProperty(PropertyName = "street")]
        public string MailingStreet { get; set; }

        public Address() { }

       
    }

    // Lead Status types
    /* Soft Denied, Hard Denied, Descision Pending, Fully dispersied, Documantation, DocumantationPending, Submitted, */
}
