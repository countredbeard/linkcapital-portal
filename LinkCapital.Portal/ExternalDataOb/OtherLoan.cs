﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalDataObj
{
    /// <summary>
    /// Mapping of the sales force field of the same name
    /// </summary>
    public class OtherLoan
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public string BorrowerID { get; set; }
        public bool Consolidated { get; set; }
        public double CurrentIntrestRate { get; set; }
        public double CurrentPrincipal { get; set; }
        public string FullAccountNumber { get; set; }
        public double IntrestRate { get; set; }
        public DateTime LastStatementDate { get; set; }
        public string LenderName { get; set; }
        public string LoanServicer { get; set; }
        public string LoanType { get; set; }
        public string PayoffMailingAddress { get; set; }
        // in years
        public int RemainingTerm { get; set; }
        public double TotalAmountBorrowed { get; set; }
    }
}
