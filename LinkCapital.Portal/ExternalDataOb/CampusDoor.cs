﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalDataObj
{
    public class CampusDoor 
    {
        // TODO: change type of fields to what is shown in the data xls example
        
        public string Lender { get; set; }
        public string ProgramName { get; set; }
       public string ApplicationID { get; set; }
       public string LoanSequence { get; set; }
       public string ReferralID { get; set; }
       public string CommonLineUniqueIdentifier { get; set; }
       public string DOESchoolCode { get; set; }
       public string SchoolName { get; set; }
       public string CostOfAttendence { get; set; }
       public string EstimatedFinancialAid { get; set; }
       public string RecommendedMaximumLoanAmount { get; set; }
       public string RequestedLoanAmount { get; set; }
       public string CertifiedLoanAmount { get; set; }
       public string IncompleteDate { get; set; }
       public string SubmittedDate { get; set; }
       public string ApprovalDisclosureDate { get; set; }
       public string FinalCertificationDat { get; set; }
       public string FinalCreditScoreDate { get; set; }
       public string ActualDisbursementDate { get; set; }
       public string CurrentStatus { get; set; }
       public string CurrentState { get; set; }
       public string CurrentStatusDate { get; set; }
       public string BorrowerAAReason1 { get; set; }
       public string BorrowerAAReason2 { get; set; }
       public string BorrowerAAReason3 { get; set; }
       public string BorrowerAAReason4 { get; set; }
       public string PromNoteVersion { get; set; }
       public string CreditTier { get; set; }
       public string RateType { get; set; }
       public string InterestRate { get; set; }
       public string APR_FinalDisclosure { get; set; }
       public string Servicer { get; set; }
       public string RepaymentTerm { get; set; }
       public string OriginationFeePercentage { get; set; }
       public string OriginationFeeAmountTotal { get; set; }
       public string LoanStartDate { get; set; }
       public string CertifiedEnrollmentPeriodStartDate { get; set; }
       public string CertfiedExpectedGradDate { get; set; }
       public string BorrowerLoanPersonInfoSSN { get; set; }
       public string BorrowerDateofBirth { get; set; }
        public string BorrowerFirstName { get; set; }
        public string BorrowerMiddleInitial { get; set; }
        public string BorrowerLastName { get; set; }
        public string BorrowerPhoneNumber { get; set; }
        public string BorrowerEmailAddress { get; set; }
        public string BorrowerPrimaryAddress { get; set; }
        public string Borrower_City { get; set; }
        public string Borrower_State { get; set; }
        public string Borrower_Zip_Code { get; set; }
        public string eSign_Election { get; set; }
        public string Academic_Year { get; set; }
        public string EnrollmentPeriodStart1 { get; set; }
        public string EnrollmentPeriodEnd { get; set; }
        public string MajorEnumText { get; set; }
        public string Borrower_Employment { get; set; }
        public string Borrower_Annual_Salary { get; set; }
        public string Borrower_Employer_Name { get; set; }
        public string Borrower_Occupation { get; set; }
        public string Borrower_Occupation_Other { get; set; }
        public string Borrower_Employer_Phone_Numbe { get; set; }
        public string Borrower_Employment_Length { get; set; }
        public string Borrower_Other_Income { get; set; }
        public string Borrower_Other_Income_Source { get; set; }
        public string BorrowerHousingStatus { get; set; }
        public string BorrowerRent { get; set; }
        public string BorrowerTotalCDCalculatedMonthlyDebt { get; set; }
        public string BorrowerDTIPercentage { get; set; }
        public string Cosigner_SSN { get; set; }
        public string Cosigner_First_Name { get; set; }
        public string Cosigner_Middle_Name { get; set; }
        public string Cosigner_Last_Name { get; set; }
        public string Cosigner_Phone_Number { get; set; }
        public string Cosigner_Email_Address { get; set; }
        public string Cosigner_Street_Address { get; set; }
        public string Cosigner_City { get; set; }
        public string Cosigner_State { get; set; }
        public string Cosigner_Zip_Code { get; set; }
        public string Cosigner_Employment { get; set; }
        public string Cosigner_Annual_Salary { get; set; }
        public string Cosigner_Employer_Name { get; set; }
        public string Cosigner_Occupation { get; set; }
        public string Cosigner_Occupation_Other { get; set; }
        public string Cosigner_Employer_Phone_Number { get; set; }
        public string Cosigner_Employment_Length { get; set; }
        public string Cosigner_Other_Income { get; set; }
        public string Cosigner_Other_Income_Source { get; set; }
        public string CosignerHousingStatus { get; set; }
        public string CosignerRent { get; set; }
        public string CosignerTotalCDCalculatedMonthlyDebt { get; set; }
        public string CosignerDTIPercentage { get; set; }
        public string PendingDisbursementAmount1 { get; set; }
        public string PendingDisbursementAmount2 { get; set; }
        public string PendingDisbursementAmount3 { get; set; }
        public string PendingDisbursementAmount4 { get; set; }
        public string PendingDisbursementAmount5 { get; set; }
        public string PendingDisbursementAmount6 { get; set; }
        public string PendingDisbursementAmount7 { get; set; }
        public string PendingDisbursementAmount8 { get; set; }
        public string PendingDisbursementAmount9 { get; set; }
        public string PendingDisbursementDate1 { get; set; }
        public string PendingDisbursementDate2 { get; set; }
        public string PendingDisbursementDate3 { get; set; }
        public string PendingDisbursementDate4 { get; set; }
        public string PendingDisbursementDate5 { get; set; }
        public string PendingDisbursementDate6 { get; set; }
        public string PendingDisbursementDate7 { get; set; }
        public string PendingDisbursementDate8 { get; set; }
        public string PendingDisbursementDate9 { get; set; }
        public string Disbursement_Info_1 { get; set; }
        public string Disbursement_Info_2 { get; set; }
        public string Disbursement_Info_3 { get; set; }
        public string Disbursement_Info_4 { get; set; }
        public string Disbursement_Info_5 { get; set; }
        public string Disbursement_Info_6 { get; set; }
        public string Disbursement_Info_7 { get; set; }
        public string Disbursement_Info_8 { get; set; }
        public string Disbursement_Info_9 { get; set; }
        public string disbursementdate1 { get; set; }
        public string disbursementdate2 { get; set; }
        public string disbursementdate3 { get; set; }
        public string disbursementdate4 { get; set; }
        public string disbursementdate5 { get; set; }
        public string disbursementdate6 { get; set; }
        public string disbursementdate7 { get; set; }
        public string disbursementdate8 { get; set; }
        public string disbursementdate9 { get; set; }
        public string Borrower_Reference_1_First_Name { get; set; }
        public string Borrower_Reference_1_Middle_Initial { get; set; }
        public string Borrower_Reference_1_Last_Name { get; set; }
        public string Borrower_Reference_1_Suffix { get; set; }
        public string Borrower_Reference_1_Permanent_Address { get; set; }
        public string Borrower_Reference_1_AptSuiteFloor_Number  { get; set; }
        public string Borrower_Reference_1_City { get; set; }
        public string Borrower_Reference_1_StateTerritory { get; set; }
        public string Borrower_Reference_1_Zip { get; set; }
        public string Borrower_Reference_1_Phone_Number { get; set; }
        public string Borrower_Reference_1_Relationship { get; set; }
        public string Cosigner_Reference_1_First_Name { get; set; }
        public string Cosigner_Reference_1_Middle_Initial { get; set; }
        public string Cosigner_Reference_1_Last_Name { get; set; }
        public string Cosigner_Reference_1_Suffix { get; set; }
        public string Cosigner_Reference_1_Permanent_Address { get; set; }
        public string Cosigner_Reference_1_AptSuiteFloor_Number { get; set; }
        public string Cosigner_Reference_1_City { get; set; }
        public string Cosigner_Reference_1_StateTerritory { get; set; }
        public string Cosigner_Reference_1_Zip { get; set; }
        public string Cosigner_Reference_1_Phone_Number { get; set; }
        public string Cosigner_Reference_1_Relationship { get; set; }
        public string R1 { get; set; }
        public string R2 { get; set; }
        public string R3 { get; set; }
        public string R4 { get; set; }
        public string R5 { get; set; }
        public string R6 { get; set; }
        public string R7 { get; set; }
        public string R8 { get; set; }
        public string R9 { get; set; }
        public string R10 { get; set; }
        public string R11 { get; set; }
        public string R12 { get; set; }
        public string R13 { get; set; }
        public string R14 { get; set; }
        public string R15 { get; set; }
        public string R16 { get; set; }
        public string R17 { get; set; }
        public string R18 { get; set; }
        public string R19 { get; set; }
        public string R20 { get; set; }
        public string R21 { get; set; }
        public string R22 { get; set; }
        public string R23 { get; set; }

        public CampusDoor(string CSVLineInput)
        {
            string[] splitLine = CSVLineInput.Split(',');
            
            Lender = splitLine[0];
            ProgramName = splitLine[1];
            ApplicationID = splitLine[2];
            LoanSequence = splitLine[3];
            ReferralID = splitLine[4];
            CommonLineUniqueIdentifier = splitLine[5];
            DOESchoolCode = splitLine[6];
            SchoolName = splitLine[7];
            CostOfAttendence = splitLine[8];
            EstimatedFinancialAid = splitLine[9];
            RecommendedMaximumLoanAmount = splitLine[10];
            RequestedLoanAmount = splitLine[11];
            CertifiedLoanAmount = splitLine[12];
            IncompleteDate = splitLine[13];
            SubmittedDate = splitLine[14];
            ApprovalDisclosureDate = splitLine[15];
            FinalCertificationDat = splitLine[16];
            FinalCreditScoreDate = splitLine[17];
            ActualDisbursementDate = splitLine[18];
            CurrentStatus = splitLine[19];
            CurrentState = splitLine[20];
            CurrentStatusDate = splitLine[21];
            BorrowerAAReason1 = splitLine[22];
            BorrowerAAReason2 = splitLine[23];
            BorrowerAAReason3 = splitLine[24];
            BorrowerAAReason4 = splitLine[25];
            PromNoteVersion = splitLine[26];
            CreditTier = splitLine[27];
            RateType = splitLine[28];
            InterestRate = splitLine[29];
            APR_FinalDisclosure = splitLine[30];
            Servicer = splitLine[31];
            RepaymentTerm = splitLine[32];
            OriginationFeePercentage = splitLine[33];
            OriginationFeeAmountTotal = splitLine[34];
            LoanStartDate = splitLine[35];
            CertifiedEnrollmentPeriodStartDate = splitLine[36];
            CertfiedExpectedGradDate = splitLine[37];
            BorrowerLoanPersonInfoSSN = splitLine[38];
            BorrowerDateofBirth = splitLine[39];
            BorrowerFirstName = splitLine[40];
            BorrowerMiddleInitial = splitLine[41];
            BorrowerLastName = splitLine[42];
            BorrowerPhoneNumber = splitLine[43];
            BorrowerEmailAddress = splitLine[44];
            BorrowerPrimaryAddress = splitLine[45];
            Borrower_City = splitLine[46];
            Borrower_State = splitLine[47];
            Borrower_Zip_Code = splitLine[48];
            eSign_Election = splitLine[49];
            Academic_Year = splitLine[50];
            EnrollmentPeriodStart1 = splitLine[51];
            EnrollmentPeriodEnd = splitLine[52];
            MajorEnumText = splitLine[53];
            Borrower_Employment = splitLine[54];
            Borrower_Annual_Salary = splitLine[55];
            Borrower_Employment = splitLine[56];
            Borrower_Annual_Salary = splitLine[57];
            Borrower_Employer_Name = splitLine[58];
            Borrower_Occupation = splitLine[59];
            Borrower_Occupation_Other = splitLine[60];
            Borrower_Employer_Phone_Numbe = splitLine[61];
            Borrower_Employment_Length = splitLine[62];
            Borrower_Other_Income = splitLine[63];
            Borrower_Other_Income_Source = splitLine[64];
            BorrowerHousingStatus = splitLine[65];
            BorrowerRent = splitLine[66];
            BorrowerTotalCDCalculatedMonthlyDebt = splitLine[67];
            BorrowerDTIPercentage = splitLine[68];
            Cosigner_SSN = splitLine[69];
            Cosigner_First_Name = splitLine[70];
            Cosigner_Middle_Name = splitLine[71];
            Cosigner_Last_Name = splitLine[72];
            Cosigner_Phone_Number = splitLine[73];
            Cosigner_Email_Address = splitLine[74];
            Cosigner_Street_Address = splitLine[75];
            Cosigner_City = splitLine[76];
            Cosigner_State = splitLine[77];
            Cosigner_Zip_Code = splitLine[78];
            Cosigner_Employment = splitLine[79];
            Cosigner_Annual_Salary = splitLine[80];
            Cosigner_Employer_Name = splitLine[81];
            Cosigner_Occupation = splitLine[82];
            Cosigner_Occupation_Other = splitLine[83];
            Cosigner_Employer_Phone_Number = splitLine[84];
            Cosigner_Employment_Length = splitLine[85];
            Cosigner_Other_Income = splitLine[86];
            Cosigner_Other_Income_Source = splitLine[87];
            CosignerHousingStatus = splitLine[88];
            CosignerRent = splitLine[89];
            CosignerTotalCDCalculatedMonthlyDebt = splitLine[90];
            CosignerDTIPercentage = splitLine[91];
            PendingDisbursementAmount1 = splitLine[92];
            PendingDisbursementAmount2 = splitLine[93];
            PendingDisbursementAmount3 = splitLine[94];
            PendingDisbursementAmount4 = splitLine[95];
            PendingDisbursementAmount5 = splitLine[96];
            PendingDisbursementAmount6 = splitLine[97];
            PendingDisbursementAmount7 = splitLine[98];
            PendingDisbursementAmount8 = splitLine[99];
            PendingDisbursementAmount9 = splitLine[100];
            PendingDisbursementDate1 = splitLine[101];
            PendingDisbursementDate2 = splitLine[102];
            PendingDisbursementDate3 = splitLine[103];
            PendingDisbursementDate4 = splitLine[104];
            PendingDisbursementDate5 = splitLine[105];
            PendingDisbursementDate6 = splitLine[106];
            PendingDisbursementDate7 = splitLine[107];
            PendingDisbursementDate8 = splitLine[108];
            PendingDisbursementDate9 = splitLine[109];
            Disbursement_Info_1 = splitLine[110];
            Disbursement_Info_2 = splitLine[111];
            Disbursement_Info_3 = splitLine[112];
            Disbursement_Info_4 = splitLine[113];
            Disbursement_Info_5 = splitLine[114];
            Disbursement_Info_6 = splitLine[115];
            Disbursement_Info_7 = splitLine[116];
            Disbursement_Info_8 = splitLine[117];
            Disbursement_Info_9 = splitLine[118];
            disbursementdate1 = splitLine[119];
            disbursementdate2 = splitLine[120];
            disbursementdate3 = splitLine[121];
            disbursementdate4 = splitLine[122];
            disbursementdate5 = splitLine[123];
            disbursementdate6 = splitLine[124];
            disbursementdate7 = splitLine[125];
            disbursementdate8 = splitLine[126];
            disbursementdate9 = splitLine[127];
            Borrower_Reference_1_First_Name  = splitLine[128];
            Borrower_Reference_1_Middle_Initial  = splitLine[129];
            Borrower_Reference_1_Last_Name  = splitLine[130];
            Borrower_Reference_1_Suffix  = splitLine[131];
            Borrower_Reference_1_Permanent_Address  = splitLine[132];
            Borrower_Reference_1_AptSuiteFloor_Number   = splitLine[133];
            Borrower_Reference_1_City  = splitLine[134];
            Borrower_Reference_1_StateTerritory  = splitLine[135];
            Borrower_Reference_1_Zip  = splitLine[136];
            Borrower_Reference_1_Phone_Number  = splitLine[137];
            Borrower_Reference_1_Relationship  = splitLine[138];
            Cosigner_Reference_1_First_Name  = splitLine[139];
            Cosigner_Reference_1_Middle_Initial  = splitLine[140];
            Cosigner_Reference_1_Last_Name  = splitLine[141];
            Cosigner_Reference_1_Suffix  = splitLine[142];
            Cosigner_Reference_1_Permanent_Address  = splitLine[143];
            Cosigner_Reference_1_AptSuiteFloor_Number  = splitLine[144];
            Cosigner_Reference_1_City  = splitLine[145];
            Cosigner_Reference_1_StateTerritory  = splitLine[146];
            Cosigner_Reference_1_Zip  = splitLine[147];
            Cosigner_Reference_1_Phone_Number  = splitLine[148];
            Cosigner_Reference_1_Relationship  = splitLine[149];
            R1  = splitLine[150];
            R2  = splitLine[151];
            R3  = splitLine[152];
            R4  = splitLine[153];
            R5  = splitLine[154];
            R6  = splitLine[155];
            R7  = splitLine[156];
            R8  = splitLine[157];
            R9  = splitLine[158];
            R10  = splitLine[159];
            R11  = splitLine[160];
            R12  = splitLine[161];
            R13  = splitLine[162];
            R14  = splitLine[163];
            R15  = splitLine[164];
            R16  = splitLine[165];
            R17  = splitLine[167];
            R18  = splitLine[168];
            R19  = splitLine[169];
            R20  = splitLine[170];
            //R21  = splitLine[171];
            //R22  = splitLine[172];
            //R23  = splitLine[173];
        }
    }
}
