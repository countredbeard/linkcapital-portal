﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalDataObj
{
    public class LinkLoan
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public string BorrowerID { get; set; }
        public double CurrentInterestRate { get; set; }
        public string CurrentLenderOfRecord { get; set; }
        public double CurrentPrincipal { get; set; }
        public string FullAccountNumber { get; set; }
        public double InterestRate { get; set; }
        public DateTime LastStatmentDate { get; set; }
        public string LenderName { get; set; }
        public string LoanServicer { get; set; }
        public string OriginalLenderOfRecord { get; set; }
        public string Originator { get; set; }
        public string PayoffMailingAddress { get; set; }
        // in years
        public int RemainingTerm { get; set; }
        public double TotalAmountBorrowed { get; set; }
    }
}
