﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Renci.SshNet;

namespace BlmDownload
{
    class Program
    {
        private static void Main(string[] args)
        {
            var home = Environment.GetEnvironmentVariable("HOME");
            var downloadsDirectory = string.Format(@"{0}\data\downloads\", home);

            if (!Directory.Exists(downloadsDirectory))
            {
                Directory.CreateDirectory(downloadsDirectory);
            }

            string address = CloudConfigurationManager.GetSetting("BlmDataAddress");
            int port = Convert.ToInt32(CloudConfigurationManager.GetSetting("BlmDataPort"));
            string userName = CloudConfigurationManager.GetSetting("BlmDataUserName");
            string password = CloudConfigurationManager.GetSetting("BlmDataPassword");

            const string sftpDirectory = "/";

            using (var client = new SftpClient(address, port, userName, password))
            {
                client.Connect();

                // if the folder doesn't exist, something is wrong
                if (!client.Exists(sftpDirectory))
                {
                    Environment.Exit(0);
                }

                foreach (var currentFile in client.ListDirectory(sftpDirectory))
                {
                    if (currentFile.Name == "." && currentFile.Name == "..") continue;

                    if (!currentFile.Name.ToLower().Contains("allapplications") ||
                        !currentFile.Name.ToLower().Contains("csv"))
                    {
                        // todo: should these files be deleted?
                        continue;
                    }

                    // parse date string off file
                    var unformattedDateString = currentFile.Name.Split(' ')[1];

                    if (unformattedDateString.Length < 8)
                    {
                        Console.Error.WriteLine("The file name {0} could not be parsed", currentFile.Name);
                        continue;
                    }

                    var dateString = string.Format("{0}{1}{2}",
                        unformattedDateString.Substring(4, 4),
                        unformattedDateString.Substring(0, 2),
                        unformattedDateString.Substring(2, 2)
                        );

                    var fileName = new StringBuilder();
                    fileName.Append(downloadsDirectory);
                    fileName.Append("BLM-");
                    fileName.Append(dateString);
                    fileName.Append(".csv");

                    if (File.Exists(fileName.ToString()) || File.Exists(fileName.ToString().Replace(".csv", "-Archive.csv")))
                    {
                        Console.WriteLine("The file was already downloaded, skipping. ({0})", fileName.ToString());
                        continue;
                    }

                    Console.WriteLine("Downloading {0} to {1}", currentFile.Name, fileName);

                    using (FileStream fileStream = new FileStream(fileName.ToString(), FileMode.Create))
                    {
                        client.DownloadFile(currentFile.FullName, fileStream);
                        fileStream.Flush();
                    }

                    // delete the file off the FTP
                    //currentFile.Delete();
                }

                client.Disconnect();
            }
        }
    }
}
