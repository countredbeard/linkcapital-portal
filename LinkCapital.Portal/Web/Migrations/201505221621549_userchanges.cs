namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userchanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LinkCapitalLoans",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    UserSFDCID = c.Int(nullable: false),
                    LoanSFDCID = c.Int(nullable: false),
                    ApplicationUser_Id = c.String(maxLength: 128),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            AddColumn("dbo.AspNetUsers", "UserSFDCID", c => c.String());
            AddColumn("dbo.AspNetUsers", "LeadStatus", c => c.String());
            AddColumn("dbo.AspNetUsers", "LastSyncDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "LastChanged", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LinkCapitalLoans", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.LinkCapitalLoans", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.AspNetUsers", "LastChanged");
            DropColumn("dbo.AspNetUsers", "LastSyncDate");
            DropColumn("dbo.AspNetUsers", "LeadStatus");
            DropColumn("dbo.AspNetUsers", "UserSFDCID");
            DropTable("dbo.LinkCapitalLoans");
        }
    }
}
