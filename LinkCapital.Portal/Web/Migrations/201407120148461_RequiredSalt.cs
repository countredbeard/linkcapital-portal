namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredSalt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "Salt", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "Salt", c => c.String());
        }
    }
}
