namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RegistrationContexts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RegistrationContexts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReturnUrl = c.String(),
                        Context = c.String(),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RegistrationContexts", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.RegistrationContexts", new[] { "UserId" });
            DropTable("dbo.RegistrationContexts");
        }
    }
}
