namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LoanProgramRules : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoanPrograms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Url = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LoanRules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoanProgramId = c.Int(nullable: false),
                        StateId = c.Int(nullable: false),
                        LoanAmountThreshold = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LoanPrograms", t => t.LoanProgramId, cascadeDelete: true)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.LoanProgramId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Abbreviation = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanRules", "StateId", "dbo.States");
            DropForeignKey("dbo.LoanRules", "LoanProgramId", "dbo.LoanPrograms");
            DropIndex("dbo.LoanRules", new[] { "StateId" });
            DropIndex("dbo.LoanRules", new[] { "LoanProgramId" });
            DropTable("dbo.States");
            DropTable("dbo.LoanRules");
            DropTable("dbo.LoanPrograms");
        }
    }
}
