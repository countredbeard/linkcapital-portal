namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoanQualificationRules : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanRules", "LoanQualificationRestriction", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanRules", "LoanQualificationRestriction");
        }
    }
}
