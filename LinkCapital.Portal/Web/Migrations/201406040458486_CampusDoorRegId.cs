namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampusDoorRegId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CampusDoorRegId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "CampusDoorRegId");
        }
    }
}
