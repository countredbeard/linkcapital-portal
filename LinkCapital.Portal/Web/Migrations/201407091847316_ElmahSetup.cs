namespace Web.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class ElmahSetup : DbMigration
    {
        public override void Up()
        {
            Sql(CreateTable_ELMAH_Error);
            Sql(CreatreProcedure_ELMAH_GetErrorXml);
            Sql(CreateProcedure_ELMAH_GetErrorsXml);
            Sql(CreateProcedure_ELMAH_LogError);
        }

        public override void Down()
        {
            Sql(DropProcedures);
            Sql(DropTable_ELMAH_Error);
        }

        #region Create Table
        private const string CreateTable_ELMAH_Error = @"
                                                        CREATE TABLE [dbo].[ELMAH_Error]
                                                        (
                                                            [ErrorId]     UNIQUEIDENTIFIER NOT NULL,
                                                            [Application] NVARCHAR(60)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                                                            [Host]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                                                            [Type]        NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                                                            [Source]      NVARCHAR(60)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                                                            [Message]     NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                                                            [User]        NVARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                                                            [StatusCode]  INT NOT NULL,
                                                            [TimeUtc]     DATETIME NOT NULL,
                                                            [Sequence]    INT IDENTITY (1, 1) NOT NULL,
                                                            [AllXml]      NTEXT COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
                                                        ) 
                                                        ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

                                                        ALTER TABLE [dbo].[ELMAH_Error] WITH NOCHECK ADD 
                                                            CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED ([ErrorId]) ON [PRIMARY] 

                                                        ALTER TABLE [dbo].[ELMAH_Error] ADD 
                                                            CONSTRAINT [DF_ELMAH_Error_ErrorId] DEFAULT (NEWID()) FOR [ErrorId]

                                                        CREATE NONCLUSTERED INDEX [IX_ELMAH_Error_App_Time_Seq] ON [dbo].[ELMAH_Error] 
                                                        (
                                                            [Application]   ASC,
                                                            [TimeUtc]       DESC,
                                                            [Sequence]      DESC
                                                        ) 
                                                        ON [PRIMARY]
                                                                ";
        #endregion

        #region CreateProcedure
        private const string CreatreProcedure_ELMAH_GetErrorXml = @"
                                                        CREATE PROCEDURE [dbo].[ELMAH_GetErrorXml]
                                                        (
                                                            @Application NVARCHAR(60),
                                                            @ErrorId UNIQUEIDENTIFIER
                                                        )
                                                        AS

                                                            SET NOCOUNT ON

                                                            SELECT 
                                                                [AllXml]
                                                            FROM 
                                                                [ELMAH_Error]
                                                            WHERE
                                                                [ErrorId] = @ErrorId
                                                            AND
                                                                [Application] = @Application
                                                            ";
        
        private const string CreateProcedure_ELMAH_GetErrorsXml = @"
                                                        CREATE PROCEDURE [dbo].[ELMAH_GetErrorsXml]
                                                        (
                                                            @Application NVARCHAR(60),
                                                            @PageIndex INT = 0,
                                                            @PageSize INT = 15,
                                                            @TotalCount INT OUTPUT
                                                        )
                                                        AS 

                                                            SET NOCOUNT ON

                                                            DECLARE @FirstTimeUTC DATETIME
                                                            DECLARE @FirstSequence INT
                                                            DECLARE @StartRow INT
                                                            DECLARE @StartRowIndex INT

                                                            SELECT 
                                                                @TotalCount = COUNT(1) 
                                                            FROM 
                                                                [ELMAH_Error]
                                                            WHERE 
                                                                [Application] = @Application

                                                            -- Get the ID of the first error for the requested page

                                                            SET @StartRowIndex = @PageIndex * @PageSize + 1

                                                            IF @StartRowIndex <= @TotalCount
                                                            BEGIN

                                                                SET ROWCOUNT @StartRowIndex

                                                                SELECT  
                                                                    @FirstTimeUTC = [TimeUtc],
                                                                    @FirstSequence = [Sequence]
                                                                FROM 
                                                                    [ELMAH_Error]
                                                                WHERE   
                                                                    [Application] = @Application
                                                                ORDER BY 
                                                                    [TimeUtc] DESC, 
                                                                    [Sequence] DESC

                                                            END
                                                            ELSE
                                                            BEGIN

                                                                SET @PageSize = 0

                                                            END

                                                            -- Now set the row count to the requested page size and get
                                                            -- all records below it for the pertaining application.

                                                            SET ROWCOUNT @PageSize

                                                            SELECT 
                                                                errorId     = [ErrorId], 
                                                                application = [Application],
                                                                host        = [Host], 
                                                                type        = [Type],
                                                                source      = [Source],
                                                                message     = [Message],
                                                                [user]      = [User],
                                                                statusCode  = [StatusCode], 
                                                                time        = CONVERT(VARCHAR(50), [TimeUtc], 126) + 'Z'
                                                            FROM 
                                                                [ELMAH_Error] error
                                                            WHERE
                                                                [Application] = @Application
                                                            AND
                                                                [TimeUtc] <= @FirstTimeUTC
                                                            AND 
                                                                [Sequence] <= @FirstSequence
                                                            ORDER BY
                                                                [TimeUtc] DESC, 
                                                                [Sequence] DESC
                                                            FOR
                                                                XML AUTO
                                                                ";

        private const string CreateProcedure_ELMAH_LogError = @"
                                                            CREATE PROCEDURE [dbo].[ELMAH_LogError]
                                                            (
                                                                @ErrorId UNIQUEIDENTIFIER,
                                                                @Application NVARCHAR(60),
                                                                @Host NVARCHAR(30),
                                                                @Type NVARCHAR(100),
                                                                @Source NVARCHAR(60),
                                                                @Message NVARCHAR(500),
                                                                @User NVARCHAR(50),
                                                                @AllXml NTEXT,
                                                                @StatusCode INT,
                                                                @TimeUtc DATETIME
                                                            )
                                                            AS

                                                                SET NOCOUNT ON

                                                                INSERT
                                                                INTO
                                                                    [ELMAH_Error]
                                                                    (
                                                                        [ErrorId],
                                                                        [Application],
                                                                        [Host],
                                                                        [Type],
                                                                        [Source],
                                                                        [Message],
                                                                        [User],
                                                                        [AllXml],
                                                                        [StatusCode],
                                                                        [TimeUtc]
                                                                    )
                                                                VALUES
                                                                    (
                                                                        @ErrorId,
                                                                        @Application,
                                                                        @Host,
                                                                        @Type,
                                                                        @Source,
                                                                        @Message,
                                                                        @User,
                                                                        @AllXml,
                                                                        @StatusCode,
                                                                        @TimeUtc
                                                                    )
                                                                    ";
        #endregion

        #region Drop Procedures
        private const string DropProcedures = @"
                                            DROP PROCEDURE [dbo].[ELMAH_GetErrorXml]
                                            DROP PROCEDURE [dbo].[ELMAH_GetErrorsXml]
                                            DROP PROCEDURE [dbo].[ELMAH_LogError]
                                                    ";
        #endregion

        #region Drop Table
        private const string DropTable_ELMAH_Error = @"
                                            DROP INDEX [IX_ELMAH_Error_App_Time_Seq] ON [dbo].[ELMAH_Error]
                                            ALTER TABLE [dbo].[ELMAH_Error] DROP CONSTRAINT [DF_ELMAH_Error_ErrorId]
                                            ALTER TABLE [dbo].[ELMAH_Error] DROP CONSTRAINT [PK_ELMAH_Error]
                                            DROP TABLE [dbo].[ELMAH_Error]
                                                    ";
        #endregion
    }
}
