namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateRatesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rates",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    RateType = c.Int(nullable: false),
                    Resident = c.Boolean(nullable: false),
                    Term = c.Int(nullable: false),
                    InterestRate = c.Double(nullable: false)
                })
                .PrimaryKey(t => t.ID);
        }
        
        public override void Down()
        {
            DropTable("dbo.Rates");
        }
    }
}
