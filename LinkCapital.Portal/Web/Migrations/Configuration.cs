using Web.EntityModels;

namespace Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Repositories.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Web.Repositories.DataContext";
        }

        protected override void Seed(Web.Repositories.DataContext context)
        {
            // Create and retrieve the loan programs.
            const string lcProgramName = "Link Capital";
            const string blmProgramName = "Bank of Lake Mills";
            const string lcResidencyProgramName = "Link Capital (Residency)";
            const string blmResidencyProgramName = "Bank of Lake Mills (Residency)";

            string lcUrl = Web.Utilities.Configuration.CampusDoorNewLoanAppUrl + "&ReferralID=3301";
            string blmUrl = Web.Utilities.Configuration.CampusDoorNewLoanAppUrl + "&ReferralID=3307";
            string lcResidencyUrl = Web.Utilities.Configuration.CampusDoorNewLoanAppUrl + "&ReferralID=3310";
            //string blResidencymUrl = Web.Utilities.Configuration.CampusDoorNewLoanAppUrl + "&ReferralID=3305";
            string blResidencymUrl = Web.Utilities.Configuration.CampusDoorNewLoanAppUrl + "&ReferralID=3308";

            context.LoanPrograms.AddOrUpdate(
                lp => lp.Name,
                new LoanProgram { Name = lcProgramName, Url = lcUrl },
                new LoanProgram { Name = blmProgramName, Url = blmUrl },
                new LoanProgram { Name = lcResidencyProgramName, Url = lcResidencyUrl },
                new LoanProgram { Name = blmResidencyProgramName, Url = blResidencymUrl }
            );
            context.SaveChanges();

            LoanProgram lcProgram = context.LoanPrograms.Single(lp => lp.Name == lcProgramName);
            LoanProgram blmProgram = context.LoanPrograms.Single(lp => lp.Name == blmProgramName);
            LoanProgram lcResidencyProgram = context.LoanPrograms.Single(lp => lp.Name == lcResidencyProgramName);
            LoanProgram blResidencymProgram = context.LoanPrograms.Single(lp => lp.Name == blmResidencyProgramName);

            #region Create and retrieve the states.
            context.States.AddOrUpdate(
                s => s.Name,
                new State { Name = "Alabama", Abbreviation = "AL" },
                new State { Name = "Alaska", Abbreviation = "AK" },
                new State { Name = "Arizona", Abbreviation = "AZ" },
                new State { Name = "Arkansas", Abbreviation = "AR" },
                new State { Name = "California", Abbreviation = "CA" },
                new State { Name = "Colorado", Abbreviation = "CO" },
                new State { Name = "Connecticut", Abbreviation = "CT" },
                new State { Name = "Delaware", Abbreviation = "DE" },
                new State { Name = "Florida", Abbreviation = "FL" },
                new State { Name = "Georgia", Abbreviation = "GA" },
                new State { Name = "Hawaii", Abbreviation = "HI" },
                new State { Name = "Idaho", Abbreviation = "ID" },
                new State { Name = "Illinois", Abbreviation = "IL" },
                new State { Name = "Indiana", Abbreviation = "IN" },
                new State { Name = "Iowa", Abbreviation = "IA" },
                new State { Name = "Kansas", Abbreviation = "KS" },
                new State { Name = "Kentucky", Abbreviation = "KY" },
                new State { Name = "Louisiana", Abbreviation = "LA" },
                new State { Name = "Maine", Abbreviation = "ME" },
                new State { Name = "Maryland", Abbreviation = "MD" },
                new State { Name = "Massachusetts", Abbreviation = "MA" },
                new State { Name = "Michigan", Abbreviation = "MI" },
                new State { Name = "Minnesota", Abbreviation = "MN" },
                new State { Name = "Mississippi", Abbreviation = "MS" },
                new State { Name = "Missouri", Abbreviation = "MO" },
                new State { Name = "Montana", Abbreviation = "MT" },
                new State { Name = "Nebraska", Abbreviation = "NE" },
                new State { Name = "Nevada", Abbreviation = "NV" },
                new State { Name = "New Hampshire", Abbreviation = "NH" },
                new State { Name = "New Jersey", Abbreviation = "NJ" },
                new State { Name = "New Mexico", Abbreviation = "NM" },
                new State { Name = "New York", Abbreviation = "NY" },
                new State { Name = "North Carolina", Abbreviation = "NC" },
                new State { Name = "North Dakota", Abbreviation = "ND" },
                new State { Name = "Ohio", Abbreviation = "OH" },
                new State { Name = "Oklahoma", Abbreviation = "OK" },
                new State { Name = "Oregon", Abbreviation = "OR" },
                new State { Name = "Pennsylvania", Abbreviation = "PA" },
                new State { Name = "Rhode Island", Abbreviation = "RI" },
                new State { Name = "South Carolina", Abbreviation = "SC" },
                new State { Name = "South Dakota", Abbreviation = "SD" },
                new State { Name = "Tennessee", Abbreviation = "TN" },
                new State { Name = "Texas", Abbreviation = "TX" },
                new State { Name = "Utah", Abbreviation = "UT" },
                new State { Name = "Vermont", Abbreviation = "VT" },
                new State { Name = "Virginia", Abbreviation = "VA" },
                new State { Name = "Washington", Abbreviation = "WA" },
                new State { Name = "West Virginia", Abbreviation = "WV" },
                new State { Name = "Wisconsin", Abbreviation = "WI" },
                new State { Name = "Wyoming", Abbreviation = "WY" },
                new State { Name = "District of Columbia", Abbreviation = "DC" }
            );
            context.SaveChanges();

            State alabama = context.States.Single(s => s.Name == "Alabama");
            State alaska = context.States.Single(s => s.Name == "Alaska");
            State arizona = context.States.Single(s => s.Name == "Arizona");
            State arkansas = context.States.Single(s => s.Name == "Arkansas");
            State california = context.States.Single(s => s.Name == "California");
            State colorado = context.States.Single(s => s.Name == "Colorado");
            State connecticut = context.States.Single(s => s.Name == "Connecticut");
            State delaware = context.States.Single(s => s.Name == "Delaware");
            State florida = context.States.Single(s => s.Name == "Florida");
            State georgia = context.States.Single(s => s.Name == "Georgia");
            State hawaii = context.States.Single(s => s.Name == "Hawaii");
            State idaho = context.States.Single(s => s.Name == "Idaho");
            State illinois = context.States.Single(s => s.Name == "Illinois");
            State indiana = context.States.Single(s => s.Name == "Indiana");
            State iowa = context.States.Single(s => s.Name == "Iowa");
            State kansas = context.States.Single(s => s.Name == "Kansas");
            State kentucky = context.States.Single(s => s.Name == "Kentucky");
            State louisiana = context.States.Single(s => s.Name == "Louisiana");
            State maine = context.States.Single(s => s.Name == "Maine");
            State maryland = context.States.Single(s => s.Name == "Maryland");
            State massachusetts = context.States.Single(s => s.Name == "Massachusetts");
            State michigan = context.States.Single(s => s.Name == "Michigan");
            State minnesota = context.States.Single(s => s.Name == "Minnesota");
            State mississippi = context.States.Single(s => s.Name == "Mississippi");
            State missouri = context.States.Single(s => s.Name == "Missouri");
            State montana = context.States.Single(s => s.Name == "Montana");
            State nebraska = context.States.Single(s => s.Name == "Nebraska");
            State nevada = context.States.Single(s => s.Name == "Nevada");
            State newHampshire = context.States.Single(s => s.Name == "New Hampshire");
            State newJersey = context.States.Single(s => s.Name == "New Jersey");
            State newMexico = context.States.Single(s => s.Name == "New Mexico");
            State newYork = context.States.Single(s => s.Name == "New York");
            State northCarolina = context.States.Single(s => s.Name == "North Carolina");
            State northDakota = context.States.Single(s => s.Name == "North Dakota");
            State ohio = context.States.Single(s => s.Name == "Ohio");
            State oklahoma = context.States.Single(s => s.Name == "Oklahoma");
            State oregon = context.States.Single(s => s.Name == "Oregon");
            State pennsylvania = context.States.Single(s => s.Name == "Pennsylvania");
            State rhodeIsland = context.States.Single(s => s.Name == "Rhode Island");
            State southCarolina = context.States.Single(s => s.Name == "South Carolina");
            State southDakota = context.States.Single(s => s.Name == "South Dakota");
            State tennessee = context.States.Single(s => s.Name == "Tennessee");
            State texas = context.States.Single(s => s.Name == "Texas");
            State utah = context.States.Single(s => s.Name == "Utah");
            State vermont = context.States.Single(s => s.Name == "Vermont");
            State virginia = context.States.Single(s => s.Name == "Virginia");
            State washington = context.States.Single(s => s.Name == "Washington");
            State westVirginia = context.States.Single(s => s.Name == "West Virginia");
            State wisconsin = context.States.Single(s => s.Name == "Wisconsin");
            State wyoming = context.States.Single(s => s.Name == "Wyoming");
            State districtOfColumbia = context.States.Single(s => s.Name == "District Of Columbia");

            #endregion


            #region Create General LC Program Rules
            if (lcProgram != null)
            {
                context.LoanRules.RemoveRange(context.LoanRules.Where(lr => lr.LoanProgramId == lcProgram.Id));
                context.LoanRules.AddRange(new LoanRule[] {
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = alaska.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General},
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = arizona.Id, LoanAmountThreshold = 10000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = arkansas.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = colorado.Id, LoanAmountThreshold = 75000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = connecticut.Id, LoanAmountThreshold = 15000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = florida.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = georgia.Id, LoanAmountThreshold = 3000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = hawaii.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = idaho.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = illinois.Id, LoanAmountThreshold = 40000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = indiana.Id, LoanAmountThreshold = 53000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = iowa.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = kansas.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = kentucky.Id, LoanAmountThreshold = 15000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = maine.Id, LoanAmountThreshold = 53000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = massachusetts.Id, LoanAmountThreshold = 6000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = minnesota.Id, LoanAmountThreshold = 100000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = newJersey.Id, LoanAmountThreshold = 50000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = newMexico.Id, LoanAmountThreshold = 2500m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = newYork.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = northCarolina.Id, LoanAmountThreshold = 15000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = ohio.Id, LoanAmountThreshold = 5000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = oregon.Id, LoanAmountThreshold = 50000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = pennsylvania.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = southCarolina.Id, LoanAmountThreshold = 7500m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = tennessee.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = texas.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = utah.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = virginia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = westVirginia.Id, LoanAmountThreshold = 45000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = wisconsin.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = wyoming.Id, LoanAmountThreshold = 75000m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = lcProgram.Id, StateId = districtOfColumbia.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.General } }
                );
                context.SaveChanges();
            }

            #endregion

            #region Create General BLM Program Rules

            if (blmProgram != null)
            {
                context.LoanRules.RemoveRange(context.LoanRules.Where(lr => lr.LoanProgramId == blmProgram.Id));
                context.LoanRules.AddRange(new LoanRule[] {
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = alabama.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = alaska.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = arizona.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = california.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = colorado.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = connecticut.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = delaware.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = florida.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = georgia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = illinois.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = indiana.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = iowa.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = kansas.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = kentucky.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = louisiana.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = maine.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = maryland.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = massachusetts.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = michigan.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = minnesota.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = mississippi.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = missouri.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = montana.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = nebraska.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = nevada.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = newHampshire.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = newJersey.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = newMexico.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = newYork.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = northCarolina.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = northDakota.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = ohio.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = oklahoma.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = oregon.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = rhodeIsland.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = southCarolina.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = southDakota.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = utah.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = vermont.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = washington.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = westVirginia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = wisconsin.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = wyoming.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General },
                    new LoanRule { LoanProgramId = blmProgram.Id, StateId = districtOfColumbia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.General } }
                );
                context.SaveChanges();
            }

            #endregion

            #region Create LC Residency Program Rules

            if (lcResidencyProgram != null)
            {
                context.LoanRules.RemoveRange(context.LoanRules.Where(lr => lr.LoanProgramId == lcResidencyProgram.Id));
                context.LoanRules.AddRange(new LoanRule[] {
                        new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = alaska.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency},
                        new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = arizona.Id, LoanAmountThreshold = 10000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = arkansas.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = colorado.Id, LoanAmountThreshold = 75000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = connecticut.Id, LoanAmountThreshold = 15000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = florida.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = georgia.Id, LoanAmountThreshold = 3000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = hawaii.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = idaho.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = illinois.Id, LoanAmountThreshold = 40000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = indiana.Id, LoanAmountThreshold = 53000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = iowa.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = kansas.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = kentucky.Id, LoanAmountThreshold = 15000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = maine.Id, LoanAmountThreshold = 53000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = massachusetts.Id, LoanAmountThreshold = 6000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = minnesota.Id, LoanAmountThreshold = 100000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = newJersey.Id, LoanAmountThreshold = 50000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = newMexico.Id, LoanAmountThreshold = 2500m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = newYork.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = northCarolina.Id, LoanAmountThreshold = 15000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = ohio.Id, LoanAmountThreshold = 5000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = oregon.Id, LoanAmountThreshold = 50000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = pennsylvania.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = southCarolina.Id, LoanAmountThreshold = 7500m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = tennessee.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = texas.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = utah.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = virginia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = westVirginia.Id, LoanAmountThreshold = 45000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = wisconsin.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = wyoming.Id, LoanAmountThreshold = 75000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = lcResidencyProgram.Id, StateId = districtOfColumbia.Id, LoanAmountThreshold = 25000m, LoanQualificationRestriction = LoanQualificationRestriction.Residency } }
                );
                context.SaveChanges();
            }

            #endregion

            #region Create BLM Residency Program Rules

            if (blResidencymProgram != null)
            {
                context.LoanRules.RemoveRange(context.LoanRules.Where(lr => lr.LoanProgramId == blResidencymProgram.Id));
                context.LoanRules.AddRange(new LoanRule[] {
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = alabama.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = alaska.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = arizona.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = california.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = colorado.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = connecticut.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = delaware.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = florida.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = georgia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = illinois.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = indiana.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = iowa.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = kansas.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = kentucky.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = louisiana.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = maine.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = maryland.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = massachusetts.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = michigan.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = minnesota.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = mississippi.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = missouri.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = montana.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = nebraska.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = nevada.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = newHampshire.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = newJersey.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = newMexico.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = newYork.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = northCarolina.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = northDakota.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = ohio.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = oklahoma.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = oregon.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = rhodeIsland.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = southCarolina.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = southDakota.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = utah.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = vermont.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = washington.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = westVirginia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = wisconsin.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = wyoming.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency },
                    new LoanRule { LoanProgramId = blResidencymProgram.Id, StateId = districtOfColumbia.Id, LoanAmountThreshold = 0m, LoanQualificationRestriction = LoanQualificationRestriction.Residency } }
                );
                context.SaveChanges();
            }

            #endregion

            #region Affiliates
            context.Partners.AddOrUpdate(aff => aff.AffiliateName,
                new Partner { ID = 0, AffiliateName = "Test" }
                );
            context.SaveChanges();
            #endregion OrgDomain

            #region Domains
            context.PartnerDomains.AddOrUpdate(od => od.ID,
                new PartnerDomain { ID = 0, AffliateID = 0, Domain = "test.com", Active = true }
                );
            context.SaveChanges();
            #endregion 

            #region PopulateLoanCalc
            context.Rates.AddOrUpdate(
                id => id.ID,
                new Rate { ID = 1, RateType = RateTypes.Fixed, Resident = false, Term = 3, InterestRate = 0.04250 },
                new Rate { ID = 2, RateType = RateTypes.Fixed, Resident = false, Term = 4, InterestRate = 0.04750 },
                new Rate { ID = 3, RateType = RateTypes.Fixed, Resident = false, Term = 5, InterestRate = 0.05250 },
                new Rate { ID = 4, RateType = RateTypes.Fixed, Resident = false, Term = 7, InterestRate = 0.05500 },
                new Rate { ID = 5, RateType = RateTypes.Fixed, Resident = false, Term = 10, InterestRate = 0.05750 },
                new Rate { ID = 6, RateType = RateTypes.Fixed, Resident = false, Term = 15, InterestRate = 0.06500 },
                new Rate { ID = 7, RateType = RateTypes.Fixed, Resident = false, Term = 20, InterestRate = 0.07000 },
                new Rate { ID = 8, RateType = RateTypes.Variable, Resident = false, Term = 3, InterestRate = 0.02500 },
                new Rate { ID = 9, RateType = RateTypes.Variable, Resident = false, Term = 4, InterestRate = 0.03000 },
                new Rate { ID = 10, RateType = RateTypes.Variable, Resident = false, Term = 5, InterestRate = 0.03250 },
                new Rate { ID = 11, RateType = RateTypes.Variable, Resident = false, Term = 7, InterestRate = 0.03500 },
                new Rate { ID = 12, RateType = RateTypes.Variable, Resident = false, Term = 10, InterestRate = 0.03750 },
                new Rate { ID = 13, RateType = RateTypes.Variable, Resident = false, Term = 15, InterestRate = 0.04250 },
                new Rate { ID = 14, RateType = RateTypes.Variable, Resident = false, Term = 20, InterestRate = 0.04500 },
                new Rate { ID = 15, RateType = RateTypes.Fixed, Resident = true, Term = 7, InterestRate = 0.05990 },
                new Rate { ID = 16, RateType = RateTypes.Fixed, Resident = true, Term = 10, InterestRate = 0.06125 },
                new Rate { ID = 17, RateType = RateTypes.Fixed, Resident = true, Term = 15, InterestRate = 0.06675 },
                new Rate { ID = 18, RateType = RateTypes.Fixed, Resident = true, Term = 20, InterestRate = 0.07100 });
            context.SaveChanges();
            #endregion

            #region ConfigDynmicPopulate
            context.DynamicConfigValues.AddOrUpdate(
                cv => cv.Name,
                new DynamicConfigValue { Name = "3MonthLIBOR", Description = "3-Month LIBOR used in the loan Calc", Value = "0.0028", DefaultValue = "0.0028", ID = Guid.NewGuid() },
                new DynamicConfigValue { Name = "EftReductionRate", Description = "Rate reduction if borrower is using EFT/ACH", Value = "0.0025", DefaultValue = "0.0025", ID = Guid.NewGuid() }
            );
            context.SaveChanges();
            #endregion
        }
    }
}
