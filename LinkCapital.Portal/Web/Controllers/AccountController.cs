﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Web.Services;
using Web.Services.Interfaces;
using Web.ViewModels.Account;
using Web.ViewModels.LoanCenter;
using ApplicationUser = Web.EntityModels.ApplicationUser;
using DataContext = Web.Repositories.DataContext;
using System;


namespace Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        private UserManager _userManager;
        private readonly ILoanCenterService _loanCenterService;
        private readonly IRegistrationContextService _registrationContextService;

        /// <summary>Default Constructor</summary>
        public AccountController()
            : this(new UserManager(new UserStore<ApplicationUser>(new DataContext())), new LoanCenterService(), new RegistrationContextService())
        {
        }

        /// <summary>Constructor</summary>
        /// <param name="userManager">User Manager</param>
        /// <param name="loanCenterService">Loan Center Service</param>
        public AccountController(UserManager userManager, ILoanCenterService loanCenterService, IRegistrationContextService registrationContextService)
        {
            _userManager = userManager;
            _userManager.UserValidator = new UserValidator<ApplicationUser>(_userManager) { AllowOnlyAlphanumericUserNames = false };
            _loanCenterService = loanCenterService;
            _registrationContextService = registrationContextService;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, int? userType, int? hasRegistration, string context)
        {
            return PerformLogin(returnUrl, userType, hasRegistration, context, false);
        }

        //
        // GET/POST: /Account/ReAuthenticate
        [AllowAnonymous]
        public ActionResult ReAuthenticate(string returnUrl, int? userType, int? hasRegistration, string context)
        {
            return PerformLogin(returnUrl, userType, hasRegistration, context, true);
        }


        private ActionResult PerformLogin(string returnUrl, int? userType, int? hasRegistration, string context,
            bool isReAuth)
        {
            // Create the model for us
            var model = new LoginViewModel
            {
                Context = context,
                ReturnUrl = returnUrl,
                ShowRegistration = hasRegistration != 1, // Note: Registration shouldn't be shown if the hasRegistration is '1'. This is sent by campus door and allows us to restrict our login page elements
                UserType = userType,
                IsReAuth = isReAuth
            };

            //Returning from CampusDoor should be logged out.
            if (isReAuth && Request.IsAuthenticated)
            {
                AuthenticationManager.SignOut();
                return RedirectToAction("ReAuthenticate", "Account", new
                {
                    returnUrl = returnUrl, userType = userType, hasRegistration = hasRegistration, context = context
                });
            }

            // Logged in users shouldn't see login
            if (Request.IsAuthenticated) 
            {   
                if (string.IsNullOrEmpty(model.ReturnUrl))
                {
                    return RedirectToAction("Index", "Home");
                }

                if (model.ReturnUrl.ToLower().Contains("admin") && !AuthenticationManager.User.IsInRole("Admin"))
                {
                    
                    return RedirectToAction("Index", "Home");
                }
                
                return Redirect(model.ReturnUrl);
            }

            return View("Login", model);
        }

        /// <summary>Transfer the User to the Url and back into their Campus Door Session</summary>
        /// <param name="currentUser">Current User</param>
        /// <param name="returnUrl">Return Url</param>
        /// <param name="context">Context to return to the Campus Door Session</param>
        /// <returns>Action Result</returns>
        private ActionResult TransferToCampusDoorSession(ApplicationUser currentUser, string returnUrl, string context)
        {
            // Delete any registration context for this user
            _registrationContextService.DeleteRegistrationContext(currentUser.Id);

            var loanApplicationViewModel = new LoanApplicationViewModel
            {
                Payload = _loanCenterService.GetCampusDoorToken(currentUser, context),
                Url = returnUrl,
                HttpMethod = "POST"
            };
            
            return View("~/Views/LoanCenter/LoanApplication.cshtml", loanApplicationViewModel);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            var result = await PerformLoginPost(model);
            return result;



        }

        private async Task<ActionResult> PerformLoginPost(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userManager.Find(model.Email, model.Password);
                if (user != null)
                {
                    if (user.EmailConfirmed)
                    {
                        await SignInAsync(user, false);

                        return (model.IsReAuth && !string.IsNullOrWhiteSpace(model.ReturnUrl)) ? TransferToCampusDoorSession(user, model.ReturnUrl, model.Context) : RedirectToLocal(model.ReturnUrl);
                    }
                    var confirmToken = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var confirmCallbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token = confirmToken }, protocol: Request.Url.Scheme);
                    await _userManager.SendEmailAsync(user.Id, "Confirm Link Capital Account", string.Format("Please confirm your Link Capital account by clicking <a href=\"{0}\">here</a>.", confirmCallbackUrl));

                    ModelState.AddModelError("", "You must confirm your email address before signing in. Please check your email.");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Login", model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            // Logged in users shouldn't see register
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    UserName = model.Email,
                    Email = model.Email,
                    QualifyingEmail = model.QualifyingEmail,
                    FirstName = model.FirstName.Trim(),
                    LastName = model.LastName.Trim(),
                    LeadStatus = "Create Portal Account",
                    LastChanged = DateTime.Now,
                    LastSyncDate = DateTime.Parse("01/01/1970"),
                    IsCosigner = model.IsCosigner
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var confirmToken = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var confirmCallbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token = confirmToken }, protocol: Request.Url.Scheme);
                    await _userManager.SendEmailAsync(user.Id, "Confirm Link Capital Account", string.Format("Please confirm your Link Capital account by clicking <a href=\"{0}\">here</a>.", confirmCallbackUrl));


                    if (user.Email != user.QualifyingEmail && user.QualifyingEmail != null)
                    {
                        // Qualifying Email Validation
                        var qeToken = await _userManager.GenerateQualifyingToken(user.Id);
                        var qeConfirmCallbackUrl = Url.Action("ConfirmQualifyingEmail", "Account", new { userId = user.Id, token = qeToken }, protocol: Request.Url.Scheme);
                        await _userManager.SendQulEmailAsync(user.Id, "Confirm Link Capital Qualifying Email", string.Format("Please confirm the Qualifying Email attached to your Link Capital account by clicking <a href=\"{0}\">here</a>.", confirmCallbackUrl));
                    }

                    if (!string.IsNullOrEmpty(model.ReturnUrl) && model.IsReAuth)
                    {
                        _registrationContextService.CreateRegistrationContext(user.Id, model.ReturnUrl, model.Context);
                    }

                    return RedirectToAction("ConfirmEmailSent");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await _userManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message, string returnUrl)
        {
            var currentUser = _userManager.FindById(User.Identity.GetUserId());

            ViewBag.StatusMessage =
				message == ManageMessageId.ChangeProfileSuccess ? "Your profile has been changed."
				: message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
				: message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword(currentUser);
            ViewBag.ReturnUrl = Url.Action("Manage");

            var model = new ManageUserViewModel
            {
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName,
                Email = currentUser.Email,
                QualifyingEmail = currentUser.QualifyingEmail,
                CancelUrl = returnUrl ?? (Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : null)
            };

            return View(model);
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            var currentUser = _userManager.FindById(User.Identity.GetUserId());
            bool hasPassword = HasPassword(currentUser);
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (ModelState.IsValid)
            {
                bool isSuccess = true;

                // Update the user
                currentUser.FirstName = model.FirstName;
                currentUser.LastName = model.LastName;
                currentUser.Email = model.Email;
                currentUser.UserName = model.Email;
                currentUser.QualifyingEmail = model.QualifyingEmail;
                var result = await _userManager.UpdateAsync(currentUser);
                if (!result.Succeeded)
                {
                    isSuccess = false;
                    AddErrors(result);
                }

                // Update the password if needed
                if (hasPassword && (!string.IsNullOrEmpty(model.ConfirmPassword) || !string.IsNullOrEmpty(model.NewPassword)))
                {
                    IdentityResult passwordResult = await _userManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (passwordResult.Succeeded)
                    {
                        if (isSuccess)
                        {
                            return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess, returnUrl = model.CancelUrl });
                        }
                    }
                    else
                    {
                        isSuccess = false;
                        AddErrors(passwordResult);
                    }
                }
                else
                {
                    // User does not have a password so remove any validation errors caused by a missing OldPassword field
                    ModelState state = ModelState["OldPassword"];
                    if (state != null)
                    {
                        state.Errors.Clear();
                    }

                    if (ModelState.IsValid)
                    {
                        IdentityResult passwordResult = await _userManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                        if (passwordResult.Succeeded)
                        {
                            return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                        }
                        else
                        {
                            AddErrors(passwordResult);
                        }
                    }
                }

                if (isSuccess)
                {
                    return RedirectToAction("Manage", new { Message = ManageMessageId.ChangeProfileSuccess, returnUrl = model.CancelUrl });
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await _userManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await _userManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl, string LoginProvider)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        var confirmToken = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var confirmCallbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, token = confirmToken }, protocol: Request.Url.Scheme);
                        await _userManager.SendEmailAsync(user.Id, "Confirm Link Capital Account", string.Format("Please confirm your Link Capital account by clicking <a href=\"{0}\">here</a>.", confirmCallbackUrl));

                        return View("ConfirmEmailSent");
                        //await SignInAsync(user, isPersistent: false);
                        //return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.LoginProvider = LoginProvider;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        //
        // GET: /Account/ConfirmEmailSent
        [AllowAnonymous]
        public ActionResult ConfirmEmailSent()
        {
            return View();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(ConfirmEmailViewModel model)
        {
            if(string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.Token))
            {
                ModelState.AddModelError("", "Invalid email confirmation.");
                return View();
            }

            var result = await _userManager.ConfirmEmailAsync(model.UserId, model.Token);

            if(!result.Succeeded)
            {
                AddErrors(result);
                return View();
            }

            var user = await _userManager.FindByIdAsync(model.UserId);

            // if the two email addresses are the same than set the QE confirmed also
            if(user.Email == user.QualifyingEmail)
            {
                user.QualifyingEmailConfirmed = true;
            }

            await _userManager.UpdateAsync(user);

            // We signed in, so now let's go to step 2
            if(user != null)
            {
                await SignInAsync(user, false);
                
                var regContext = _registrationContextService.GetRegistrationContext(user.Id);
                if (regContext != null)
                {
                    return TransferToCampusDoorSession(user, regContext.ReturnUrl, regContext.Context);
                }
                
                return RedirectToAction("Index", "LoanCenter");
            }

            ModelState.AddModelError("", "We were unable to find your user account.");
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmQualifyingEmail(ConfirmEmailViewModel model)
        {
            if (string.IsNullOrEmpty(model.UserId) || string.IsNullOrEmpty(model.Token))
            {
                ModelState.AddModelError("", "Invalid qualifying email confirmation.");
                return View();
            }

            bool result = await _userManager.ConfirmQualifyingEmailAsync(model.UserId, model.Token);

            // little old school but tired of fighting the identity construct
            foreach(ApplicationUser currentUser in _userManager.Users)
            {
                if(currentUser.Id == model.UserId)
                {
                    currentUser.QualifyingEmailConfirmed = true;
                }

                await _userManager.UpdateAsync(currentUser);
            }

            // shouldn't we just move the user back to the homepage... or a previous page they where on - Aaron V.
            // TODO: Finish this section
            var user = await _userManager.FindByIdAsync(model.UserId);
            if(user != null)
            {
                if (user.EmailConfirmed == true)
                {
                    await SignInAsync(user, false);

                    var regContext = _registrationContextService.GetRegistrationContext(user.Id);
                    if (regContext != null)
                    {
                        return TransferToCampusDoorSession(user, regContext.ReturnUrl, regContext.Context);
                    }

                    return RedirectToAction("Index", "LoanCenter");
                }
                else
                {
                    ModelState.AddModelError("", "You must confirm your personal email address before moving foreword, please check you personal email inbox.");
                    return View();
                }
            }

            ModelState.AddModelError("", "We were unable to find your user account.");
            return View();
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user.Id))) // TODO: SH - we shouldn't really show this to the user
                {
                    ModelState.AddModelError("", "The user either does not exist or is not confirmed.");
                    return View();
                }

                // TODO: SH - For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                string token = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                var resetCallbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = token }, protocol: Request.Url.Scheme);
                await _userManager.SendEmailAsync(user.Id, "Reset Link Capital Password", "Please reset your Link Capital password by clicking <a href=\"" + resetCallbackUrl + "\">here</a>.");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

		//
		// GET: /Account/ResetPassword
		[AllowAnonymous]
		public ActionResult ResetPassword(string code)
		{
			if (code == null)
			{
				return View("Error");
			}
			return View();
		}

		//
		// POST: /Account/ResetPassword
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
		{
			if (ModelState.IsValid)
			{
				var user = await _userManager.FindByNameAsync(model.Email);
				if (user == null)
				{
					ModelState.AddModelError("", "No user found.");
					return View();
				}
				IdentityResult result = await _userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
				if (result.Succeeded)
				{
					return RedirectToAction("ResetPasswordConfirmation", "Account");
				}
				else
				{
					AddErrors(result);
					return View();
				}
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}

		//
		// GET: /Account/ResetPasswordConfirmation
		[AllowAnonymous]
		public ActionResult ResetPasswordConfirmation()
		{
			return View();
		}

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = _userManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        [AllowAnonymous]
        public ActionResult UsersUnusedLoginProvidersList()
        {
            IEnumerable<AuthenticationDescription> allLoginProviders = HttpContext.GetOwinContext().Authentication.GetExternalAuthenticationTypes();
            IEnumerable<UserLoginInfo> linkedLoginProviders = _userManager.GetLogins(User.Identity.GetUserId());
            var nonLinkedLoginProviders = allLoginProviders.Where(m => !(linkedLoginProviders.Any(n => n.LoginProvider == m.AuthenticationType)));
            return (ActionResult)PartialView("_AddExternalLoginsListPartial", new Web.ViewModels.Account.AddExternalLoginsListViewModel { Action = "LinkLogin", ReturnUrl = "/Account/Manage", FilteredProviders = nonLinkedLoginProviders });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword(ApplicationUser user = null)
        {
            user = user ?? _userManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
			ChangeProfileSuccess,
			ChangePasswordSuccess,
			SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}