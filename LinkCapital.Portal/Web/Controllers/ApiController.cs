﻿using System.Collections.Generic;
using System.Linq;
using Web.ViewModels.LoanCalc;
using Web.EntityModels;
using System.Web.Mvc;
using Web.Repositories;
using System;
using System.Web.Http;
using Web.EntityModels.LoanCalculator;
using Web.Filters;

namespace Web.Controllers
{
    [AllowCors]
    public class ApiController : Controller
    {
        private DataContext Context;

        public ApiController()
        {
            Context = new DataContext();
        }

        // POST api/<controller>
        [AllowCors]
        public string TermsAndConditions()
        {
            return Context.DynamicConfigValues.FirstOrDefault(x => x.Name == "TermsAndConditions").Value;
        }

        [AllowCors]
        public string PrivacyPolicy()
        {
            return Context.DynamicConfigValues.FirstOrDefault(x => x.Name == "PrivacyPolicy").Value;
        }
    }
}