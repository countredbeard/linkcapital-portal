﻿using System.Collections.Generic;
using System.Linq;
using Web.ViewModels.LoanCalc;
using Web.EntityModels;
using System.Web.Mvc;
using Web.Repositories;
using System;
using System.Web.Http;
using Web.EntityModels.LoanCalculator;
using Web.Filters;

namespace Web.Controllers
{
    public class LoanSubmissionsController : Controller
    {
        private DataContext Context;

        public LoanSubmissionsController()
        {
            Context = new DataContext();
        }

        [System.Web.Mvc.HttpGet]
        public JsonResult All()
        {
            var submissions = Context.LoanSubmissions.ToList();
            return Json(submissions, JsonRequestBehavior.AllowGet);
        }
    }
}