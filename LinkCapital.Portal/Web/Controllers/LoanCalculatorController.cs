﻿using System.Collections.Generic;
using System.Linq;
using Web.ViewModels.LoanCalc;
using Web.EntityModels;
using System.Web.Mvc;
using Web.Repositories;
using System;
using System.Web.Http;
using Web.EntityModels.LoanCalculator;
using Web.Filters;

namespace Web.Controllers
{
    [AllowCors]
    public class LoanCalculatorController : Controller
    {
        private DataContext Context;

        public LoanCalculatorController()
        {
            Context = new DataContext();
        }

        // why in the world is this so explicitly required?
        [System.Web.Mvc.HttpOptions]
        [AllowCors]
        public void ConsolidationProgram() { }

        [System.Web.Mvc.HttpOptions]
        [AllowCors]
        public void ResidentProgram() { }

        // POST api/<controller>
        [System.Web.Mvc.HttpPost]
        public JsonResult ConsolidationProgram([FromBody] List<ExistingLoan> existingLoans, float tier, bool assumeEftRateReduction = true)
        {
            ConsolidationProgramViewModel viewModel = new ConsolidationProgramViewModel();

            List<Rate> rates = Context.Rates.Where(cr => !cr.Resident).ToList();
            
            var libor = Convert.ToDouble(Context.DynamicConfigValues.FirstOrDefault(cv => cv.Name == "3MonthLIBOR").Value);
            var eftRateReduction = Convert.ToDouble(Context.DynamicConfigValues.FirstOrDefault(cv => cv.Name == "EftReductionRate").Value);

            CalculateLoanAverages(existingLoans, viewModel);
            
            foreach (var ratesByTerm in rates.Where(x => x.RateType == RateTypes.Fixed).GroupBy(x => x.Term))
            {
                var minRate = ratesByTerm.Where(x => x.Tier == Tiers.One).First().InterestRate;
                var maxRate = ratesByTerm.Where(x => x.Tier == Tiers.Five).First().InterestRate;

                if (assumeEftRateReduction)
                {
                    minRate -= eftRateReduction;
                    maxRate -= eftRateReduction;
                }

                var rateRange = string.Format("{0:N3}% - {1:N3}%", minRate * 100, maxRate * 100);

                var rate = minRate + ((1 - (tier / 4.0)) * (maxRate - minRate));
                //if (assumeEftRateReduction) rate -= eftRateReduction;

                var itemToAdd = CalculateConsolidationItem(RateTypes.Fixed, rate, ratesByTerm.Key / 12, viewModel.TotalBalance);
                itemToAdd.RateRange = rateRange;

                if (viewModel.LifetimeCost - itemToAdd.LifetimeCost > 0)
                {
                    itemToAdd.LifetimeSavings = viewModel.LifetimeCost - itemToAdd.LifetimeCost;
                }
                else
                {
                    itemToAdd.LifetimeSavings = 0;
                }

                if (viewModel.CurrentMonthlyPayment - itemToAdd.MonthlyPayment > 0)
                {
                    itemToAdd.MonthlySavings = viewModel.CurrentMonthlyPayment - itemToAdd.MonthlyPayment;
                }
                else
                {
                    itemToAdd.MonthlySavings = 0;
                }

                viewModel.FixedRates.Add(itemToAdd);
            }

            foreach (var ratesByTerm in rates.Where(x => x.RateType == RateTypes.Variable).GroupBy(x => x.Term))
            {
                var minRate = ratesByTerm.Where(x => x.Tier == Tiers.One).First().InterestRate + libor;
                var maxRate = ratesByTerm.Where(x => x.Tier == Tiers.Five).First().InterestRate + libor;

                if (assumeEftRateReduction)
                {
                    minRate -= eftRateReduction;
                    maxRate -= eftRateReduction;
                }

                var rateRange = string.Format("{0:N3}% - {1:N3}%", minRate*100, maxRate*100);

                var rate = minRate + ((1 - (tier/4))*(maxRate - minRate)); // + libor;
                //if (assumeEftRateReduction) rate -= eftRateReduction;

                // notes: rate should be percent as double (i.e., 0.0525 for 5.25%), ratesByTerm.Key is months so make it years
                var itemToAdd = CalculateConsolidationItem(RateTypes.Variable, rate, ratesByTerm.Key / 12, viewModel.TotalBalance);
                itemToAdd.RateRange = rateRange;

                if (viewModel.LifetimeCost - itemToAdd.LifetimeCost > 0)
                {
                    itemToAdd.LifetimeSavings = viewModel.LifetimeCost - itemToAdd.LifetimeCost;
                }
                else
                {
                    itemToAdd.LifetimeSavings = 0;
                }

                if (viewModel.CurrentMonthlyPayment - itemToAdd.MonthlyPayment > 0)
                {
                    itemToAdd.MonthlySavings = viewModel.CurrentMonthlyPayment - itemToAdd.MonthlyPayment;
                }
                else
                {
                    itemToAdd.MonthlySavings = 0;
                }

                viewModel.VariableRates.Add(itemToAdd);
            }

            return Json(viewModel);
        }

        private ConsolidationProgramItem CalculateConsolidationItem(RateTypes type, double rate, int term, double totalBalance)
        {
            var itemToAdd = new ConsolidationProgramItem
            {
                RateType = type,
                Term = term,
                InterestRate = rate
            };

            itemToAdd.MonthlyPayment = Pmt(itemToAdd.InterestRate, term, totalBalance);
            itemToAdd.LifetimeCost = term * itemToAdd.MonthlyPayment * 12;

            return itemToAdd;
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult ResidentProgram([FromBody] List<ExistingLoan> existingLoans, float tier, int yearsRemainingInResidency, bool assumeEftRateReduction = true)
        {
            ResidentProgramViewModel viewModel = new ResidentProgramViewModel();
            List<Rate> rates = Context.Rates.Where(cr => cr.Resident && cr.RateType == RateTypes.Fixed).ToList();
            double eftRateReduction = Convert.ToDouble(Context.DynamicConfigValues.FirstOrDefault(cv => cv.Name == "EftReductionRate").Value);

            CalculateLoanAverages(existingLoans, yearsRemainingInResidency, viewModel);

            foreach (var ratesByTerm in rates.GroupBy(x => x.Term))
            {
                var minRate = ratesByTerm.Where(x => x.Tier == Tiers.One).First().InterestRate;
                var maxRate = ratesByTerm.Where(x => x.Tier == Tiers.Five).First().InterestRate;

                if (assumeEftRateReduction)
                {
                    minRate -= eftRateReduction;
                    maxRate -= eftRateReduction;
                }

                var rateRange = string.Format("{0:N3}% - {1:N3}%", minRate * 100, maxRate * 100);

                var rate = minRate + ((1 - (tier / 4.0)) * (maxRate - minRate));

                var itemToAdd = new ResidentProgramItem
                {
                    RateType = RateTypes.Fixed,
                    Term = ratesByTerm.Key / 12,
                    InterestRate = rate,
                    RateRange = rateRange,
                    MonthlyPaymentPre = 0
                };

                var rateReflectingEftReduction = assumeEftRateReduction
                    ? itemToAdd.InterestRate + eftRateReduction
                    : itemToAdd.InterestRate;
                var deferredBalance = viewModel.TotalBalance * (1 + rateReflectingEftReduction * yearsRemainingInResidency);

                itemToAdd.MonthlyPaymentPost = Pmt(itemToAdd.InterestRate, itemToAdd.Term, deferredBalance);

                itemToAdd.MonthlySavingsPre = (viewModel.TotalBalance * (viewModel.AverageInterestRate / 12)) -
                                              (viewModel.TotalBalance * rateReflectingEftReduction / 12);
                if (itemToAdd.MonthlySavingsPre < 0) itemToAdd.MonthlySavingsPre = 0;

                itemToAdd.MonthlySavingsPost = (viewModel.CurrentMonthlyPayment - itemToAdd.MonthlyPaymentPost);
                if (itemToAdd.MonthlySavingsPost < 0) itemToAdd.MonthlySavingsPost = 0;

                itemToAdd.LifetimeSavingsPre = (viewModel.TotalBalance * viewModel.AverageInterestRate * yearsRemainingInResidency) -
                                               (viewModel.TotalBalance * rateReflectingEftReduction * yearsRemainingInResidency);
                if (itemToAdd.LifetimeSavingsPre < 0) itemToAdd.LifetimeSavingsPre = 0;

                itemToAdd.LifetimeCost = itemToAdd.Term * itemToAdd.MonthlyPaymentPost * 12;

                itemToAdd.LifetimeSavingsPost = (viewModel.LifetimeCost - itemToAdd.LifetimeCost);
                if (itemToAdd.LifetimeSavingsPost < 0) itemToAdd.LifetimeSavingsPost = 0;

                viewModel.FixedRates.Add(itemToAdd);
            }

            return Json(viewModel);

        }

        private double Pmt(double rate, int term, double balance)
        {
            // ensure rate is a percent divided by 12
            if (rate > 1) rate /= 100;
            rate /= 12;

            // ensure term is in months, not years
            term *= 12;

            var denominator = Math.Pow((1 + rate), term) - 1;

            return (rate + (rate / denominator)) * balance;
        }

        private void CalculateLoanAverages(IList<ExistingLoan> existingLoans, ConsolidationProgramViewModel viewModel)
        {
            var weightedRates = new List<double>();

            viewModel.TotalBalance = existingLoans.Sum(x => x.Balance);

            foreach (var loan in existingLoans)
            {
                var weight = loan.Balance / viewModel.TotalBalance;
                weightedRates.Add(loan.InterestRate * weight);

                loan.MonthlyPayment = Pmt(loan.InterestRate, loan.RemainingTerm, loan.Balance);
                loan.LifetimeCost = loan.MonthlyPayment * loan.RemainingTerm * 12;
            }

            viewModel.LifetimeCost = existingLoans.Sum(x => x.LifetimeCost);
            viewModel.CurrentMonthlyPayment = existingLoans.Sum(x => x.MonthlyPayment);
            viewModel.AverageInterestRate = weightedRates.Average();
        }

        private void CalculateLoanAverages(IList<ExistingLoan> existingLoans, int yearsRemainingInResidency, ResidentProgramViewModel viewModel)
        {
            var weightedRates = new List<double>();

            viewModel.TotalBalance = existingLoans.Sum(x => x.Balance);

            foreach (var loan in existingLoans)
            {
                var weight = loan.Balance / viewModel.TotalBalance;
                weightedRates.Add(loan.InterestRate * weight);

                loan.MonthlyPayment = Pmt(loan.InterestRate, loan.RemainingTerm, (loan.Balance + (loan.Balance * loan.InterestRate * yearsRemainingInResidency)));
                loan.LifetimeCost = loan.MonthlyPayment * loan.RemainingTerm * 12;
            }

            viewModel.LifetimeCost = existingLoans.Sum(x => x.LifetimeCost);
            viewModel.CurrentMonthlyPayment = Math.Round(existingLoans.Sum(x => x.MonthlyPayment));
            viewModel.AverageInterestRate = weightedRates.Average();
        }
    }
}