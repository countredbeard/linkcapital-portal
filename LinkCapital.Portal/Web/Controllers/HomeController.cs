﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
	[Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // Authenticated users should go straight to the loan center
            if(Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "LoanCenter");
            }
            return View();
        }
    }
}