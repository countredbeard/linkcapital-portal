﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Web.EntityModels;
using Web.Services;
using Web.Services.Interfaces;
using Web.Utilities;
using Web.ViewModels.LoanCenter;
using ApplicationUser = Web.EntityModels.ApplicationUser;
using DataContext = Web.Repositories.DataContext;

namespace Web.Controllers
{
    [Authorize]
    public class LoanCenterController : Controller
    {
        public LoanCenterController()
            : this(new UserManager(new UserStore<ApplicationUser>(new DataContext())), new LoanCenterService(), new LoanProgramService(), new StateService(), new LoanSubmissionService())
        {
        }

        public LoanCenterController(UserManager userManager, ILoanCenterService loanCenterService, ILoanProgramService loanProgramService, IStateService stateService, ILoanSubmissionService loanSubmissionService)
        {
            UserManager = userManager;
            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };

			LoanCenterService = loanCenterService;
			LoanProgramService = loanProgramService;
			StateService = stateService;
            LoanSubmissionService = loanSubmissionService;
        }

        public UserManager UserManager { get; private set; }
		public ILoanCenterService LoanCenterService { get; private set; }
		public ILoanProgramService LoanProgramService { get; private set; }
		public IStateService StateService { get; private set; }
        public ILoanSubmissionService LoanSubmissionService { get; private set; }

        public async Task<ActionResult> Index()
        {
            var currentUser = UserManager.FindById(User.Identity.GetUserId());
            

            return View();
        }

        [HttpGet]
        public ActionResult LoanInformation()
        {
            var currentUser = UserManager.FindById(User.Identity.GetUserId());
            currentUser.LeadStatus = "Started Application";
            UserManager.Update(currentUser);

            ViewBag.StateNames = StateService.GetStates().Select(s => s.Name);
			
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoanInformation(LoanInformationViewModel model)
        {
            if (ModelState.IsValid)
            {
                LoanQualificationRestriction loanRestriction = (model.IsMedicalResident.HasValue && model.IsMedicalResident.Value) ?
                                                                        LoanQualificationRestriction.Residency :
                                                                        LoanQualificationRestriction.General;
                LoanProgram loanProgram = LoanProgramService.GetLoanProgram(model.StateOfResidence, model.LoanAmount, loanRestriction);

                // if the loan program is null than there is no program for that combination of state and loanrestriction
                if (loanProgram != null)
                {
                    if (model.LoanAmount > 15000 && model.LoanAmount < 450000)
                    {
                        var currentUser = UserManager.FindById(User.Identity.GetUserId());
                        currentUser.LeadStatus = "Left Portal";
                        UserManager.Update(currentUser);

                        LoanSubmissionService.SaveLoanSubmission(currentUser.Id, model.StateOfResidence, model.LoanAmount,
                            model.IsMedicalResident ?? false, loanProgram.Name, loanProgram.Url);

                        return ReturnLoanView(Web.Utilities.Configuration.CampusDoorNewLoanAppUrl + loanProgram.Url);
                    }
                    else
                    {
                        return View("NotAvailableLoanAmount");
                    }
                }
                else
                {
                    return View("NotAvailable");
                }
            }
            

            ViewBag.StateNames = StateService.GetStates().Select(s => s.Name);
            return View(model);
        }


        [HttpGet]
        public ActionResult CheckList()
        {
            return File("~/downlods/LinkCapital_Application_Checklist.pdf", "application/pdf", "LinkCapital_Application_Checklist.pdf");
        }

        private ViewResult ReturnLoanView(string url)
        {
            var currentUser = UserManager.FindById(User.Identity.GetUserId());

            var loanAppModel = new LoanApplicationViewModel();
            loanAppModel.HttpMethod = "POST";
            loanAppModel.Payload = LoanCenterService.GetCampusDoorToken(currentUser);
            loanAppModel.Url = url;

            return View("LoanApplication", loanAppModel);
        }

        /// <summary>Transfer to Campus Door's My Account</summary>
        /// <returns>Return</returns>
        [HttpGet]
        public ActionResult ContinueApplication()
        {
            var currentUser = UserManager.FindById(User.Identity.GetUserId());
            LoanApplicationViewModel viewModel = new LoanApplicationViewModel
            {
                HttpMethod = "POST",
                Payload = LoanCenterService.GetCampusDoorToken(currentUser),
                Url = Configuration.CampusDoorMyAccountUrl
            };

            return View("LoanApplication", viewModel);
        }

        [HttpGet]
        public ActionResult ManageLoan()
        {
            ManageLoanViewModel viewModel = new ManageLoanViewModel
            {
                HttpMethod = "Post",
                Payload = string.Empty,
                Url = "https://myaccount.studentloan.org"
            };

            return View("LoanManagementView", viewModel);
        }

        [HttpGet]
        public ActionResult NotAvailable()
        {
            return View();
        }
    }
}