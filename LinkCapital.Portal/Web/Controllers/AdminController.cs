﻿using System.Collections.Generic;
using System.Linq;
using Web.ViewModels.LoanCalc;
using Web.EntityModels;
using System.Web.Mvc;
using Web.Repositories;
using System;
using System.Web.Http;
using Web.EntityModels.LoanCalculator;
using Web.Filters;
using Web.ViewModels.Admin;
using Web.Services;
using Web.Services.Interfaces;
using System;

namespace Web.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        DataContext data = new DataContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoanSubmissions()
        {
            return View();
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult SetPrograms([FromBody]List<LoanProgram> programs)
        {
            foreach(LoanProgram currentProgram in programs) 
            {
                LoanProgram tempData = data.LoanPrograms.Find(currentProgram.Id);
                tempData.Id = currentProgram.Id;
                tempData.Name = currentProgram.Name;
                tempData.Url = currentProgram.Url;
                //tempData = currentProgram;
                data.SaveChanges();
            }

            return Json("Valid");
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult SetRates([FromBody]List<Rate> rates)
        {
            foreach(Rate currentRate in rates)
            {
                Rate tempData = data.Rates.Find(currentRate.ID);
                tempData.ID = currentRate.ID;
                tempData.InterestRate = currentRate.InterestRate;
                tempData.RateType = currentRate.RateType;
                tempData.Resident = currentRate.Resident;
                tempData.Term = currentRate.Term;
                tempData.Tier = currentRate.Tier;
                tempData.FicoMin = currentRate.FicoMin;
                tempData.FicoMax = currentRate.FicoMax;
                data.SaveChanges();
            }

            return Json("Valid");
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult SetDynamicValues([FromBody]List<DynamicConfigValue> values)
        {
            foreach(DynamicConfigValue currentValue in values)
            {
                DynamicConfigValue tempData = data.DynamicConfigValues.Find(currentValue.ID);
                tempData.ID = currentValue.ID;
                tempData.Name = currentValue.Name;
                tempData.Description = currentValue.Description;
                tempData.DefaultValue = currentValue.DefaultValue;
                tempData.Value = currentValue.Value;
                data.SaveChanges();
            }

            return Json("Valid");
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult SetLoanRestrictions([FromBody]List<LoanRule> rules)
        {
            foreach(LoanRule currentRule in rules)
            {
                LoanRule tempData = data.LoanRules.Find(currentRule.Id);
                tempData.LoanAmountThreshold = currentRule.LoanAmountThreshold;
                tempData.Active = currentRule.Active;
                data.SaveChanges();
            }

            return Json("Valid");
        }

        public JsonResult GetPrograms()
        {
            List<LoanProgram> programsToReturn = data.LoanPrograms.ToList();
            return Json(programsToReturn, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLoanRestrictions()
        {
            return Json(data.LoanRules.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDynamicValues()
        {
            return Json(data.DynamicConfigValues.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRates()
        {
            var vm = new RatesViewModel
            {
                Rates = data.Rates.ToList()
            };

            return Json(vm, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            data.Dispose();
            base.Dispose(disposing);
        }
    }
}