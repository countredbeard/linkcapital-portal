﻿var AdminApp = angular.module('AdminApp', ['textAngular', 'ngTable']);

// Serivce to retive data
AdminApp.factory('WebData', ['$http', function ($http) {
    var WebData = {};
    WebData.GetPrograms = function () {
        return $http.get('/Admin/GetPrograms');
    };
    WebData.GetRestrictions = function () {
        return $http.get('/Admin/GetLoanRestrictions');
    };
    WebData.GetDynamicValues = function () {
        return $http.get('/Admin/GetDynamicValues');
    };
    WebData.GetRates = function () {
        return $http.get('/Admin/GetRates');
    };
    WebData.SetPrograms = function (obj) {
        return $http({
            method: "POST",
            url: '/Admin/SetPrograms',
            headers: {
                'Content-type': 'application/json'
            },
            data: obj
        });
    }
    WebData.SetRates = function (obj) {
        return $http({
            method: "POST",
            url: '/Admin/SetRates',
            headers: {
                'Content-type': 'application/json'
            },
            data: obj
        }).success(function () {
            alert(data.msg);
        });
    }
    WebData.SetDynamicValues = function (obj) {
        return $http({
            method: "POST",
            url: '/Admin/SetDynamicValues',
            headers: {
                'Content-type': 'application/json'
            },
            data: obj
        }).success(function () {
            alert(data.msg);
        });
    }
    WebData.SetLoanRestrictions = function (obj) {
        return $http({
            method: "POST",
            url: '/Admin/SetLoanRestrictions',
            headers: {
                'Content-type': 'application/json'
            },
            data: obj
        }).success(function () {
            alert(data.msg);
        });
    }

    WebData.GetLoanSubmissions = function() {
        return $http.get('/LoanSubmissions/All');
    }

    return WebData;
}]);

// Controller to bind to
AdminApp.controller('AdminController', function ($scope, WebData) {
    $scope.saving = { LoanProgram: false, LoanRes: false, Rates: false, Settings: false }
    $scope.saved = { LoanProgram: false, LoanRes: false, Rates: false, Settings: false }

    $scope.submitPrograms = function () {
        $scope.saving.LoanProgram = true;
        WebData.SetPrograms($scope.programs).success(function () {
            $scope.saved.LoanProgram = true;
            $scope.saving.LoanProgram = false;
        });
    }

    $scope.submitRates = function () {
        $scope.saving.Rates = true;

        // update the tier fico min/max based on first entry
        angular.forEach($scope.Rates.Consolidation.Fixed.Tier1, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Fixed.Tier1[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Fixed.Tier1[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Fixed.Tier2, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Fixed.Tier2[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Fixed.Tier2[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Fixed.Tier3, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Fixed.Tier3[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Fixed.Tier3[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Fixed.Tier4, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Fixed.Tier4[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Fixed.Tier4[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Fixed.Tier5, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Fixed.Tier5[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Fixed.Tier5[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Variable.Tier1, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Variable.Tier1[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Variable.Tier1[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Variable.Tier2, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Variable.Tier2[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Variable.Tier2[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Variable.Tier3, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Variable.Tier3[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Variable.Tier3[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Variable.Tier4, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Variable.Tier4[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Variable.Tier4[0].FicoMax;
        });
        angular.forEach($scope.Rates.Consolidation.Variable.Tier5, function(rate) {
            rate.FicoMin = $scope.Rates.Consolidation.Variable.Tier5[0].FicoMin;
            rate.FicoMax = $scope.Rates.Consolidation.Variable.Tier5[0].FicoMax;
        });
        angular.forEach($scope.Rates.Resident.Fixed.Tier1, function(rate) {
            rate.FicoMin = $scope.Rates.Resident.Fixed.Tier1[0].FicoMin;
            rate.FicoMax = $scope.Rates.Resident.Fixed.Tier1[0].FicoMax;
        });
        angular.forEach($scope.Rates.Resident.Fixed.Tier2, function(rate) {
            rate.FicoMin = $scope.Rates.Resident.Fixed.Tier2[0].FicoMin;
            rate.FicoMax = $scope.Rates.Resident.Fixed.Tier2[0].FicoMax;
        });
        angular.forEach($scope.Rates.Resident.Fixed.Tier3, function(rate) {
            rate.FicoMin = $scope.Rates.Resident.Fixed.Tier3[0].FicoMin;
            rate.FicoMax = $scope.Rates.Resident.Fixed.Tier3[0].FicoMax;
        });
        angular.forEach($scope.Rates.Resident.Fixed.Tier4, function(rate) {
            rate.FicoMin = $scope.Rates.Resident.Fixed.Tier4[0].FicoMin;
            rate.FicoMax = $scope.Rates.Resident.Fixed.Tier4[0].FicoMax;
        });
        angular.forEach($scope.Rates.Resident.Fixed.Tier5, function(rate) {
            rate.FicoMin = $scope.Rates.Resident.Fixed.Tier5[0].FicoMin;
            rate.FicoMax = $scope.Rates.Resident.Fixed.Tier5[0].FicoMax;
        });

        // pivot all the programs back into one object and post it
        var rates = [].concat(
            $scope.Rates.Consolidation.Fixed.Tier1,
            $scope.Rates.Consolidation.Fixed.Tier2,
            $scope.Rates.Consolidation.Fixed.Tier3,
            $scope.Rates.Consolidation.Fixed.Tier4,
            $scope.Rates.Consolidation.Fixed.Tier5,
            $scope.Rates.Consolidation.Variable.Tier1,
            $scope.Rates.Consolidation.Variable.Tier2,
            $scope.Rates.Consolidation.Variable.Tier3,
            $scope.Rates.Consolidation.Variable.Tier4,
            $scope.Rates.Consolidation.Variable.Tier5,
            $scope.Rates.Resident.Fixed.Tier1,
            $scope.Rates.Resident.Fixed.Tier2,
            $scope.Rates.Resident.Fixed.Tier3,
            $scope.Rates.Resident.Fixed.Tier4,
            $scope.Rates.Resident.Fixed.Tier5
        );

        WebData.SetRates(rates).success(function () {
            $scope.saved.Rates = true;
            $scope.saving.Rates = false;
        });
    }
    $scope.submitDynamicValues = function () {
        $scope.saving.Settings = true;
        WebData.SetDynamicValues($scope.DynamicValues).success(function () {
            $scope.saved.Settings = true;
            $scope.saving.Settings = false;
        });
    }
    $scope.submitLoanRestrictions = function () {
        $scope.saving.LoanRes = true;
        WebData.SetLoanRestrictions($scope.Restrictions).success(function () {
            $scope.saved.LoanRes = true;
            $scope.saving.LoanRes = false;
        });
    }
    GetPrograms();
    function GetPrograms() {
        WebData.GetPrograms()
            .success(function (stubs) {
                $scope.programs = stubs;
            })
            .error(function (error) {
                $scope.status = 'Unable to load program data: ' + error.message;
            });
    }
    GetRestrictions();
    function GetRestrictions() {
        WebData.GetRestrictions()
            .success(function(stubs) {
                $scope.Restrictions = stubs;
            })
            .error(function(error) {
                $scope.status = 'Unable to load restrictions data: ' + error.message;
            });
    }
    GetDynamicValues();
    function GetDynamicValues() {
        WebData.GetDynamicValues()
            .success(function(stubs) {
                $scope.DynamicValues = stubs;
            })
            .error(function(error) {
                $scope.status = 'Unable to load dynamic values data: ' + error.message;
            });
    }
    GetRates();
    function GetRates() {
        WebData.GetRates()
            .success(function(stubs) {
                $scope.Rates = stubs;
            })
            .error(function(error) {
                $scope.status = 'Unable to load rates data: ' + error.message;
            });
    }
});

AdminApp.controller('LoanSubmissionsController', ['$scope', '$filter', 'NgTableParams', 'WebData', function($scope, $filter, NgTableParams, WebData) {

    $scope.submissions = [];
    $scope.filters = {}
    $scope.selectedUser = {};
    $scope.users = [];

    $scope.tableParams = new NgTableParams({
        page: 1,
        count: 10,
        sorting: { DateSubmitted: 'desc' },
        filter: $scope.filters
    }, {
        total: $scope.submissions.length,
        getData: function ($defer, params) {
            var filteredData = ($scope.selectedUser && $scope.selectedUser.Id) ?
                $filter('filter')($scope.submissions, {UserId: $scope.selectedUser.Id}) :
                $scope.submissions;
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            params.total(orderedData.length); // set total for recalc pagination
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.setTableSort = function (column) {
        var sort = {};
        sort[column] = $scope.tableParams.isSortBy(column, 'asc') ? 'desc' : 'asc';
        $scope.tableParams.sorting(sort);
    }

    $scope.$watch('selectedUser', function () {
        console.log($scope.selectedUser);
        $scope.tableParams.reload();
    });

    WebData.GetLoanSubmissions().success(function(data) {
        $scope.submissions = data;

        angular.forEach($scope.submissions, function(submission) {
            submission.DateSubmitted = new Date(parseInt(submission.DateSubmitted.substr(6)));
            $scope.users.push(angular.copy(submission.User));
        });

        $scope.tableParams.reload();
    });

}]);

