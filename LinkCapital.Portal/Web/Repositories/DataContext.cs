﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using Web.EntityModels;

namespace Web.Repositories
{
    public class DataContext : IdentityDbContext<ApplicationUser>
	{
		#region DBSets

        public DbSet<DynamicConfigValue> DynamicConfigValues { get; set; }
		public DbSet<State> States { get; set; }
		public DbSet<LoanProgram> LoanPrograms { get; set; }
        public DbSet<LoanRule> LoanRules { get; set; }
        public DbSet<RegistrationContext> RegistrationContexts { get; set; }
        public DbSet<PartnerDomain> PartnerDomains { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Rate> Rates { get; set; }
        //public DbSet<LinkCapitalLoan> LinkLoans { get; set; }
        public DbSet<LoanSubmission> LoanSubmissions { get; set; }

        #endregion

        #region Constructors

#if DEBUG
        //public DataContext() : this("name=" + Web.Utilities.Configuration.AdminConnectionStringName) { }
        public DataContext() : this("name=" + Web.Utilities.Configuration.UserConnectionStringName) { }
#elif !DEBUG
        public DataContext() : this("name=" + Web.Utilities.Configuration.ProdConnectionStringName) { }
#endif

        public DataContext(string nameOrConnectionString) : base(nameOrConnectionString, throwIfV1Schema: false) { }

        #endregion

        public override int SaveChanges()
        {
            // Warning not my code but a simplified interpretation of http://benjii.me/2014/03/track-created-and-modified-fields-automatically-with-entity-framework-code-first - Aaron V. 05/21/2015
            var changedRec = ChangeTracker.Entries().Where(x => x.Entity is ApplicationUser && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var currentEnt in changedRec)
            {
                if (currentEnt.State == EntityState.Added)
                {
                    ((ApplicationUser) currentEnt.Entity).LeadStatus = "Created Portal Account";
                }

                ((ApplicationUser)currentEnt.Entity).LastChanged = DateTime.Now;
            }

            return base.SaveChanges();
        }
    }
}
