﻿using System.Collections.Specialized;

namespace Web.Utilities.Extensions
{
	public static class NameValueCollectionExtension
	{
		/// <summary>
		/// Returns the value associated with the given key name from the given name-value collection,
		/// converted to the given type.  Returns the default value for the given type if the
		/// key name does not exist within the collection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="collection"></param>
		/// <param name="name"></param>
		/// <returns>The value associated with the given key name from the given name-value collection.</returns>
		public static T Get<T>(this NameValueCollection collection, string name)
		{
			return collection.Get<T>(name, default(T));
		}

		/// <summary>
		/// Returns the value associated with the given key name from the given name-value collection,
		/// converted to the given type.  Returns the given default value if the key name does not exist
		/// within the collection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="collection"></param>
		/// <param name="name"></param>
		/// <param name="defaultValue"></param>
		/// <returns>The value associated with the given key name from the given name-value collection.</returns>
		public static T Get<T>(this NameValueCollection collection, string name, T defaultValue)
		{
			T value = defaultValue;

			try
			{
				string stringValue = collection[name];
				if (!string.IsNullOrEmpty(stringValue))
					value = stringValue.ConvertTo<T>();
			}
			catch
			{
			}

			return value;
		}
	}
}
