﻿using System.Collections.Specialized;
using System.Configuration;
using Web.Utilities.Extensions;

namespace Web.Utilities
{
	public static class Configuration
	{
		public static NameValueCollection AppSettings
		{
			get { return ConfigurationManager.AppSettings; }
		}

		public static ConnectionStringSettingsCollection ConnectionStrings
		{
			get { return ConfigurationManager.ConnectionStrings; }
		}

		#region General

		public static string PortalInstance
		{
			get { return AppSettings.Get("PortalInstance", string.Empty); }
		}

		public static string DbEncryptionKey
		{
			get { return AppSettings.Get("DbEncryptionKey", string.Empty); }
		}

		#endregion

		#region Connection Strings

		public static string UserConnectionStringName = "LinkCapital.User";
		public static string AdminConnectionStringName = "LinkCapital.Admin";
        public static string ProdConnectionStringName = "LinkCapital.Prod";

		public static string UserConnectionString
		{
			get { return ConnectionStrings[UserConnectionStringName].ConnectionString; }
		}

		public static string AdminConnectionString
		{
			get { return ConnectionStrings[AdminConnectionStringName].ConnectionString; }
		}

        public static string ProdConnectionString
        {
            get { return ConnectionStrings[ProdConnectionStringName].ConnectionString; }
        }

		#endregion

		#region OAuth

        private const string OAuthEnabledKey = "OAuthEnabled";
        private const string OAuthTwitterClientIdKey = "OAuthTwitterClientId";
        private const string OAuthTwitterSecretKey = "OAuthTwitterSecret";
        private const string OAuthFacebookClientIdKey = "OAuthFacebookClientId";
        private const string OAuthFacebookSecretKey = "OAuthFacebookSecret";
        private const string OAuthGoogleClientIdKey = "OAuthGoogleClientId";
        private const string OAuthGoogleSecretKey = "OAuthGoogleSecret";

		public static bool OAuthEnabled
        {
            get { return AppSettings.Get(OAuthEnabledKey, false); }
        }

        public static string OAuthTwitterClientId
        {
            get { return AppSettings.Get(OAuthTwitterClientIdKey, ""); }
        }

        public static string OAuthTwitterSecret
        {
            get { return AppSettings.Get(OAuthTwitterSecretKey, ""); }
        }

        public static string OAuthFacebookClientId
        {
            get { return AppSettings.Get(OAuthFacebookClientIdKey, ""); }
        }

        public static string OAuthFacebookSecret
        {
            get { return AppSettings.Get(OAuthFacebookSecretKey, ""); }
        }

        public static string OAuthGoogleClientId
        {
            get { return AppSettings.Get(OAuthGoogleClientIdKey, ""); }
        }

        public static string OAuthGoogleSecret
        {
            get { return AppSettings.Get(OAuthGoogleSecretKey, ""); }
        }

		#endregion

        #region Smtp

        public static string SmtpHost
		{
			get { return AppSettings.Get("SmtpHost", string.Empty); }
		}

		public static int SmtpPort
		{
			get { return AppSettings.Get("SmtpPort", 0); }
		}

		public static bool SmtpSsl
		{
			get { return AppSettings.Get("SmtpSsl", false); }
		}

		public static string SmtpUser
		{
			get { return AppSettings.Get("SmtpUser", string.Empty); }
		}

		public static string SmtpPassword
		{
			get { return AppSettings.Get("SmtpPassword", string.Empty); }
		}

		public static string SmtpFromEmail
		{
			get { return AppSettings.Get("SmtpFromEmail", string.Empty); }
		}

        #endregion

		#region Certificates

		public static string LocalCertificate
		{
			get { return AppSettings.Get("LocalCertificate", string.Empty); }
		}

		public static string CampusDoorCertificateLocation
		{
			get { return AppSettings.Get("CampusDoorCertificateLocation", string.Empty); }
		}

		#endregion

        #region CampusDoor

		public static string CampusDoorNewLoanAppUrl
		{
			get { return AppSettings.Get("CampusDoorNewLoanAppUrl", string.Empty); }
		}

		public static string CampusDoorMyAccountUrl
		{
			get { return AppSettings.Get("CampusDoorMyAccountUrl", string.Empty); }
		}

        #endregion
	}
}
