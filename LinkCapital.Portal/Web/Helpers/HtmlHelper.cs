﻿using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Web.Helpers
{
    public static class HtmlHelper
    {
        public static MvcHtmlString MenuItem(this System.Web.Mvc.HtmlHelper htmlHelper, string linkText, string actionName, string controllerName)
        {
            var builder = new TagBuilder("li")
            {
                InnerHtml = htmlHelper.ActionLink(linkText, actionName, controllerName).ToHtmlString()
            };

            bool isCurrentPage = controllerName == htmlHelper.ViewContext.RouteData.GetRequiredString("controller") && actionName == htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            if (isCurrentPage)
            {
                builder.AddCssClass("active");
            }

            return new MvcHtmlString(isCurrentPage && actionName == "Register" && controllerName == "Account" ? "" : builder.ToString());
        }
    }
}