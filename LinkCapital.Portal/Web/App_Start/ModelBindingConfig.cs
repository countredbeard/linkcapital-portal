﻿using System.Web.Mvc;
using Web.Utilities;

namespace Web
{
    public class ModelBindingConfig
    {
        public static void RegisterModelBindings(ModelBinderDictionary binders)
        {
            binders.Add(typeof(decimal), new DecimalModelBinder());
        }
    }
}