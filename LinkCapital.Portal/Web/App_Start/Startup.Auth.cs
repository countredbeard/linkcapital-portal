﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Web.Services;

namespace Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: Web.Utilities.Configuration.OAuthMicrosoftClientId,
            //    clientSecret: Web.Utilities.Configuration.OAuthMicrosoftSecret);

            app.UseTwitterAuthentication(
               consumerKey: Web.Utilities.Configuration.OAuthTwitterClientId,
               consumerSecret: Web.Utilities.Configuration.OAuthTwitterSecret);

            app.UseFacebookAuthentication(
               appId: Web.Utilities.Configuration.OAuthFacebookClientId,
               appSecret: Web.Utilities.Configuration.OAuthFacebookSecret);

            app.UseGoogleAuthentication(
                clientId: Web.Utilities.Configuration.OAuthGoogleClientId,
                clientSecret: Web.Utilities.Configuration.OAuthGoogleSecret);

            //app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
        }
    }
}