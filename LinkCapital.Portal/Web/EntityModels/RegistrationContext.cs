﻿


using System.ComponentModel.DataAnnotations.Schema;

namespace Web.EntityModels
{
    public class RegistrationContext
    {
        public int Id { get; set; }
        public string ReturnUrl { get; set; }
        public string Context { get; set; }
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}