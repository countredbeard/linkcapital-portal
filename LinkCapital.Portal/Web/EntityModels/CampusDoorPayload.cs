﻿using System;

namespace Web.EntityModels
{
    public class CampusDoorPayload
    {
        public Guid RegId { get; set; }
        public DateTime Utc { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Context { get; set; }
    }
}