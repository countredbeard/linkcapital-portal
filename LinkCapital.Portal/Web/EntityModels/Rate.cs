﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.EntityModels
{
    public enum RateTypes
    {
        Fixed, Variable
    }

    public enum Tiers
    {
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5
    }

    public class Rate
    {
        public int ID { get; set; }
        public RateTypes RateType { get; set; }
        public bool Resident { get; set; }
        public int Term { get; set; }
        public double InterestRate { get; set; }
        public Tiers Tier { get; set; }
        public int FicoMax { get; set; }
        public int FicoMin { get; set; }
    }
}