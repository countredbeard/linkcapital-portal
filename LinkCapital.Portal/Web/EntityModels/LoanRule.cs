﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.EntityModels
{
	public class LoanRule
	{
		public int Id { get; set; }
		public int LoanProgramId { get; set; }
		public int StateId { get; set; }
		public decimal LoanAmountThreshold { get; set; }
        public LoanQualificationRestriction LoanQualificationRestriction { get; set; }
        public bool Active { get; set; }

		public virtual LoanProgram LoanProgram { get; set; }
		public virtual State State { get; set; }
	}
}