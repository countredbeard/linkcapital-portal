﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Helpers;
using Web.Utilities;

namespace Web.EntityModels
{
    public class ApplicationUser : IdentityUser
    {
        private string _firstName;
        private string _lastName;

        public ApplicationUser()
        {
            CampusDoorRegId = Guid.NewGuid();
            Salt = Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// User First Name
        /// </summary>
        [NotMapped]
        public string FirstName
        {
            get
            {
                if (string.IsNullOrEmpty(_firstName))
                {
                    _firstName = EncryptionHelper.SimpleDecryptWithPassword(SecureFirstName, Salt + Configuration.DbEncryptionKey);
                }
                return _firstName;
            }
            set
            {
                _firstName = value;
                SecureFirstName = EncryptionHelper.SimpleEncryptWithPassword(value, Salt + Configuration.DbEncryptionKey);
            }
        }
        
        /// <summary>
        /// Encrypted First Name
        /// </summary>
        [Column("FirstName")]
        public string SecureFirstName { get; set; }
        
        /// <summary>
        /// User Last Name
        /// </summary>
        [NotMapped]
        public string LastName
        {
            get
            {
                if (string.IsNullOrEmpty(_lastName))
                {
                    _lastName = EncryptionHelper.SimpleDecryptWithPassword(SecureLastName, Salt + Configuration.DbEncryptionKey);
                }
                return _lastName;
            }
            set
            {
                _lastName = value;
                SecureLastName = EncryptionHelper.SimpleEncryptWithPassword(value, Salt + Configuration.DbEncryptionKey);
            }
        }

        /// <summary>
        /// Encrypted Last Name
        /// </summary>
        [Column("LastName")]
        public string SecureLastName { get; set; }
        
        /// <summary>
        /// Campus Door Registration Id
        /// </summary>
        public Guid CampusDoorRegId { get; set; }
        
        /// <summary>
        /// Unique Salt used during encryption for this user
        /// </summary>
        [Required]
        public string Salt { get; set; }

        /// <summary>
        /// Email for user affiliated organization
        /// </summary>
        public string QualifyingEmail { get; set; }

        /// <summary>
        /// Set to true after the user has verified there qualifying email
        /// </summary>
        public bool QualifyingEmailConfirmed { get; set; }

        public string UserSFDCID { get; set; }

        public string LeadStatus { get; set; }

        public DateTime LastSyncDate { get; set; }

        /// <summary>
        /// The last time this record has been changed 
        /// </summary>
        public DateTime LastChanged { get; set; }

        public bool IsCosigner { get; set; }

        public bool IsBorrower { get { return !IsCosigner; } }

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        //public virtual System.Collections.Generic.ICollection<LinkCapitalLoan> LinkLoans { get; set; }
    }
}