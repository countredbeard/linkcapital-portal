﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.EntityModels
{
	public class LoanSubmission
	{
		public int Id { get; set; }
        public string UserId { get; set; }
        public string State { get; set; }
		public decimal Amount { get; set; }
        public bool Resident { get; set; }
        public string LoanProgram { get; set; }
        public string LoanUrl { get; set; }
        public DateTime DateSubmitted { get; set; }

        public virtual ApplicationUser User { get; set; }
	}
}