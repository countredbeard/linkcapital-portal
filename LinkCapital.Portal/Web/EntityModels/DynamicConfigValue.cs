﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.EntityModels
{
    /// <summary>
    /// An object used to store a dynamic config for the site
    /// </summary>
    public class DynamicConfigValue
    {
        /// <summary>
        /// SQL needs an ID so here it is
        /// </summary>
        public Guid ID { get; set; }
        /// <summary>
        /// Name of the field
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Description used to display to the user
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The value to be retrieved and stored to
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// A default value that can be used if the Value given is not correct or unusable
        /// </summary>
        public string DefaultValue { get; set; }
    }
}