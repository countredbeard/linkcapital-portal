﻿using System.Collections.Generic;
using System.Linq;
using Web.EntityModels;
using Web.Repositories;
using Web.Services.Interfaces;

namespace Web.Services
{
	public class StateService : IStateService
	{
		/// <summary>
		/// Returns a collection of all states.
		/// </summary>
		/// <returns>A collection of all states.</returns>
		public ICollection<State> GetStates()
		{
			var states = new List<State>();

			using (var context = new DataContext())
			{
				states = context.States.ToList();
			}

			return states;
		}

		/// <summary>
		/// Returns the state with the given name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns>The state with the given name.</returns>
		public State GetState(string name)
		{
			State state = null;

			using (var context = new DataContext())
			{
				state = context.States.FirstOrDefault(s => s.Name == name);
			}

			return state;
		}
	}
}