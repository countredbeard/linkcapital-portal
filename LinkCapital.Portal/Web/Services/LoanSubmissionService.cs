﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.EntityModels;
using Web.Repositories;
using Web.Services.Interfaces;

namespace Web.Services
{
    public class LoanSubmissionService : ILoanSubmissionService
    {
        public LoanSubmission SaveLoanSubmission(string userId, string state, decimal amount, bool resident, string program, string url)
        {
            using (var context = new DataContext())
            {
                var submission = context.LoanSubmissions.Create();
                submission.UserId = userId;
                submission.State = state;
                submission.Amount = amount;
                submission.Resident = resident;
                submission.LoanProgram = program;
                submission.LoanUrl = url;
                submission.DateSubmitted = DateTime.Now;

                context.LoanSubmissions.Add(submission);
                context.SaveChanges();

                return submission;
            }
        }

        public IList<LoanSubmission> GetAllSubmissions()
        {
            using (var context = new DataContext())
            {
                return context.LoanSubmissions.ToList();
            }
        }

        public IList<LoanSubmission> GetAllSubmissionsForUser(string id)
        {
            using (var context = new DataContext())
            {
                return context.LoanSubmissions.Where(x => x.UserId == id).ToList();
            }
        }
    }
}