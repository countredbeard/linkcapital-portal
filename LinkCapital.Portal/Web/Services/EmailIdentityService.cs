﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using Web.Utilities;

namespace Web.Services
{
    public class EmailIdentityService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            var email = new MailMessage(Configuration.SmtpFromEmail, message.Destination, message.Subject, message.Body);
            email.IsBodyHtml = true;

            var smtpClient = new SmtpClient(Configuration.SmtpHost, Configuration.SmtpPort)
            {
                Credentials = new NetworkCredential(Configuration.SmtpUser, Configuration.SmtpPassword),
                EnableSsl = Configuration.SmtpSsl
            };

            return smtpClient.SendMailAsync(email);
        }
    }
}