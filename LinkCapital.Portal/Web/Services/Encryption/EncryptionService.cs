﻿using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

namespace Web.Services.Encryption
{
    public class EncryptionService
    {
        public byte[] EncryptAndSign(byte[] data, X509Certificate2 encryptionCert, X509Certificate2 signingCert)
        {
            var encryptedData = EncryptData(data, encryptionCert);

            return SignData(encryptedData, signingCert);
        }

        private byte[] EncryptData(byte[] data, X509Certificate2 encryptionCert)
        {
            ContentInfo dataToEncrypt = new ContentInfo(data);
            EnvelopedCms evCms = new EnvelopedCms(dataToEncrypt);
            CmsRecipient recipient = new CmsRecipient(encryptionCert);
            evCms.Encrypt(recipient);
            byte[] encryptedData = evCms.Encode();
            return encryptedData;
        }

        private byte[] SignData(byte[] encryptedData, X509Certificate2 signingCert)
        {
            ContentInfo dataToSign = new ContentInfo(encryptedData);
            CmsSigner signer = new CmsSigner(signingCert);
            SignedCms signedCms = new SignedCms(dataToSign);
            signedCms.ComputeSignature(signer);
            return signedCms.Encode();
        }
    }
}
