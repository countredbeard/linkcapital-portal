﻿using System.Collections.Generic;
using System.Linq;
using Web.EntityModels;
using Web.Repositories;
using Web.Services.Interfaces;

namespace Web.Services
{
	public class LoanProgramService : ILoanProgramService
	{
		/// <summary>
		/// Returns a collection of all loan programs.
		/// </summary>
		/// <returns>A collection of all loan programs.</returns>
		public ICollection<LoanProgram> GetLoanPrograms()
		{
			var loanPrograms = new List<LoanProgram>();

			using (var context = new DataContext())
			{
				loanPrograms = context.LoanPrograms.ToList();
			}

			return loanPrograms;
		}

		/// <summary>
		/// Returns the loan program that matches the given criteria.
		/// </summary>
		/// <param name="stateName"></param>
		/// <param name="loanAmount"></param>
		/// <param name="loanRestriction"></param>
		/// <returns>The loan program that matches the given criteria.</returns>
		public LoanProgram GetLoanProgram(string stateName, decimal loanAmount, LoanQualificationRestriction loanRestriction)
		{
			LoanProgram loanProgram = null;

			using (var context = new DataContext())
			{
                // if the LoanRule is not active than null is passed back
				loanProgram = (from lr in context.LoanRules
								join s in context.States on lr.StateId equals s.Id
								join lp in context.LoanPrograms on lr.LoanProgramId equals lp.Id
								where s.Name == stateName && lr.LoanAmountThreshold < loanAmount && lr.LoanQualificationRestriction == loanRestriction && lr.Active == true
								orderby lr.LoanAmountThreshold descending
								select lp).FirstOrDefault();
			}

            
			return loanProgram;
		}
	}
}