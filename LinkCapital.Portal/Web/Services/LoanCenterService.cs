﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using Web.EntityModels;
using Web.Services.Encryption;
using Web.Services.Interfaces;
using Web.Utilities;

namespace Web.Services
{
    public class LoanCenterService : ILoanCenterService
    {
        private readonly EncryptionService _encryptionService = new EncryptionService();

        public string GetCampusDoorToken(ApplicationUser user)
        {
            return GetCampusDoorToken(user, "");
        }

        public string GetCampusDoorToken(ApplicationUser user, string context)
        {
            CampusDoorPayload payload = new CampusDoorPayload()
            {
                Context = context,
                EmailAddress = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                RegId = user.CampusDoorRegId,
                Utc = DateTime.UtcNow
            };

            var campusDoorCert = GetCampusDoorCert();

            var linkCert = GetLocalCert();

            string unencryptedPayload = JsonConvert.SerializeObject(payload, new JsonConverter[] {new IsoDateTimeConverter(), new GuidConverter(), });
            byte[] encryptedPayload = _encryptionService.EncryptAndSign(Encoding.UTF8.GetBytes(unencryptedPayload), campusDoorCert, linkCert);

            return HttpServerUtility.UrlTokenEncode(encryptedPayload);
        }

        private X509Certificate2 GetLocalCert()
        {
            #if DEBUG
            X509Store localStore = new X509Store(StoreLocation.LocalMachine);
            localStore.Open(OpenFlags.ReadOnly);
            X509Certificate2 linkCert =
                localStore.Certificates.Cast<X509Certificate2>()
                    .FirstOrDefault(cert => cert.FriendlyName.Equals(Configuration.LocalCertificate));
            return linkCert;
            #endif

              X509Store certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
              certStore.Open(OpenFlags.ReadOnly);
              X509Certificate2Collection certCollection = certStore.Certificates.Find(
                                         X509FindType.FindByThumbprint,
                                         // Replace below with your cert's thumbprint
                                         "C44BF7DEDD43F8872F66C6DB627DB02DBE9C8AF7",
                                         false);
              // Get the first cert with the thumbprint
              if (certCollection.Count > 0)
              {
                X509Certificate2 cert = certCollection[0];
                // Use certificate
                return cert;
              }
              certStore.Close();
              return null;
        }

        private X509Certificate2 GetCampusDoorCert()
        {
			HttpWebRequest request = WebRequest.CreateHttp(Configuration.CampusDoorCertificateLocation);
            request.Method = WebRequestMethods.Http.Head;
            WebResponse response = request.GetResponse();
            X509Certificate2 campusDoorCert = new X509Certificate2(request.ServicePoint.Certificate);
            return campusDoorCert;
        }
    }
}