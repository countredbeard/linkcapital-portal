﻿using System.Linq;
using Web.EntityModels;
using Web.Repositories;
using Web.Services.Interfaces;

namespace Web.Services
{
    public class RegistrationContextService : IRegistrationContextService
    {
        public void CreateRegistrationContext(string userId, string returnUrl, string context)
        {
            RegistrationContext regContext = new RegistrationContext()
            {
                UserId = userId,
                Context = context,
                ReturnUrl = returnUrl
            };

            using (var dbContext = new DataContext())
            {
                dbContext.RegistrationContexts.Add(regContext);
                dbContext.SaveChanges();
            }
        }

        public RegistrationContext GetRegistrationContext(string userId)
        {
            RegistrationContext regContext;
            using (var dbContext = new DataContext())
            {
                regContext = dbContext.RegistrationContexts.FirstOrDefault(c => c.UserId == userId);
            }

            return regContext;
        }

        public void DeleteRegistrationContext(string userId)
        {
            using (var dbContext = new DataContext())
            {
                RegistrationContext regContext = dbContext.RegistrationContexts.FirstOrDefault(c => c.UserId == userId);
                if (regContext != null)
                {
                    dbContext.RegistrationContexts.Remove(regContext);
                    dbContext.SaveChanges();
                }
            }
        }
    }
}