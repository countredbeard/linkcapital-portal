﻿using Web.EntityModels;

namespace Web.Services.Interfaces
{
	public interface ILoanCenterService
	{
		string GetCampusDoorToken(ApplicationUser user);
		string GetCampusDoorToken(ApplicationUser user, string context);
	}
}
