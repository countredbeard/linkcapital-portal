﻿using Web.EntityModels;

namespace Web.Services.Interfaces
{
    public interface IRegistrationContextService
    {
        void CreateRegistrationContext(string userId, string returnUrl, string context);
        RegistrationContext GetRegistrationContext(string userId);
        void DeleteRegistrationContext(string userId);
    }
}
