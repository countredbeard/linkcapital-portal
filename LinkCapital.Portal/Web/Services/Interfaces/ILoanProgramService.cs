﻿using System.Collections.Generic;
using Web.EntityModels;

namespace Web.Services.Interfaces
{
	public interface ILoanProgramService
	{
		ICollection<LoanProgram> GetLoanPrograms();
        LoanProgram GetLoanProgram(string stateName, decimal loanAmount, LoanQualificationRestriction loanRestriction);
	}
}
