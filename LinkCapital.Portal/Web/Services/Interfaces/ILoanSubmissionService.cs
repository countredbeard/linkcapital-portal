﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.EntityModels;

namespace Web.Services.Interfaces
{
    public interface ILoanSubmissionService
    {
        LoanSubmission SaveLoanSubmission(string userId, string state, decimal amount, bool resident, string program, string url);
        IList<LoanSubmission> GetAllSubmissions();
        IList<LoanSubmission> GetAllSubmissionsForUser(string id);
    }
}
