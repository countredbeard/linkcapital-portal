﻿using System.Collections.Generic;
using Web.EntityModels;

namespace Web.Services.Interfaces
{
	public interface IStateService
	{
		ICollection<State> GetStates();
		State GetState(string name);
	}
}
