﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using System.Web.Security;
using ApplicationUser = Web.EntityModels.ApplicationUser;
using System.Threading.Tasks;
using System;
using System.Globalization;

namespace Web.Services
{
    /// <summary>UserManager which Handles the Application Users</summary>
    public class UserManager : Microsoft.AspNet.Identity.UserManager<ApplicationUser>
    {
        /// <summary>Constructor</summary>
        /// <param name="userStore">User Store</param>
        public UserManager(IUserStore<ApplicationUser> userStore)
            : base(userStore)
        {
            this.EmailService = new EmailIdentityService();
            var provider = new MachineKeyProtectionProvider();
            this.UserTokenProvider = new Microsoft.AspNet.Identity.Owin.DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"));
        }

        /// <summary>Custom Data Protection Provider than uses the Machine Key</summary>
        public class MachineKeyProtectionProvider : IDataProtectionProvider
        {
            /// <summary>Create Factory</summary>
            /// <param name="purposes">Purposes</param>
            /// <returns>Data Protector</returns>
            public IDataProtector Create(params string[] purposes)
            {
                return new MachineKeyDataProtector(purposes);
            }
        }

        /// <summary>Custom Data Protector that uses the Machine Key</summary>
        public class MachineKeyDataProtector : IDataProtector
        {
            private readonly string[] _purposes;

            /// <summary>Constructor</summary>
            /// <param name="purposes">Purposes</param>
            public MachineKeyDataProtector(string[] purposes)
            {
                _purposes = purposes;
            }

            /// <summary>Protect some User Data</summary>
            /// <param name="userData">User Data to Protect</param>
            /// <returns>Protected User Data</returns>
            public byte[] Protect(byte[] userData)
            {
                return MachineKey.Protect(userData, _purposes);
            }

            /// <summary>Unprotect some User Data</summary>
            /// <param name="protectedData">Protected User Data</param>
            /// <returns>Unprotected User Data</returns>
            public byte[] Unprotect(byte[] protectedData)
            {
                return MachineKey.Unprotect(protectedData, _purposes);
            }
        }

        public async Task<string> GenerateQualifyingToken (string id)
        {
            string result = await this.GenerateUserTokenAsync("QualifyingToken", id);
            return result;
        }

        public virtual async Task<bool> ConfirmQualifyingEmailAsync(string userId, string token)
        {
            //ThrowIfDisposed();
            //var store = GetEmailStore();
            var user = await FindByIdAsync(userId);
            if (user == null)
            {
                // throw exception
            }

            if (!await VerifyUserTokenAsync(userId, "QualifyingToken", token))
            {
                return false;
            }

                

            return true;
        }

        public virtual async Task SendQulEmailAsync(string userId, string subject, string body)
        {
            //ThrowIfDisposed();
            if (EmailService != null)
            {
                var msg = new IdentityMessage
                {
                    Destination = await GetQulEmailAsync(userId),
                    Subject = subject,
                    Body = body,
                };
                await EmailService.SendAsync(msg);
            }
        }

        public virtual async Task<string> GetQulEmailAsync(string userId)
        {
            //ThrowIfDisposed();
            //var store = GetEmailStore();
            var user = await FindByIdAsync(userId);
            if (user == null)
            {
                throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture, "User info not found",
                    userId));
            }
            return user.QualifyingEmail;
        }
    }
}