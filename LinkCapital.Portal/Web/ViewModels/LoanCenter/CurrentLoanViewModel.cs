﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.LoanCenter
{
    public class CurrentLoanViewModel
    {
        [Display(Name = "Loan Number")]
        public long LoanId { get; set; }
        [Display(Name = "Loan Disbursement Date")]
        public DateTime? DisbursementDate { get; set; }
        [Display(Name = "Loan Type")]
        public string LoanType { get; set; }
        [Display(Name = "Loan Status")]
        public string LoanStatus { get; set; }
         
        [Display(Name = "Original Balance")]
        public decimal OriginalBalance { get; set; }
        [Display(Name = "Late Fees")]
        public decimal LateFees { get; set; }
        [Display(Name = "Current Balance")]
        public decimal CurrentBalance { get; set; }

        [Display(Name = "Monthly Payment")]
        public decimal MonthlyPayment { get; set; }
        
        [Display(Name = "Next Due Date")]
        public DateTime? NextDueDate { get; set; }
        [Display(Name = "Days Delinquent")]
        public long DaysDelinquent { get; set; }
        [Display(Name = "Amount Past Due")]
        public decimal PastDueAmount { get; set; }

        [Display(Name = "Interest Rate")]
        public decimal InterestRate { get; set; }
        
        [Display(Name = "Accurate As Of")]
        public DateTime? DateCalculated { get; set; }
    }
}