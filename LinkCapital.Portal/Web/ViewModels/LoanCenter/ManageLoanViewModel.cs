﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModels.LoanCenter
{
    public class ManageLoanViewModel
    {
        public string HttpMethod { get; set; }
        public string Url { get; set; }
        public string Payload { get; set; }
    }
}