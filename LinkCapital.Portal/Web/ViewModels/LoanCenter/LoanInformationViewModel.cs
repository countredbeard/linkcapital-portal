﻿
using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.LoanCenter
{
    public class LoanInformationViewModel
    {
        [Required]
        [Display(Name = "State of Permanent Residence")]
        public string StateOfResidence { get; set; }
        [Required]
        [Display(Name = "Loan Balance")]
        public decimal LoanAmount { get; set; }
        [Required(ErrorMessage = "You must answer the question.")]
        [Display(Name="Residency Status")]
        public bool? IsMedicalResident { get; set; }
    }
}