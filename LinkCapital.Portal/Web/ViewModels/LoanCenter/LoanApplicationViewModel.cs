﻿
namespace Web.ViewModels.LoanCenter
{
    public class LoanApplicationViewModel
    {
        public string HttpMethod { get; set; }
        public string Url { get; set; }
        public string Payload { get; set; }
    }
}