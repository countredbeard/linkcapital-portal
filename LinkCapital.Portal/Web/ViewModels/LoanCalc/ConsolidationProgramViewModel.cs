﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.EntityModels.LoanCalculator;

namespace Web.ViewModels.LoanCalc
{
    public class ConsolidationProgramViewModel
    {
        public ConsolidationProgramViewModel()
        {
            FixedRates = new List<ConsolidationProgramItem>();
            VariableRates = new List<ConsolidationProgramItem>();
        }

        public double CurrentMonthlyPayment { get; set; }

        public double LifetimeCost { get; set; }

        public double AverageInterestRate { get; set; }

        public double TotalBalance { get; set; }

        public List<ConsolidationProgramItem> FixedRates { get; set; }

        public List<ConsolidationProgramItem> VariableRates { get; set; }
    }
}