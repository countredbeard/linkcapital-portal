﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.EntityModels.LoanCalculator;

namespace Web.ViewModels.LoanCalc
{
    public class ResidentProgramViewModel
    {
        public ResidentProgramViewModel()
        {
            FixedRates = new List<ResidentProgramItem>();
        }

        public double CurrentMonthlyPayment { get; set; }

        public double LifetimeCost { get; set; }

        public double AverageInterestRate { get; set; }

        public double TotalBalance { get; set; }

        public List<ResidentProgramItem> FixedRates { get; set; }
    }
}
