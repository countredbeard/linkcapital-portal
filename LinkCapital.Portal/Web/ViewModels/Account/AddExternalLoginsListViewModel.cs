﻿using System.Collections.Generic;
using Microsoft.Owin.Security;

namespace Web.ViewModels.Account
{
    public class AddExternalLoginsListViewModel
    {
        public string Action { get; set; }
        public string ReturnUrl { get; set; }

        public IEnumerable<AuthenticationDescription> FilteredProviders { get; set; }
    }
}