﻿using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels.Account
{
    /// <summary>Model for Login View</summary>
    public class LoginViewModel
    {
        [Required(ErrorMessage = "The Email Address field is required.")]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The Password field is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>Url to Return to After Login</summary>
        public string ReturnUrl { get; set; }

        /// <summary>Should Login show the Registration Information</summary>
        public bool? ShowRegistration { get; set; }

        /// <summary>UserType from Campus Door</summary>
        public int? UserType { get; set; }

        /// <summary>Context from Campus Door</summary>
        public string Context { get; set; }

        /// <summary>Flag indicating if we are reauthenticating from Campus Door</summary>
        public bool IsReAuth { get; set; }
    }
}