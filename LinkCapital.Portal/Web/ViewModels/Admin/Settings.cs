﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.EntityModels;

namespace Web.ViewModels.Admin
{
    public class Settings
    {
        List<LoanProgram> Programs { get; set; }
        List<LoanRule> LoanRules { get; set; }
    }
}