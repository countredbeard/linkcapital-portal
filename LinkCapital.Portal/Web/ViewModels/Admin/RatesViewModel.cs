﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.EntityModels;

namespace Web.ViewModels.Admin
{
    public class RatesViewModel
    {
        public List<Rate> Rates { get; set; }

        public List<string> ConsolidationTerms
        {
            get
            {
                var terms = new List<string>();

                if (Rates == null || Rates.Count <= 0) return terms;

                foreach (var rate in Rates.Where(x => !x.Resident).OrderBy(x => x.Term))
                {
                    if (!terms.Contains(rate.Term.ToString()))
                        terms.Add(rate.Term.ToString());
                }

                return terms;
            }
        }

        public List<string> ResidentTerms
        {
            get
            {
                var terms = new List<string>();

                if (Rates == null || Rates.Count <= 0) return terms;

                foreach (var rate in Rates.Where(x => x.Resident).OrderBy(x => x.Term))
                {
                    if (!terms.Contains(rate.Term.ToString()))
                        terms.Add(rate.Term.ToString());
                }

                return terms;
            }
        }

        public Dictionary<string, Dictionary<string, List<Rate>>> Consolidation
        {
            get
            {
                var dict = new Dictionary<string, Dictionary<string, List<Rate>>>();

                if (Rates == null || Rates.Count <= 0) return dict;

                var f = new Dictionary<string, List<Rate>>
                {
                    {"Tier1", Rates.Where(x => x.RateType == RateTypes.Fixed && !x.Resident && x.Tier == Tiers.One).OrderBy(x => x.Term).ToList()},
                    {"Tier2", Rates.Where(x => x.RateType == RateTypes.Fixed && !x.Resident && x.Tier == Tiers.Two).OrderBy(x => x.Term).ToList()},
                    {"Tier3", Rates.Where(x => x.RateType == RateTypes.Fixed && !x.Resident && x.Tier == Tiers.Three).OrderBy(x => x.Term).ToList()},
                    {"Tier4", Rates.Where(x => x.RateType == RateTypes.Fixed && !x.Resident && x.Tier == Tiers.Four).OrderBy(x => x.Term).ToList()},
                    {"Tier5", Rates.Where(x => x.RateType == RateTypes.Fixed && !x.Resident && x.Tier == Tiers.Five).OrderBy(x => x.Term).ToList()},
                };

                var v = new Dictionary<string, List<Rate>>
                {
                    {"Tier1", Rates.Where(x => x.RateType == RateTypes.Variable && !x.Resident && x.Tier == Tiers.One).OrderBy(x => x.Term).ToList()},
                    {"Tier2", Rates.Where(x => x.RateType == RateTypes.Variable && !x.Resident && x.Tier == Tiers.Two).OrderBy(x => x.Term).ToList()},
                    {"Tier3", Rates.Where(x => x.RateType == RateTypes.Variable && !x.Resident && x.Tier == Tiers.Three).OrderBy(x => x.Term).ToList()},
                    {"Tier4", Rates.Where(x => x.RateType == RateTypes.Variable && !x.Resident && x.Tier == Tiers.Four).OrderBy(x => x.Term).ToList()},
                    {"Tier5", Rates.Where(x => x.RateType == RateTypes.Variable && !x.Resident && x.Tier == Tiers.Five).OrderBy(x => x.Term).ToList()},
                };

                dict.Add("Fixed", f);
                dict.Add("Variable", v);

                return dict;
            }
        }

        public Dictionary<string, Dictionary<string, List<Rate>>> Resident
        {
            get
            {
                var dict = new Dictionary<string, Dictionary<string, List<Rate>>>();

                if (Rates == null || Rates.Count <= 0) return dict;

                var f = new Dictionary<string, List<Rate>>
                {
                    {"Tier1", Rates.Where(x => x.RateType == RateTypes.Fixed && x.Resident && x.Tier == Tiers.One).ToList()},
                    {"Tier2", Rates.Where(x => x.RateType == RateTypes.Fixed && x.Resident && x.Tier == Tiers.Two).ToList()},
                    {"Tier3", Rates.Where(x => x.RateType == RateTypes.Fixed && x.Resident && x.Tier == Tiers.Three).ToList()},
                    {"Tier4", Rates.Where(x => x.RateType == RateTypes.Fixed && x.Resident && x.Tier == Tiers.Four).ToList()},
                    {"Tier5", Rates.Where(x => x.RateType == RateTypes.Fixed && x.Resident && x.Tier == Tiers.Five).ToList()},
                };

                dict.Add("Fixed", f);

                return dict;
            }
        }
    }
}
