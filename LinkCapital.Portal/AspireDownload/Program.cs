﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace AspireDownload
{
    class Program
    {
        static void Main(string[] args)
        {
            string address = CloudConfigurationManager.GetSetting("ASDataAddress");
            int port = Convert.ToInt32(CloudConfigurationManager.GetSetting("ASDataPort"));
            string userName = CloudConfigurationManager.GetSetting("ASDataUserName");
            string password = CloudConfigurationManager.GetSetting("ASDataPassword");

            using (SftpClient client = new SftpClient(address, port, userName, password))
            {
                client.Connect();

                if (!client.Exists("Aspire"))
                {
                    client.CreateDirectory("Aspire");
                }

                foreach (SftpFile currentFile in client.ListDirectory("Aspire"))
                {
                    if (currentFile.Name != "." && currentFile.Name != "..")
                    {
                        StringBuilder fileName = new StringBuilder();
                        DateTime tempTime = DateTime.Now;
                        fileName.Append("AS");
                        fileName.Append(tempTime.Month);
                        fileName.Append(tempTime.Day);
                        fileName.Append(tempTime.Year);
                        fileName.Append(tempTime.Hour);
                        fileName.Append(tempTime.Minute);
                        fileName.Append(tempTime.Second);
                        fileName.Append(".csv");

                        using (FileStream fileStream = new FileStream(fileName.ToString(), FileMode.Create))
                        {
                            client.DownloadFile(currentFile.FullName, fileStream);
                            fileStream.Flush();
                        }

                        //using (StreamWriter logFile = new StreamWriter("CDDownlod.log"))
                        //{
                        //    logFile.WriteLine(tempTime.ToString() + ", " + currentFile.Name + ", " + fileName.ToString());
                        //}
                    }

                }
                

                client.Disconnect();
            }
        }
    }
}
