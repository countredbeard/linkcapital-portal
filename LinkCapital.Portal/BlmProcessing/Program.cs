﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.DataModels;
using Core.EntityModels;
using Core.Exceptions;
using Core.Salesforce;
using Microsoft.WindowsAzure;
using Core.Utilities;
using System.Net.Mail;
using System.Net;
using AzureUtils;

namespace BlmProcessing
{
    class Program
    {
        private static List<BlmRecord> UnmatchedBorrowers;
        private static List<BlmRecord> DeletedBorrowers;

        static int Main(string[] args)
        {
            string downloadsDirectory;
            UnmatchedBorrowers = new List<BlmRecord>();
            DeletedBorrowers = new List<BlmRecord>();

            if (AzureEnvironment.IsAzure)
            {
                var home = Environment.GetEnvironmentVariable("HOME");
                downloadsDirectory = string.Format(@"{0}\data\downloads\", home);
            }
            else
            {
                downloadsDirectory = Directory.GetCurrentDirectory();
            }

            if (!Directory.Exists(downloadsDirectory))
            {
                Console.Error.WriteLine("The directory {0} doesn't exist!", downloadsDirectory);
            }

            DirectoryInfo di = new DirectoryInfo(downloadsDirectory);
            FileSystemInfo[] files = di.GetFileSystemInfos();
            var sortedFiles =
                files.Where(
                    x => x.Extension.Contains("csv") && x.Name.StartsWith("BLM") && !x.Name.Contains("Archive"))
                    .OrderBy(f => f.Name);

            try
            {
                foreach (var file in sortedFiles)
                {
                    Console.WriteLine("Processing file {0}", file.Name);

                    ProcessFile(file.FullName).Wait();
                }

            }
            catch (AggregateException ex)
            {
                try
                {
                    throw ex.InnerException;
                }
                catch (SuspendOperationException inner)
                {
                    Console.Error.WriteLine(inner.Message);

                    if (UnmatchedBorrowers.Count() > 0)
                    {
                        Console.Error.WriteLine("Unmatched borrowers: {0}", UnmatchedBorrowers.Count());

                        foreach (BlmRecord record in UnmatchedBorrowers)
                        {
                            Console.Error.WriteLine("  {0} {1} {2}", record.BorrowerFirstName, record.BorrowerLastName, record.BorrowerEmailAddress);
                        }
                    }

                    if (DeletedBorrowers.Count() > 0)
                    {
                        Console.Error.WriteLine("Deleted borrowers: {0}", DeletedBorrowers.Count());

                        foreach (BlmRecord record in DeletedBorrowers)
                        {
                            Console.Error.WriteLine("  {0} {1} {2} {3}", record.BorrowerFirstName, record.BorrowerLastName, record.BorrowerEmailAddress, record.AccountId);
                        }
                    }

                    throw;
                }
            }

            return 0;
        }

        private static async Task<string> ProcessFile(string file)
        {
            var utility = new SfdcUtility
            {
                SFClientID = CloudConfigurationManager.GetSetting("SFClientID"),
                SFClientSecret = CloudConfigurationManager.GetSetting("SFClientSecret"),
                SFUserName = CloudConfigurationManager.GetSetting("SFUserName"),
                SFPassword = CloudConfigurationManager.GetSetting("SFPassword")
            };

            var dataUtility = new DataUtility(Environment.GetEnvironmentVariable("SQLAZURECONNSTR_LinkCapital.Portal") ??
                                              CloudConfigurationManager.GetSetting("DBConnection"));

            ApplicationUser.DbEncryptionKey = CloudConfigurationManager.GetSetting("DbEncryptionKey") ?? "BSrhVfHM-PCtSJS06lTxhBP-T$N90MAGA97VLmbB0ILjDXM6JflcdkGaaCoJ-uUW_0p1OnLWdnkw9xbn71anMwTzTOG$_uvNWnzR";
            bool suspendOperation = false;

            var records = LoadData(file);

            foreach (var record in records)
            {
                // invalid record
                if (string.IsNullOrWhiteSpace(record.Lender)) continue;
                
                var foundUser = dataUtility.FindByEmail(record.BorrowerEmailAddress);

                try
                {
                    if (foundUser == null || string.IsNullOrWhiteSpace(foundUser.UserSFDCID))
                    {
                        Console.Error.WriteLine("A user with the email address {0} was not found in the database", record.BorrowerEmailAddress);

                        throw new PersonAccountNotFoundException();
                    }
                    else
                    {
                        record.AccountId = foundUser.UserSFDCID;
                    }

                    await utility.StoreCDInSfdc(record);
                }
                catch (PersonAccountNotFoundException)
                {
                    UnmatchedBorrower(record);

                    suspendOperation = true;
                }
                catch (AccountDeletedException)
                {
                    DeletedBorrower(record);

                    foundUser.UserSFDCID = null;

                    dataUtility.SaveChanges();

                    suspendOperation = true;
                }
            }
            
            if (suspendOperation)
            {
                throw new SuspendOperationException("An error occurred during sync, and we need to wait for the Portal Sync process before re-running");
            }

            // rename to Archive
            File.Move(file, file.Replace(".csv", "-Archive.csv"));

            return "OK";
        }

        private static void DeletedBorrower(BlmRecord record)
        {
            Console.Error.WriteLine("{0} {1} {2} {3} was deleted", record.BorrowerFirstName, record.BorrowerLastName, record.BorrowerEmailAddress, record.AccountId);

            DeletedBorrowers.Add(record);
        }

        private static void UnmatchedBorrower(BlmRecord record)
        {
            Console.Error.WriteLine(
                "The borrower was not located (First Name: {0}, Last Name: {1}, Email: {2})!",
                record.BorrowerFirstName,
                record.BorrowerLastName,
                record.BorrowerEmailAddress);

            UnmatchedBorrowers.Add(record);
        }

        private static List<BlmRecord> LoadData(string file)
        {
            List<string> RawLines = new List<string>();
            List<BlmRecord> ProcessedLines = new List<BlmRecord>();
            using (StreamReader fileStrem = new StreamReader(file))
            {
                while (fileStrem.EndOfStream == false)
                {
                    RawLines.Add(fileStrem.ReadLine());
                }
            }

            var foundHeaders = false;
            while (!foundHeaders)
            {
                if (RawLines[0].Trim().ToLower().StartsWith("lender"))
                {
                    foundHeaders = true;
                } else
                {
                    RawLines.RemoveAt(0);
                }

            }

            List<Dictionary<string, string>> parsedCSV = CSVUtil.Parse(RawLines, true);

            foreach (Dictionary<string, string> currentLine in parsedCSV)
            {
                if (string.IsNullOrWhiteSpace(currentLine["Lender"]) || currentLine.Count() < 166)
                    continue;

                ProcessedLines.Add(new BlmRecord(currentLine));
            }

            return ProcessedLines;
        }
    }
}
