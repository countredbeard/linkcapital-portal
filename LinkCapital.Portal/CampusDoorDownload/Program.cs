﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace CampusDoorDownload
{
    class Program
    {
        static void Main(string[] args)
        {
            var home = Environment.GetEnvironmentVariable("HOME");
            var downloadsDirectory = string.Format(@"{0}\data\downloads\", home);

            if (!Directory.Exists(downloadsDirectory))
            {
                Directory.CreateDirectory(downloadsDirectory);
            }

            string address = CloudConfigurationManager.GetSetting("CDDataAddress");
            int port = Convert.ToInt32(CloudConfigurationManager.GetSetting("CDDataPort"));
            string userName = CloudConfigurationManager.GetSetting("CDDataUserName");
            string password = CloudConfigurationManager.GetSetting("CDDataPassword");

            const string sftpDirectory = "FromCD";

            using (var client = new SftpClient(address, port, userName, password))
            {
                client.Connect();

                // if the folder doesn't exist, something is wrong
                if (!client.Exists(sftpDirectory))
                {
                    Environment.Exit(0);
                }
                
                foreach (var currentFile in client.ListDirectory(sftpDirectory))
                {
                    if (currentFile.Name == "." && currentFile.Name == "..") continue;

                    if (!currentFile.Name.ToLower().Contains("allapplications") || !currentFile.Name.ToLower().Contains("csv"))
                    {
                        // todo: should these files be deleted?
                        continue;
                    }

                    // parse date string off file
                    var dateString = currentFile.Name.Split('.')[2].Substring(0, 8);
                    
                    var fileName = new StringBuilder();
                    fileName.Append(downloadsDirectory);
                    fileName.Append("CampusDoor-");
                    fileName.Append(dateString);
                    fileName.Append(".csv");

                    if (File.Exists(fileName.ToString()) || File.Exists(fileName.ToString().Replace(".csv", "-Archive.csv")))
                    {
                        Console.WriteLine("The file was already downloaded, skipping. ({0})", fileName.ToString());
                        continue;
                    }

                    Console.WriteLine("Downloading {0} to {1}", currentFile.Name, fileName);

                    using (FileStream fileStream = new FileStream(fileName.ToString(), FileMode.Create))
                    {
                        client.DownloadFile(currentFile.FullName, fileStream);
                        fileStream.Flush();
                    }

                    // delete the file off the FTP
                    //currentFile.Delete();
                }
                
                client.Disconnect();
            }

        }
    }
}
