﻿using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebJobLogAlerts.Models;
using WebJobLogAlerts.Services;

namespace WebJobLogAlerts
{
    class Program
    {
        private static Regex carriageReturnRE = new Regex("\r", RegexOptions.Multiline | RegexOptions.Compiled);
        private static Regex errorRE = new Regex(@"^\[\d+\/\d+\/\d+ \d+\:\d+:\d+ \> [0-9a-f]+\: ERR\s*\](.+)\s*$", RegexOptions.Multiline | RegexOptions.Compiled);

        static int Main(string[] args)
        {
            Console.WriteLine("WebJobLogAlerts started");
            int exitCode = 0;
            bool failuresOnly = CloudConfigurationManager.GetSetting("ReportFailuresOnly") == "true";

            StringBuilder reportBuilder = new StringBuilder();

            try
            {
                Console.WriteLine("Pulling job statuses");
                foreach (JobReport report in GetJobReport(failuresOnly))
                {
                    Console.WriteLine("Parsing report for {0}", report.Status.name);
                    reportBuilder.Append(ToAlertMessage(report));
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                string subject = String.Format("Error collecting WebJob status on {0}", DateTime.Now),
                    message = String.Format("Exception: {0}\n\nStack trace:\n\n{1}", ex.GetType(), ex.StackTrace);
                
                Console.WriteLine(subject);
                Console.WriteLine(message);

                Alerts.SendAlert(subject, message);

                throw;
            }

            try
            {
                if (reportBuilder.Length > 0)
                {
                    string subject =
                        String.Format("Link Capital WebJob report for {0}", DateTime.Now);


                    Console.WriteLine("Sending {0}", subject);

                    bool alertSuccess = Alerts.SendAlert(subject, reportBuilder.ToString());

                    if (!alertSuccess) { exitCode = -1; }
                } else
                {
                    Console.WriteLine("No alerts to report");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                string subject = String.Format("Error sending WebJob status report", DateTime.Now),
                    message = String.Format("Exception: {0}\n\nStack trace:\n\n{1}", ex.GetType(), ex.StackTrace);

                Console.WriteLine(subject);
                Console.WriteLine(message);

                Alerts.SendAlert(subject, message);

                throw;
            }


            Console.WriteLine("WebJobLogAlerts finished with code {0}", exitCode);

            return exitCode;
        }

        /// <summary>
        /// Get the reports for all triggered webjobs
        /// </summary>
        /// <param name="failuresOnly">Only include failed jobs</param>
        /// <returns>Ienum of webjob reports</returns>
        private static IEnumerable<JobReport> GetJobReport(bool failuresOnly = true)
        {
            var statuses = GetWebJobStatuses();
            var workingSet = failuresOnly
                ? statuses.Where(s => s.latest_run != null &&
                    s.latest_run.status == AzureTypes.StatusSummary.Failed)
                : statuses;

            Dictionary <string, Task<string>> logRetrievals =
                workingSet.ToDictionary(s => s.name, s=> GetLogErrors(s));

            Task.WaitAll(logRetrievals.Values.ToArray());

            IEnumerable<JobReport> result = workingSet.Select(s => new JobReport() {
                    Status = s,
                    LogErrors = logRetrievals[s.name].Result
                });

            return result;
        }

        /// <summary>
        /// Converts a job report to a message
        /// </summary>
        /// <param name="report">The report to convert</param>
        /// <returns>A string message</returns>
        private static string ToAlertMessage(JobReport report)
        {
            StringBuilder output = new StringBuilder();
            output.AppendLine(html.HR);
            string statusSummary = (report.Status.latest_run.status == AzureTypes.StatusSummary.Success)
                ? html.FONT("succeeded", "green")
                : html.FONT("failed", "red");
            
            output.AppendLine(html.H(String.Format("Job {0}: {1}", statusSummary, report.Status.name), 1));
            
            output.AppendLine(html.UL(new string[] {
                    String.Format("Started: {0}", report.Status.latest_run.start_time),
                    String.Format("Duration: {0}", report.Status.latest_run.duration)
                }));

            if (report.LogErrors.Length > 0)
            {
                output.AppendLine(html.H("Errors:", 2));

                output.AppendLine(html.PRE(report.LogErrors));
            }

            output.AppendLine(html.BR);

            return output.ToString();
        }

        /// <summary>
        /// Retrieve error logs for the given webjob status
        /// </summary>
        /// <param name="status">The webjob status</param>
        /// <returns>A task<string> for the error log data</string></returns>
        private static async Task<string> GetLogErrors(WebJobStatus status)
        {
            AzureWebJobRestClient client = new AzureWebJobRestClient(CloudConfigurationManager.GetSetting("AzureProjectName"));

            string log = await client.GetLog(status);
            log = carriageReturnRE.Replace(log, "");
            MatchCollection matches = errorRE.Matches(log);

            StringBuilder result = new StringBuilder();

            foreach(Match match in matches)
            {
                result.AppendLine(match.Groups[1].Value);
            }

            return result.ToString();
        }

        /// <summary>
        /// Retrieve the statuses of all webjobs using Azure REST service
        /// </summary>
        /// <returns>Ienum of webjob statuses</returns>
        private static IEnumerable<WebJobStatus> GetWebJobStatuses()
        {
            AzureWebJobRestClient client = new AzureWebJobRestClient(CloudConfigurationManager.GetSetting("AzureProjectName"));

            return client.GetSummary(AzureWebJobRestClient.WebJobTypes.Triggered).Result;
        }
    }
}
