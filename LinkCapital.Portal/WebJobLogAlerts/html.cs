﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WebJobLogAlerts
{
    public static class html
    {
        public static string H(string input, int level)
        {
            return tag("h" + level.ToString(), input);
        }

        public static string LI(string input)
        {
            return tag("li", input);
        }

        public static string UL(string input)
        {
            return tag("ul", input);
        }

        public static string UL(IEnumerable<string> input)
        {
            return UL(String.Join("", input.Select(i => LI(i))));
        }

        public static string OL(string input)
        {
            return tag("ol", input);
        }

        public static string OL(IEnumerable<string> input)
        {
            return OL(String.Join("", input.Select(i => LI(i))));
        }

        public static string PRE(string input)
        {
            return tag("pre", input);
        }

        public static string FONT(string input, string color = null)
        {
            object attributes = (color != null) ? new { color = color } : null;

            return tag("font", input, attributes);
        }

        public static string BR { get { return "<br />\n"; } }

        public static string HR { get { return "<hr />\n"; } }


        private static string tag(string tag, string input, object attributes = null)
        {
            string beginTag = tag, endTag = tag;

            if (attributes != null)
            {
                foreach (var prop in attributes.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    beginTag += String.Format(" {0}='{1}'", prop.Name, prop.GetValue(attributes));
                }
            }

            return String.Format("<{0}>{1}</{2}>", beginTag, input, endTag);
        }
    }
}
