﻿using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace WebJobLogAlerts.Services
{
    public static class Alerts
    {
        public static bool SendAlert(string subject, string message)
        {
            IEnumerable<string> destinations =
                CloudConfigurationManager.GetSetting("ServiceAlertEmail").Split(';');

            string fromAddress = CloudConfigurationManager.GetSetting("SmtpFromEmail"),
                smtpUser = CloudConfigurationManager.GetSetting("SmtpUser"),
                smtpPassword = CloudConfigurationManager.GetSetting("SmtpPassword"),
                smtpHost = CloudConfigurationManager.GetSetting("SmtpHost");

            bool smtpSsl = CloudConfigurationManager.GetSetting("SmtpSsl").ToLower() == "true",
                 result = false;
            int smtpPort = int.Parse(CloudConfigurationManager.GetSetting("SmtpPort"));

            var email = new MailMessage();
            email.From = new MailAddress(fromAddress);
            email.Subject = subject;
            email.Body = message;
            destinations.ToList().ForEach(to => email.To.Add(to));
            email.IsBodyHtml = true;

            var smtpClient = new SmtpClient(smtpHost, smtpPort)
            {
                Credentials = new NetworkCredential(smtpUser, smtpPassword),
                EnableSsl = smtpSsl
            };

            try
            {
                smtpClient.Send(email);
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error sending alert email '{0}': {1} \n\n {2}", subject, ex.GetType().ToString(), ex.StackTrace);
                throw;
            }

            return result;
        }
    }
}
