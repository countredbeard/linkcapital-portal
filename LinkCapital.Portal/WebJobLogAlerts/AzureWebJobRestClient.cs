﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using WebJobLogAlerts.Models;

namespace WebJobLogAlerts
{
    public class AzureWebJobRestClient
    {
        public enum WebJobTypes { Triggered, Continuous };

        private HttpClient client;
        private string azureProjectName;

        public AzureWebJobRestClient(string projectName)
        {
            this.azureProjectName = projectName;

            this.client = new HttpClient();

            this.client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            this.client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", CloudConfigurationManager.GetSetting("RestClientKey"));
        }

        public async Task<IEnumerable<WebJobStatus>> GetSummary(WebJobTypes webJobType)
        {

            IEnumerable<WebJobStatus> result;
            string serviceUrl =
                String.Format("https://{0}.scm.azurewebsites.net/api/{1}/",
                this.azureProjectName,
                (webJobType == WebJobTypes.Triggered) ? "triggeredwebjobs" : "continuouswebjobs"
                );
            
            if (CloudConfigurationManager.GetSetting("WebJobLogAlerts:DEBUG") == "true")
            {
                Console.WriteLine("Getting job summary:");
                Console.WriteLine(GetContent(serviceUrl).Result);
            }

            result = await CallService<IEnumerable<WebJobStatus>>(serviceUrl);

            return result;
        }

        public async Task<string> GetLog(WebJobStatus status)
        {
            string result = await GetContent(status.latest_run.output_url);

            return result;
        }

        private async Task<T> CallService<T>(string url)
        {
            T result;

            HttpResponseMessage response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            result = await response.Content.ReadAsAsync<T>();

            return result;
        }

        private async Task<string> GetContent(string url)
        {
            string result;

            HttpResponseMessage response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            result = await response.Content.ReadAsStringAsync();

            return result;
        }
    }
}
