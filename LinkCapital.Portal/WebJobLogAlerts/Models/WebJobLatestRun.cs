﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebJobLogAlerts.Models
{
    public class WebJobLatestRun
    {
        public string id { get; set; }

        public string name { get; set; }

        public AzureTypes.StatusSummary status { get; set; }

        public DateTime start_time { get; set; }

        public DateTime end_time { get; set; }

        public string duration { get; set; }

        public string output_url { get; set; }

        public string error_url { get; set; }

        public string url { get; set; }

        public string job_name { get; set; }

        public string trigger { get; set; }

    }
}
