﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebJobLogAlerts.Models
{
    public class WebJobStatus
    {
        public WebJobLatestRun latest_run { get; set; }

        public string history_url { get; set; }

        public string scheduler_logs_url { get; set; }

        public string name { get; set; }

        public string run_command { get; set; }

        public string url { get; set; }

        public string extra_info_url { get; set; }

        public string type { get; set; }

        public string error { get; set; }

        public bool using_sdk { get; set; }

        public object settings { get; set; }
    }
}
