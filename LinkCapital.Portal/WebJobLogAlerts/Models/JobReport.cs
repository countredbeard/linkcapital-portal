﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebJobLogAlerts.Models
{
    /// <summary>
    /// Contains information on the status of a WebJob
    /// </summary>
    public class JobReport
    {
        /// <summary>
        /// The current job status
        /// </summary>
        public WebJobStatus Status { get; set; }

        /// <summary>
        /// Errors found in the log
        /// </summary>
        public string LogErrors { get; set; }
    }
}
