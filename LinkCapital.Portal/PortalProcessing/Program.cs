﻿using Core.EntityModels;
using Core.Repositories;
using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task = System.Threading.Tasks.Task;
using System.IO;
using Core;
using Core.DataModels;
using Core.Exceptions;
using Core.Salesforce;
using Core.SFDC;
using Salesforce.Common;
using Salesforce.Common.Models;
using Contact = Core.DataModels.Contact;

namespace PortalProcessing
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.WaitAll(ProcessPortalUsers());
        }

        /// <summary>
        /// Step 1 - fetch all Portal users
        /// Step 2 - for all without an SFID, find borrower person accounts
        /// Step 3 - for all still without an ID, find leads and convert
        /// Step 4 - for all still without an ID, create lead and convert
        /// Step 5 - any with ID pending changes, sync
        /// Step 6 - save 'em all back to the DB
        /// step 7 - profit
        /// </summary>
        public static async Task ProcessPortalUsers()
        {
            var sfdcUtility = new SfdcUtility()
            {
                SFClientID = CloudConfigurationManager.GetSetting("SFClientID"),
                SFClientSecret = CloudConfigurationManager.GetSetting("SFClientSecret"),
                SFSoapClientSecret = CloudConfigurationManager.GetSetting("SFSoapClientSecret"),
                SFUserName = CloudConfigurationManager.GetSetting("SFUserName"),
                SFPassword = CloudConfigurationManager.GetSetting("SFPassword"),
            };

            var dataUtility = new DataUtility(Environment.GetEnvironmentVariable("SQLAZURECONNSTR_LinkCapital.Portal") ??
                                              CloudConfigurationManager.GetSetting("DBConnection"));

            ApplicationUser.DbEncryptionKey = CloudConfigurationManager.GetSetting("DbEncryptionKey") ?? "BSrhVfHM-PCtSJS06lTxhBP-T$N90MAGA97VLmbB0ILjDXM6JflcdkGaaCoJ-uUW_0p1OnLWdnkw9xbn71anMwTzTOG$_uvNWnzR";

            var now = DateTime.Now;

            #region Steps 1-4

            // find all borrowers without an sfdc id
            var usersWithoutSfid = dataUtility.AllUsers.Where(x => string.IsNullOrEmpty(x.UserSFDCID) && (x.Roles.Count == 0 || x.Roles.Any(y => y.RoleId == "2"))).ToList();

            foreach (var user in usersWithoutSfid)
            {
                var storage = new PortalStorage()
                {
                    LeadStatus = string.IsNullOrWhiteSpace(user.LeadStatus) ? "Created Portal Account" : user.LeadStatus,
                    Account = new PortalBorrowerPersonAccount()
                    {
                        FirstName = user.FirstName.Trim(),
                        LastName = user.LastName.Trim(),
                        PersonEmail = user.Email
                    }
                };

                try
                {
                    // StorePortalInSfdc handles steps 2-4 automatically, returning the person account ID
                    user.UserSFDCID = await sfdcUtility.StorePortalInSfdc(storage);
                    user.LastChanged = now;
                    user.LastSyncDate = now;
                }
                catch (PortalSyncFailedToLocateBorrowerException)
                {
                    Console.Error.WriteLine(
                        "The borrower was not located (First Name: {0}, Last Name: {1}, Email: {2})!", 
                        user.FirstName, 
                        user.LastName, 
                        user.Email);
                }

                dataUtility.SaveChanges();
            }

            #endregion

            #region Step 5

            var usersToSync = dataUtility.AllUsers.Where(x => !string.IsNullOrEmpty(x.UserSFDCID) && x.LastChanged > x.LastSyncDate && (x.Roles.Count == 0 || x.Roles.Any(y => y.RoleId == "2"))).ToList();

            foreach (var user in usersToSync)
            {
                var storage = new PortalStorage()
                {
                    Id = user.UserSFDCID,
                    LeadStatus = string.IsNullOrWhiteSpace(user.LeadStatus) ? "Created Portal Account" : user.LeadStatus,
                    Account = new PortalBorrowerPersonAccount()
                    {
                        FirstName = user.FirstName.Trim(),
                        LastName = user.LastName.Trim(),
                        PersonEmail = user.Email
                    }
                };

                try
                {
                    var accountId = await sfdcUtility.StorePortalInSfdc(storage);
                    if (string.IsNullOrWhiteSpace(user.UserSFDCID)) user.UserSFDCID = accountId;
                    user.LastSyncDate = now;

                    dataUtility.SaveChanges();
                }
                catch (PortalSyncFailedToLocateBorrowerException)
                {
                    Console.Error.WriteLine(
                        "The borrower was not located (First Name: {0}, Last Name: {1}, Email: {2})!",
                        user.FirstName,
                        user.LastName,
                        user.Email);
                }
            }

            #endregion

            #region Step 6

            // just in case
            dataUtility.SaveChanges();

            #endregion

            // profit!

        }
    }
}
