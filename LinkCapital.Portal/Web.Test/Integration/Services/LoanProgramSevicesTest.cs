﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Web.EntityModels;
using Web.Repositories;
using Web.Services;

namespace Web.Test.Integration.Services
{
    [TestFixture]
    public class LoanProgramService_Test
    {
        LoanProgramService Service { get; set; }

        [SetUp]
        public void Setup()
        {
            Service = new LoanProgramService();
        }

        [Test]
        public void ValidateProgramListReturns()
        {
            //var mock = new Mock<DataContext>();

            LoanProgram tempPro = Service.GetLoanProgram("Michigan", 50000, LoanQualificationRestriction.Residency);
            ICollection<LoanProgram> resultList = Service.GetLoanPrograms();

            Assert.Greater(resultList.Count, 0);
        }
    }
}
